en_uk_kjv~SIR~C13~01~He that toucheth pitch shall be defiled therewith; and he that hath fellowship with a proud man shall be like unto him.
en_uk_kjv~SIR~C13~02~Burden not thyself above thy power while thou livest; and have no fellowship with one that is mightier and richer than thyself: for how agree the kettle and the earthen pot together? for if the one be smitten against the other, it shall be broken.
en_uk_kjv~SIR~C13~03~The rich man hath done wrong, and yet he threateneth withal: the poor is wronged, and he must intreat also.
en_uk_kjv~SIR~C13~04~If thou be for his profit, he will use thee: but if thou have nothing, he will forsake thee.
en_uk_kjv~SIR~C13~05~If thou have any thing, he will live with thee: yea, he will make thee bare, and will not be sorry for it.
en_uk_kjv~SIR~C13~06~If he have need of thee, he will deceive thee, and smile upon thee, and put thee in hope; he will speak thee fair, and say, What wantest thou?
en_uk_kjv~SIR~C13~07~And he will shame thee by his meats, until he have drawn thee dry twice or thrice, and at the last he will laugh thee to scorn afterward, when he seeth thee, he will forsake thee, and shake his head at thee.
en_uk_kjv~SIR~C13~08~Beware that thou be not deceived and brought down in thy jollity.
en_uk_kjv~SIR~C13~09~If thou be invited of a mighty man, withdraw thyself, and so much the more will he invite thee.
en_uk_kjv~SIR~C13~10~Press thou not upon him, lest thou be put back; stand not far off, lest thou be forgotten.
en_uk_kjv~SIR~C13~11~Affect not to be made equal unto him in talk, and believe not his many words: for with much communication will he tempt thee, and smiling upon thee will get out thy secrets:
en_uk_kjv~SIR~C13~12~But cruelly he will lay up thy words, and will not spare to do thee hurt, and to put thee in prison.
en_uk_kjv~SIR~C13~13~Observe, and take good heed, for thou walkest in peril of thy overthrowing: when thou hearest these things, awake in thy sleep.
en_uk_kjv~SIR~C13~14~Love the Lord all thy life, and call upon him for thy salvation.
en_uk_kjv~SIR~C13~15~Every beast loveth his like, and every man loveth his neighbor.
en_uk_kjv~SIR~C13~16~All flesh consorteth according to kind, and a man will cleave to his like.
en_uk_kjv~SIR~C13~17~What fellowship hath the wolf with the lamb? so the sinner with the godly.
en_uk_kjv~SIR~C13~18~What agreement is there between the hyena and a dog? and what peace between the rich and the poor?
en_uk_kjv~SIR~C13~19~As the wild ass is the lion’s prey in the wilderness: so the rich eat up the poor.
en_uk_kjv~SIR~C13~20~As the proud hate humility: so doth the rich abhor the poor.
en_uk_kjv~SIR~C13~21~A rich man beginning to fall is held up of his friends: but a poor man being down is thrust away by his friends.
en_uk_kjv~SIR~C13~22~When a rich man is fallen, he hath many helpers: he speaketh things not to be spoken, and yet men justify him: the poor man slipped, and yet they rebuked him too; he spake wisely, and could have no place.
en_uk_kjv~SIR~C13~23~When a rich man speaketh, every man holdeth his tongue, and, look, what he saith, they extol it to the clouds: but if the poor man speak, they say, What fellow is this? and if he stumble, they will help to overthrow him.
en_uk_kjv~SIR~C13~24~Riches are good unto him that hath no sin, and poverty is evil in the mouth of the ungodly.
en_uk_kjv~SIR~C13~25~The heart of a man changeth his countenance, whether it be for good or evil: and a merry heart maketh a cheerful countenance.
en_uk_kjv~SIR~C13~26~A cheerful countenance is a token of a heart that is in prosperity; and the finding out of parables is a wearisome labour of the mind.
