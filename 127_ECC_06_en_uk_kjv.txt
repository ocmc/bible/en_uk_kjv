en_uk_kjv~ECC~C06~01~There is an evil which I have seen under the sun, and it [is] common among men:
en_uk_kjv~ECC~C06~02~A man to whom God hath given riches, wealth, and honour, so that he wanteth nothing for his soul of all that he desireth, yet God giveth him not power to eat thereof, but a stranger eateth it: this [is] vanity, and it [is] an evil disease.
en_uk_kjv~ECC~C06~03~If a man beget an hundred [children], and live many years, so that the days of his years be many, and his soul be not filled with good, and also [that] he have no burial; I say, [that] an untimely birth [is] better than he.
en_uk_kjv~ECC~C06~04~For he cometh in with vanity, and departeth in darkness, and his name shall be covered with darkness.
en_uk_kjv~ECC~C06~05~Moreover he hath not seen the sun, nor known [any thing:] this hath more rest than the other.
en_uk_kjv~ECC~C06~06~Yea, though he live a thousand years twice [told], yet hath he seen no good: do not all go to one place?
en_uk_kjv~ECC~C06~07~All the labour of man [is] for his mouth, and yet the appetite is not filled.
en_uk_kjv~ECC~C06~08~For what hath the wise more than the fool? what hath the poor, that knoweth to walk before the living?
en_uk_kjv~ECC~C06~09~Better [is] the sight of the eyes than the wandering of the desire: this [is] also vanity and vexation of spirit.
en_uk_kjv~ECC~C06~10~That which hath been is named already, and it is known that it [is] man: neither may he contend with him that is mightier than he.
en_uk_kjv~ECC~C06~11~Seeing there be many things that increase vanity, what [is] man the better?
en_uk_kjv~ECC~C06~12~For who knoweth what [is] good for man in [this] life, all the days of his vain life which he spendeth as a shadow? for who can tell a man what shall be after him under the sun?
