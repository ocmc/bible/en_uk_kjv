en_uk_kjv~MA2~C10~01~Now Maccabeus and his company, the Lord guiding them, recovered the temple and the city:
en_uk_kjv~MA2~C10~02~But the altars which the heathen had built in the open street, and also the chapels, they pulled down.
en_uk_kjv~MA2~C10~03~And having cleansed the temple they made another altar, and striking stones they took fire out of them, and offered a sacrifice after two years, and set forth incense, and lights, and shewbread.
en_uk_kjv~MA2~C10~04~When that was done, they fell flat down, and besought the Lord that they might come no more into such troubles; but if they sinned any more against him, that he himself would chasten them with mercy, and that they might not be delivered unto the blasphemous and barbarous nations.
en_uk_kjv~MA2~C10~05~Now upon the same day that the strangers profaned the temple, on the very same day it was cleansed again, even the five and twentieth day of the same month, which is Casleu.
en_uk_kjv~MA2~C10~06~And they kept the eight days with gladness, as in the feast of the tabernacles, remembering that not long afore they had held the feast of the tabernacles, when as they wandered in the mountains and dens like beasts.
en_uk_kjv~MA2~C10~07~Therefore they bare branches, and fair boughs, and palms also, and sang psalms unto him that had given them good success in cleansing his place.
en_uk_kjv~MA2~C10~08~They ordained also by a common statute and decree, That every year those days should be kept of the whole nation of the Jews.
en_uk_kjv~MA2~C10~09~And this was the end of Antiochus, called Epiphanes.
en_uk_kjv~MA2~C10~10~Now will we declare the acts of Antiochus Eupator, who was the son of this wicked man, gathering briefly the calamities of the wars.
en_uk_kjv~MA2~C10~11~So when he was come to the crown, he set one Lysias over the affairs of his realm, and appointed him his chief governor of Celosyria and Phenice.
en_uk_kjv~MA2~C10~12~For Ptolemeus, that was called Macron, choosing rather to do justice unto the Jews for the wrong that had been done unto them, endeavoured to continue peace with them.
en_uk_kjv~MA2~C10~13~Whereupon being accused of the king’s friends before Eupator, and called traitor at every word because he had left Cyprus, that Philometor had committed unto him, and departed to Antiochus Epiphanes, and seeing that he was in no honourable place, he was so discouraged, that he poisoned himself and died.
en_uk_kjv~MA2~C10~14~But when Gorgias was governor of the holds, he hired soldiers, and nourished war continually with the Jews:
en_uk_kjv~MA2~C10~15~And therewithall the Idumeans, having gotten into their hands the most commodious holds, kept the Jews occupied, and receiving those that were banished from Jerusalem, they went about to nourish war.
en_uk_kjv~MA2~C10~16~Then they that were with Maccabeus made supplication, and besought God that he would be their helper; and so they ran with violence upon the strong holds of the Idumeans,
en_uk_kjv~MA2~C10~17~And assaulting them strongly, they won the holds, and kept off all that fought upon the wall, and slew all that fell into their hands, and killed no fewer than twenty thousand.
en_uk_kjv~MA2~C10~18~And because certain, who were no less than nine thousand, were fled together into two very strong castles, having all manner of things convenient to sustain the siege,
en_uk_kjv~MA2~C10~19~Maccabeus left Simon and Joseph, and Zaccheus also, and them that were with him, who were enough to besiege them, and departed himself unto those places which more needed his help.
en_uk_kjv~MA2~C10~20~Now they that were with Simon, being led with covetousness, were persuaded for money through certain of those that were in the castle, and took seventy thousand drachms, and let some of them escape.
en_uk_kjv~MA2~C10~21~But when it was told Maccabeus what was done, he called the governors of the people together, and accused those men, that they had sold their brethren for money, and set their enemies free to fight against them.
en_uk_kjv~MA2~C10~22~So he slew those that were found traitors, and immediately took the two castles.
en_uk_kjv~MA2~C10~23~And having good success with his weapons in all things he took in hand, he slew in the two holds more than twenty thousand.
en_uk_kjv~MA2~C10~24~Now Timotheus, whom the Jews had overcome before, when he had gathered a great multitude of foreign forces, and horses out of Asia not a few, came as though he would take Jewry by force of arms.
en_uk_kjv~MA2~C10~25~But when he drew near, they that were with Maccabeus turned themselves to pray unto God, and sprinkled earth upon their heads, and girded their loins with sackcloth,
en_uk_kjv~MA2~C10~26~And fell down at the foot of the altar, and besought him to be merciful to them, and to be an enemy to their enemies, and an adversary to their adversaries, as the law declareth.
en_uk_kjv~MA2~C10~27~So after the prayer they took their weapons, and went on further from the city: and when they drew near to their enemies, they kept by themselves.
en_uk_kjv~MA2~C10~28~Now the sun being newly risen, they joined both together; the one part having together with their virtue their refuge also unto the Lord for a pledge of their success and victory: the other side making their rage leader of their battle.
en_uk_kjv~MA2~C10~29~But when the battle waxed strong, there appeared unto the enemies from heaven five comely men upon horses, with bridles of gold, and two of them led the Jews,
en_uk_kjv~MA2~C10~30~And took Maccabeus betwixt them, and covered him on every side with their weapons, and kept him safe, but shot arrows and lightnings against the enemies: so that being confounded with blindness, and full of trouble, they were killed.
en_uk_kjv~MA2~C10~31~And there were slain of footmen twenty thousand and five hundred, and six hundred horsemen.
en_uk_kjv~MA2~C10~32~As for Timotheus himself, he fled into a very strong hold, called Gawra, where Chereas was governor.
en_uk_kjv~MA2~C10~33~But they that were with Maccabeus laid siege against the fortress courageously four days.
en_uk_kjv~MA2~C10~34~And they that were within, trusting to the strength of the place, blasphemed exceedingly, and uttered wicked words.
en_uk_kjv~MA2~C10~35~Nevertheless upon the fifth day early twenty young men of Maccabeus’ company, inflamed with anger because of the blasphemies, assaulted the wall manly, and with a fierce courage killed all that they met withal.
en_uk_kjv~MA2~C10~36~Others likewise ascending after them, whiles they were busied with them that were within, burnt the towers, and kindling fires burnt the blasphemers alive; and others broke open the gates, and, having received in the rest of the army, took the city,
en_uk_kjv~MA2~C10~37~And killed Timotheus, that was hid in a certain pit, and Chereas his brother, with Apollophanes.
en_uk_kjv~MA2~C10~38~When this was done, they praised the Lord with psalms and thanksgiving, who had done so great things for Israel, and given them the victory.
