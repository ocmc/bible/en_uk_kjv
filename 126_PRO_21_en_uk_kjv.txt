en_uk_kjv~PRO~C21~01~The king’s heart [is] in the hand of the LORD, [as] the rivers of water: he turneth it whithersoever he will.
en_uk_kjv~PRO~C21~02~Every way of a man [is] right in his own eyes: but the LORD pondereth the hearts.
en_uk_kjv~PRO~C21~03~To do justice and judgment [is] more acceptable to the LORD than sacrifice.
en_uk_kjv~PRO~C21~04~An high look, and a proud heart, [and] the plowing of the wicked, [is] sin.
en_uk_kjv~PRO~C21~05~The thoughts of the diligent [tend] only to plenteousness; but of every one [that is] hasty only to want.
en_uk_kjv~PRO~C21~06~The getting of treasures by a lying tongue [is] a vanity tossed to and fro of them that seek death.
en_uk_kjv~PRO~C21~07~The robbery of the wicked shall destroy them; because they refuse to do judgment.
en_uk_kjv~PRO~C21~08~The way of man [is] froward and strange: but [as for] the pure, his work [is] right.
en_uk_kjv~PRO~C21~09~[It is] better to dwell in a corner of the housetop, than with a brawling woman in a wide house.
en_uk_kjv~PRO~C21~10~The soul of the wicked desireth evil: his neighbour findeth no favour in his eyes.
en_uk_kjv~PRO~C21~11~When the scorner is punished, the simple is made wise: and when the wise is instructed, he receiveth knowledge.
en_uk_kjv~PRO~C21~12~The righteous [man] wisely considereth the house of the wicked: [but God] overthroweth the wicked for [their] wickedness.
en_uk_kjv~PRO~C21~13~Whoso stoppeth his ears at the cry of the poor, he also shall cry himself, but shall not be heard.
en_uk_kjv~PRO~C21~14~A gift in secret pacifieth anger: and a reward in the bosom strong wrath.
en_uk_kjv~PRO~C21~15~[It is] joy to the just to do judgment: but destruction [shall be] to the workers of iniquity.
en_uk_kjv~PRO~C21~16~The man that wandereth out of the way of understanding shall remain in the congregation of the dead.
en_uk_kjv~PRO~C21~17~He that loveth pleasure [shall be] a poor man: he that loveth wine and oil shall not be rich.
en_uk_kjv~PRO~C21~18~The wicked [shall be] a ransom for the righteous, and the transgressor for the upright.
en_uk_kjv~PRO~C21~19~[It is] better to dwell in the wilderness, than with a contentious and an angry woman.
en_uk_kjv~PRO~C21~20~[There is] treasure to be desired and oil in the dwelling of the wise; but a foolish man spendeth it up.
en_uk_kjv~PRO~C21~21~He that followeth after righteousness and mercy findeth life, righteousness, and honour.
en_uk_kjv~PRO~C21~22~A wise [man] scaleth the city of the mighty, and casteth down the strength of the confidence thereof.
en_uk_kjv~PRO~C21~23~Whoso keepeth his mouth and his tongue keepeth his soul from troubles.
en_uk_kjv~PRO~C21~24~Proud [and] haughty scorner [is] his name, who dealeth in proud wrath.
en_uk_kjv~PRO~C21~25~The desire of the slothful killeth him; for his hands refuse to labour.
en_uk_kjv~PRO~C21~26~He coveteth greedily all the day long: but the righteous giveth and spareth not.
en_uk_kjv~PRO~C21~27~The sacrifice of the wicked [is] abomination: how much more, [when] he bringeth it with a wicked mind?
en_uk_kjv~PRO~C21~28~A false witness shall perish: but the man that heareth speaketh constantly.
en_uk_kjv~PRO~C21~29~A wicked man hardeneth his face: but [as for] the upright, he directeth his way.
en_uk_kjv~PRO~C21~30~[There is] no wisdom nor understanding nor counsel against the LORD.
en_uk_kjv~PRO~C21~31~The horse [is] prepared against the day of battle: but safety [is] of the LORD.
