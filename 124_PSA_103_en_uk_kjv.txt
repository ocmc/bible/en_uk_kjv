en_uk_kjv~PSA~C103~001~(C104:001) Bless the LORD, O my soul. O LORD my God, thou art very great; thou art clothed with honour and majesty.
en_uk_kjv~PSA~C103~002~(C104:002) Who coverest [thyself] with light as [with] a garment: who stretchest out the heavens like a curtain:
en_uk_kjv~PSA~C103~003~(C104:003) Who layeth the beams of his chambers in the waters: who maketh the clouds his chariot: who walketh upon the wings of the wind:
en_uk_kjv~PSA~C103~004~(C104:004) Who maketh his angels spirits; his ministers a flaming fire:
en_uk_kjv~PSA~C103~005~(C104:005) [Who] laid the foundations of the earth, [that] it should not be removed for ever.
en_uk_kjv~PSA~C103~006~(C104:006) Thou coveredst it with the deep as [with] a garment: the waters stood above the mountains.
en_uk_kjv~PSA~C103~007~(C104:007) At thy rebuke they fled; at the voice of thy thunder they hasted away.
en_uk_kjv~PSA~C103~008~(C104:008) They go up by the mountains; they go down by the valleys unto the place which thou hast founded for them.
en_uk_kjv~PSA~C103~009~(C104:009) Thou hast set a bound that they may not pass over; that they turn not again to cover the earth.
en_uk_kjv~PSA~C103~010~(C104:010) He sendeth the springs into the valleys, [which] run among the hills.
en_uk_kjv~PSA~C103~011~(C104:011) They give drink to every beast of the field: the wild asses quench their thirst.
en_uk_kjv~PSA~C103~012~(C104:012) By them shall the fowls of the heaven have their habitation, [which] sing among the branches.
en_uk_kjv~PSA~C103~013~(C104:013) He watereth the hills from his chambers: the earth is satisfied with the fruit of thy works.
en_uk_kjv~PSA~C103~014~(C104:014) He causeth the grass to grow for the cattle, and herb for the service of man: that he may bring forth food out of the earth;
en_uk_kjv~PSA~C103~015~(C104:015) And wine [that] maketh glad the heart of man, [and] oil to make [his] face to shine, and bread [which] strengtheneth man’s heart.
en_uk_kjv~PSA~C103~016~(C104:016) The trees of the LORD are full [of sap;] the cedars of Lebanon, which he hath planted;
en_uk_kjv~PSA~C103~017~(C104:017) Where the birds make their nests: [as for] the stork, the fir trees [are] her house.
en_uk_kjv~PSA~C103~018~(C104:018) The high hills [are] a refuge for the wild goats; [and] the rocks for the conies.
en_uk_kjv~PSA~C103~019~(C104:019) He appointed the moon for seasons: the sun knoweth his going down.
en_uk_kjv~PSA~C103~020~(C104:020) Thou makest darkness, and it is night: wherein all the beasts of the forest do creep [forth].
en_uk_kjv~PSA~C103~021~(C104:021) The young lions roar after their prey, and seek their meat from God.
en_uk_kjv~PSA~C103~022~(C104:022) The sun ariseth, they gather themselves together, and lay them down in their dens.
en_uk_kjv~PSA~C103~023~(C104:023) Man goeth forth unto his work and to his labour until the evening.
en_uk_kjv~PSA~C103~024~(C104:024) O LORD, how manifold are thy works! in wisdom hast thou made them all: the earth is full of thy riches.
en_uk_kjv~PSA~C103~025~(C104:025) [So is] this great and wide sea, wherein [are] things creeping innumerable, both small and great beasts.
en_uk_kjv~PSA~C103~026~(C104:026) There go the ships: [there is] that leviathan, [whom] thou hast made to play therein.
en_uk_kjv~PSA~C103~027~(C104:027) These wait all upon thee; that thou mayest give [them] their meat in due season.
en_uk_kjv~PSA~C103~028~(C104:028) [That] thou givest them they gather: thou openest thine hand, they are filled with good.
en_uk_kjv~PSA~C103~029~(C104:029) Thou hidest thy face, they are troubled: thou takest away their breath, they die, and return to their dust.
en_uk_kjv~PSA~C103~030~(C104:030) Thou sendest forth thy spirit, they are created: and thou renewest the face of the earth.
en_uk_kjv~PSA~C103~031~(C104:031) The glory of the LORD shall endure for ever: the LORD shall rejoice in his works.
en_uk_kjv~PSA~C103~032~(C104:032) He looketh on the earth, and it trembleth: he toucheth the hills, and they smoke.
en_uk_kjv~PSA~C103~033~(C104:033) I will sing unto the LORD as long as I live: I will sing praise to my God while I have my being.
en_uk_kjv~PSA~C103~034~(C104:034) My meditation of him shall be sweet: I will be glad in the LORD.
en_uk_kjv~PSA~C103~035~(C104:035) Let the sinners be consumed out of the earth, and let the wicked be no more. Bless thou the LORD, O my soul. Praise ye the LORD.
