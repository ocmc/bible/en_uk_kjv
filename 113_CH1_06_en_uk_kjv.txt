en_uk_kjv~CH1~C06~01~The sons of Levi; Gershon, Kohath, and Merari.
en_uk_kjv~CH1~C06~02~And the sons of Kohath; Amram, Izhar, and Hebron, and Uzziel.
en_uk_kjv~CH1~C06~03~And the children of Amram; Aaron, and Moses, and Miriam. The sons also of Aaron; Nadab, and Abihu, Eleazar, and Ithamar.
en_uk_kjv~CH1~C06~04~Eleazar begat Phinehas, Phinehas begat Abishua,
en_uk_kjv~CH1~C06~05~And Abishua begat Bukki, and Bukki begat Uzzi,
en_uk_kjv~CH1~C06~06~And Uzzi begat Zerahiah, and Zerahiah begat Meraioth,
en_uk_kjv~CH1~C06~07~Meraioth begat Amariah, and Amariah begat Ahitub,
en_uk_kjv~CH1~C06~08~And Ahitub begat Zadok, and Zadok begat Ahimaaz,
en_uk_kjv~CH1~C06~09~And Ahimaaz begat Azariah, and Azariah begat Johanan,
en_uk_kjv~CH1~C06~10~And Johanan begat Azariah, (he [it is] that executed the priest’s office in the temple that Solomon built in Jerusalem:)
en_uk_kjv~CH1~C06~11~And Azariah begat Amariah, and Amariah begat Ahitub,
en_uk_kjv~CH1~C06~12~And Ahitub begat Zadok, and Zadok begat Shallum,
en_uk_kjv~CH1~C06~13~And Shallum begat Hilkiah, and Hilkiah begat Azariah,
en_uk_kjv~CH1~C06~14~And Azariah begat Seraiah, and Seraiah begat Jehozadak,
en_uk_kjv~CH1~C06~15~And Jehozadak went [into captivity], when the LORD carried away Judah and Jerusalem by the hand of Nebuchadnezzar.
en_uk_kjv~CH1~C06~16~The sons of Levi; Gershom, Kohath, and Merari.
en_uk_kjv~CH1~C06~17~And these [be] the names of the sons of Gershom; Libni, and Shimei.
en_uk_kjv~CH1~C06~18~And the sons of Kohath [were], Amram, and Izhar, and Hebron, and Uzziel.
en_uk_kjv~CH1~C06~19~The sons of Merari; Mahli, and Mushi. And these [are] the families of the Levites according to their fathers.
en_uk_kjv~CH1~C06~20~Of Gershom; Libni his son, Jahath his son, Zimmah his son,
en_uk_kjv~CH1~C06~21~Joah his son, Iddo his son, Zerah his son, Jeaterai his son.
en_uk_kjv~CH1~C06~22~The sons of Kohath; Amminadab his son, Korah his son, Assir his son,
en_uk_kjv~CH1~C06~23~Elkanah his son, and Ebiasaph his son, and Assir his son,
en_uk_kjv~CH1~C06~24~Tahath his son, Uriel his son, Uzziah his son, and Shaul his son.
en_uk_kjv~CH1~C06~25~And the sons of Elkanah; Amasai, and Ahimoth.
en_uk_kjv~CH1~C06~26~[As for] Elkanah: the sons of Elkanah; Zophai his son, and Nahath his son,
en_uk_kjv~CH1~C06~27~Eliab his son, Jeroham his son, Elkanah his son.
en_uk_kjv~CH1~C06~28~And the sons of Samuel; the firstborn Vashni, and Abiah.
en_uk_kjv~CH1~C06~29~The sons of Merari; Mahli, Libni his son, Shimei his son, Uzza his son,
en_uk_kjv~CH1~C06~30~Shimea his son, Haggiah his son, Asaiah his son.
en_uk_kjv~CH1~C06~31~And these [are they] whom David set over the service of song in the house of the LORD, after that the ark had rest.
en_uk_kjv~CH1~C06~32~And they ministered before the dwelling place of the tabernacle of the congregation with singing, until Solomon had built the house of the LORD in Jerusalem: and [then] they waited on their office according to their order.
en_uk_kjv~CH1~C06~33~And these [are] they that waited with their children. Of the sons of the Kohathites: Heman a singer, the son of Joel, the son of Shemuel,
en_uk_kjv~CH1~C06~34~The son of Elkanah, the son of Jeroham, the son of Eliel, the son of Toah,
en_uk_kjv~CH1~C06~35~The son of Zuph, the son of Elkanah, the son of Mahath, the son of Amasai,
en_uk_kjv~CH1~C06~36~The son of Elkanah, the son of Joel, the son of Azariah, the son of Zephaniah,
en_uk_kjv~CH1~C06~37~The son of Tahath, the son of Assir, the son of Ebiasaph, the son of Korah,
en_uk_kjv~CH1~C06~38~The son of Izhar, the son of Kohath, the son of Levi, the son of Israel.
en_uk_kjv~CH1~C06~39~And his brother Asaph, who stood on his right hand, [even] Asaph the son of Berachiah, the son of Shimea,
en_uk_kjv~CH1~C06~40~The son of Michael, the son of Baaseiah, the son of Malchiah,
en_uk_kjv~CH1~C06~41~The son of Ethni, the son of Zerah, the son of Adaiah,
en_uk_kjv~CH1~C06~42~The son of Ethan, the son of Zimmah, the son of Shimei,
en_uk_kjv~CH1~C06~43~The son of Jahath, the son of Gershom, the son of Levi.
en_uk_kjv~CH1~C06~44~And their brethren the sons of Merari [stood] on the left hand: Ethan the son of Kishi, the son of Abdi, the son of Malluch,
en_uk_kjv~CH1~C06~45~The son of Hashabiah, the son of Amaziah, the son of Hilkiah,
en_uk_kjv~CH1~C06~46~The son of Amzi, the son of Bani, the son of Shamer,
en_uk_kjv~CH1~C06~47~The son of Mahli, the son of Mushi, the son of Merari, the son of Levi.
en_uk_kjv~CH1~C06~48~Their brethren also the Levites [were] appointed unto all manner of service of the tabernacle of the house of God.
en_uk_kjv~CH1~C06~49~But Aaron and his sons offered upon the altar of the burnt offering, and on the altar of incense, [and were appointed] for all the work of the [place] most holy, and to make an atonement for Israel, according to all that Moses the servant of God had commanded.
en_uk_kjv~CH1~C06~50~And these [are] the sons of Aaron; Eleazar his son, Phinehas his son, Abishua his son,
en_uk_kjv~CH1~C06~51~Bukki his son, Uzzi his son, Zerahiah his son,
en_uk_kjv~CH1~C06~52~Meraioth his son, Amariah his son, Ahitub his son,
en_uk_kjv~CH1~C06~53~Zadok his son, Ahimaaz his son.
en_uk_kjv~CH1~C06~54~Now these [are] their dwelling places throughout their castles in their coasts, of the sons of Aaron, of the families of the Kohathites: for theirs was the lot.
en_uk_kjv~CH1~C06~55~And they gave them Hebron in the land of Judah, and the suburbs thereof round about it.
en_uk_kjv~CH1~C06~56~But the fields of the city, and the villages thereof, they gave to Caleb the son of Jephunneh.
en_uk_kjv~CH1~C06~57~And to the sons of Aaron they gave the cities of Judah, [namely], Hebron, [the city] of refuge, and Libnah with her suburbs, and Jattir, and Eshtemoa, with their suburbs,
en_uk_kjv~CH1~C06~58~And Hilen with her suburbs, Debir with her suburbs,
en_uk_kjv~CH1~C06~59~And Ashan with her suburbs, and Beth-shemesh with her suburbs:
en_uk_kjv~CH1~C06~60~And out of the tribe of Benjamin; Geba with her suburbs, and Alemeth with her suburbs, and Anathoth with her suburbs. All their cities throughout their families [were] thirteen cities.
en_uk_kjv~CH1~C06~61~And unto the sons of Kohath, [which were] left of the family of that tribe, [were cities given] out of the half tribe, [namely, out of] the half [tribe] of Manasseh, by lot, ten cities.
en_uk_kjv~CH1~C06~62~And to the sons of Gershom throughout their families out of the tribe of Issachar, and out of the tribe of Asher, and out of the tribe of Naphtali, and out of the tribe of Manasseh in Bashan, thirteen cities.
en_uk_kjv~CH1~C06~63~Unto the sons of Merari [were given] by lot, throughout their families, out of the tribe of Reuben, and out of the tribe of Gad, and out of the tribe of Zebulun, twelve cities.
en_uk_kjv~CH1~C06~64~And the children of Israel gave to the Levites [these] cities with their suburbs.
en_uk_kjv~CH1~C06~65~And they gave by lot out of the tribe of the children of Judah, and out of the tribe of the children of Simeon, and out of the tribe of the children of Benjamin, these cities, which are called by [their] names.
en_uk_kjv~CH1~C06~66~And [the residue] of the families of the sons of Kohath had cities of their coasts out of the tribe of Ephraim.
en_uk_kjv~CH1~C06~67~And they gave unto them, [of] the cities of refuge, Shechem in mount Ephraim with her suburbs; [they gave] also Gezer with her suburbs,
en_uk_kjv~CH1~C06~68~And Jokmeam with her suburbs, and Beth-horon with her suburbs,
en_uk_kjv~CH1~C06~69~And Aijalon with her suburbs, and Gath-rimmon with her suburbs:
en_uk_kjv~CH1~C06~70~And out of the half tribe of Manasseh; Aner with her suburbs, and Bileam with her suburbs, for the family of the remnant of the sons of Kohath.
en_uk_kjv~CH1~C06~71~Unto the sons of Gershom [were given] out of the family of the half tribe of Manasseh, Golan in Bashan with her suburbs, and Ashtaroth with her suburbs:
en_uk_kjv~CH1~C06~72~And out of the tribe of Issachar; Kedesh with her suburbs, Daberath with her suburbs,
en_uk_kjv~CH1~C06~73~And Ramoth with her suburbs, and Anem with her suburbs:
en_uk_kjv~CH1~C06~74~And out of the tribe of Asher; Mashal with her suburbs, and Abdon with her suburbs,
en_uk_kjv~CH1~C06~75~And Hukok with her suburbs, and Rehob with her suburbs:
en_uk_kjv~CH1~C06~76~And out of the tribe of Naphtali; Kedesh in Galilee with her suburbs, and Hammon with her suburbs, and Kirjathaim with her suburbs.
en_uk_kjv~CH1~C06~77~Unto the rest of the children of Merari [were given] out of the tribe of Zebulun, Rimmon with her suburbs, Tabor with her suburbs:
en_uk_kjv~CH1~C06~78~And on the other side Jordan by Jericho, on the east side of Jordan, [were given them] out of the tribe of Reuben, Bezer in the wilderness with her suburbs, and Jahzah with her suburbs,
en_uk_kjv~CH1~C06~79~Kedemoth also with her suburbs, and Mephaath with her suburbs:
en_uk_kjv~CH1~C06~80~And out of the tribe of Gad; Ramoth in Gilead with her suburbs, and Mahanaim with her suburbs,
en_uk_kjv~CH1~C06~81~And Heshbon with her suburbs, and Jazer with her suburbs.
