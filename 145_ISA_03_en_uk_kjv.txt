en_uk_kjv~ISA~C03~01~For, behold, the Lord, the LORD of hosts, doth take away from Jerusalem and from Judah the stay and the staff, the whole stay of bread, and the whole stay of water,
en_uk_kjv~ISA~C03~02~The mighty man, and the man of war, the judge, and the prophet, and the prudent, and the ancient,
en_uk_kjv~ISA~C03~03~The captain of fifty, and the honourable man, and the counsellor, and the cunning artificer, and the eloquent orator.
en_uk_kjv~ISA~C03~04~And I will give children [to be] their princes, and babes shall rule over them.
en_uk_kjv~ISA~C03~05~And the people shall be oppressed, every one by another, and every one by his neighbour: the child shall behave himself proudly against the ancient, and the base against the honourable.
en_uk_kjv~ISA~C03~06~When a man shall take hold of his brother of the house of his father, [saying], Thou hast clothing, be thou our ruler, and [let] this ruin [be] under thy hand:
en_uk_kjv~ISA~C03~07~In that day shall he swear, saying, I will not be an healer; for in my house [is] neither bread nor clothing: make me not a ruler of the people.
en_uk_kjv~ISA~C03~08~For Jerusalem is ruined, and Judah is fallen: because their tongue and their doings [are] against the LORD, to provoke the eyes of his glory.
en_uk_kjv~ISA~C03~09~The shew of their countenance doth witness against them; and they declare their sin as Sodom, they hide [it] not. Woe unto their soul! for they have rewarded evil unto themselves.
en_uk_kjv~ISA~C03~10~Say ye to the righteous, that [it shall be] well [with him:] for they shall eat the fruit of their doings.
en_uk_kjv~ISA~C03~11~Woe unto the wicked! [it shall be] ill [with him:] for the reward of his hands shall be given him.
en_uk_kjv~ISA~C03~12~[As for] my people, children [are] their oppressors, and women rule over them. O my people, they which lead thee cause [thee] to err, and destroy the way of thy paths.
en_uk_kjv~ISA~C03~13~The LORD standeth up to plead, and standeth to judge the people.
en_uk_kjv~ISA~C03~14~The LORD will enter into judgment with the ancients of his people, and the princes thereof: for ye have eaten up the vineyard; the spoil of the poor [is] in your houses.
en_uk_kjv~ISA~C03~15~What mean ye [that] ye beat my people to pieces, and grind the faces of the poor? saith the Lord GOD of hosts.
en_uk_kjv~ISA~C03~16~Moreover the LORD saith, Because the daughters of Zion are haughty, and walk with stretched forth necks and wanton eyes, walking and mincing [as] they go, and making a tinkling with their feet:
en_uk_kjv~ISA~C03~17~Therefore the Lord will smite with a scab the crown of the head of the daughters of Zion, and the LORD will discover their secret parts.
en_uk_kjv~ISA~C03~18~In that day the Lord will take away the bravery of [their] tinkling ornaments [about their feet], and [their] cauls, and [their] round tires like the moon,
en_uk_kjv~ISA~C03~19~The chains, and the bracelets, and the mufflers,
en_uk_kjv~ISA~C03~20~The bonnets, and the ornaments of the legs, and the headbands, and the tablets, and the earrings,
en_uk_kjv~ISA~C03~21~The rings, and nose jewels,
en_uk_kjv~ISA~C03~22~The changeable suits of apparel, and the mantles, and the wimples, and the crisping pins,
en_uk_kjv~ISA~C03~23~The glasses, and the fine linen, and the hoods, and the vails.
en_uk_kjv~ISA~C03~24~And it shall come to pass, [that] instead of sweet smell there shall be stink; and instead of a girdle a rent; and instead of well set hair baldness; and instead of a stomacher a girding of sackcloth; [and] burning instead of beauty.
en_uk_kjv~ISA~C03~25~Thy men shall fall by the sword, and thy mighty in the war.
en_uk_kjv~ISA~C03~26~And her gates shall lament and mourn; and she [being] desolate shall sit upon the ground.
