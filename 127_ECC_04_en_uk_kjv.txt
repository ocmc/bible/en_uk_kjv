en_uk_kjv~ECC~C04~01~So I returned, and considered all the oppressions that are done under the sun: and behold the tears of [such as were] oppressed, and they had no comforter; and on the side of their oppressors [there was] power; but they had no comforter.
en_uk_kjv~ECC~C04~02~Wherefore I praised the dead which are already dead more than the living which are yet alive.
en_uk_kjv~ECC~C04~03~Yea, better [is he] than both they, which hath not yet been, who hath not seen the evil work that is done under the sun.
en_uk_kjv~ECC~C04~04~Again, I considered all travail, and every right work, that for this a man is envied of his neighbour. This [is] also vanity and vexation of spirit.
en_uk_kjv~ECC~C04~05~The fool foldeth his hands together, and eateth his own flesh.
en_uk_kjv~ECC~C04~06~Better [is] an handful [with] quietness, than both the hands full [with] travail and vexation of spirit.
en_uk_kjv~ECC~C04~07~Then I returned, and I saw vanity under the sun.
en_uk_kjv~ECC~C04~08~There is one [alone], and [there is] not a second; yea, he hath neither child nor brother: yet [is there] no end of all his labour; neither is his eye satisfied with riches; neither [saith he], For whom do I labour, and bereave my soul of good? This [is] also vanity, yea, it [is] a sore travail.
en_uk_kjv~ECC~C04~09~Two [are] better than one; because they have a good reward for their labour.
en_uk_kjv~ECC~C04~10~For if they fall, the one will lift up his fellow: but woe to him [that is] alone when he falleth; for [he hath] not another to help him up.
en_uk_kjv~ECC~C04~11~Again, if two lie together, then they have heat: but how can one be warm [alone]?
en_uk_kjv~ECC~C04~12~And if one prevail against him, two shall withstand him; and a threefold cord is not quickly broken.
en_uk_kjv~ECC~C04~13~Better [is] a poor and a wise child than an old and foolish king, who will no more be admonished.
en_uk_kjv~ECC~C04~14~For out of prison he cometh to reign; whereas also [he that is] born in his kingdom becometh poor.
en_uk_kjv~ECC~C04~15~I considered all the living which walk under the sun, with the second child that shall stand up in his stead.
en_uk_kjv~ECC~C04~16~[There is] no end of all the people, [even] of all that have been before them: they also that come after shall not rejoice in him. Surely this also [is] vanity and vexation of spirit.
