en_uk_kjv~PSA~C011~001~(C012:000) To the chief Musician upon Sheminith, A Psalm of David. 
en_uk_kjv~PSA~C011~002~(C012:001) Help, LORD; for the godly man ceaseth; for the faithful fail from among the children of men.
en_uk_kjv~PSA~C011~003~(C012:002) They speak vanity every one with his neighbour: [with] flattering lips [and] with a double heart do they speak.
en_uk_kjv~PSA~C011~004~(C012:003) The LORD shall cut off all flattering lips, [and] the tongue that speaketh proud things:
en_uk_kjv~PSA~C011~005~(C012:004) Who have said, With our tongue will we prevail; our lips [are] our own: who [is] lord over us?
en_uk_kjv~PSA~C011~006~(C012:005) For the oppression of the poor, for the sighing of the needy, now will I arise, saith the LORD; I will set [him] in safety [from him that] puffeth at him.
en_uk_kjv~PSA~C011~007~(C012:006) The words of the LORD [are] pure words: [as] silver tried in a furnace of earth, purified seven times.
en_uk_kjv~PSA~C011~008~(C012:007) Thou shalt keep them, O LORD, thou shalt preserve them from this generation for ever.
en_uk_kjv~PSA~C011~009~(C012:008) The wicked walk on every side, when the vilest men are exalted.
