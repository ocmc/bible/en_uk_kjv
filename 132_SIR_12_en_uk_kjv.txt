en_uk_kjv~SIR~C12~01~When thou wilt do good know to whom thou doest it; so shalt thou be thanked for thy benefits.
en_uk_kjv~SIR~C12~02~Do good to the godly man, and thou shalt find a recompence; and if not from him, yet from the most High.
en_uk_kjv~SIR~C12~03~There can no good come to him that is always occupied in evil, nor to him that giveth no alms.
en_uk_kjv~SIR~C12~04~Give to the godly man, and help not a sinner.
en_uk_kjv~SIR~C12~05~Do well unto him that is lowly, but give not to the ungodly: hold back thy bread, and give it not unto him, lest he overmaster thee thereby: for [else] thou shalt receive twice as much evil for all the good thou shalt have done unto him.
en_uk_kjv~SIR~C12~06~For the most High hateth sinners, and will repay vengeance unto the ungodly, and keepeth them against the mighty day of their punishment.
en_uk_kjv~SIR~C12~07~Give unto the good, and help not the sinner.
en_uk_kjv~SIR~C12~08~A friend cannot be known in prosperity: and an enemy cannot be hidden in adversity.
en_uk_kjv~SIR~C12~09~In the prosperity of a man enemies will be grieved: but in his adversity even a friend will depart.
en_uk_kjv~SIR~C12~10~Never trust thine enemy: for like as iron rusteth, so is his wickedness.
en_uk_kjv~SIR~C12~11~Though he humble himself, and go crouching, yet take good heed and beware of him, and thou shalt be unto him as if thou hadst wiped a lookingglass, and thou shalt know that his rust hath not been altogether wiped away.
en_uk_kjv~SIR~C12~12~Set him not by thee, lest, when he hath overthrown thee, he stand up in thy place; neither let him sit at thy right hand, lest he seek to take thy seat, and thou at the last remember my words, and be pricked therewith.
en_uk_kjv~SIR~C12~13~Who will pity a charmer that is bitten with a serpent, or any such as come nigh wild beasts?
en_uk_kjv~SIR~C12~14~So one that goeth to a sinner, and is defiled with him in his sins, who will pity?
en_uk_kjv~SIR~C12~15~For a while he will abide with thee, but if thou begin to fall, he will not tarry.
en_uk_kjv~SIR~C12~16~An enemy speaketh sweetly with his lips, but in his heart he imagineth how to throw thee into a pit: he will weep with his eyes, but if he find opportunity, he will not be satisfied with blood.
en_uk_kjv~SIR~C12~17~If adversity come upon thee, thou shalt find him there first; and though he pretend to help thee, yet shall he undermine thee.
en_uk_kjv~SIR~C12~18~He will shake his head, and clap his hands, and whisper much, and change his countenance.
