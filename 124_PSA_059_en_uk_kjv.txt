en_uk_kjv~PSA~C059~001~(C060:000a) To the chief Musician upon Shushan-eduth, Michtam of David, to teach;
en_uk_kjv~PSA~C059~002~(C060:000b) when he strove with Aram-naharaim and with Aram-zobah, when Joab returned, and smote of Edom in the valley of salt twelve thousand.
en_uk_kjv~PSA~C059~003~(C060:001) O God, thou hast cast us off, thou hast scattered us, thou hast been displeased; O turn thyself to us again.
en_uk_kjv~PSA~C059~004~(C060:002) Thou hast made the earth to tremble; thou hast broken it: heal the breaches thereof; for it shaketh.
en_uk_kjv~PSA~C059~005~(C060:003) Thou hast shewed thy people hard things: thou hast made us to drink the wine of astonishment.
en_uk_kjv~PSA~C059~006~(C060:004) Thou hast given a banner to them that fear thee, that it may be displayed because of the truth. Selah.
en_uk_kjv~PSA~C059~007~(C060:005) That thy beloved may be delivered; save [with] thy right hand, and hear me.
en_uk_kjv~PSA~C059~008~(C060:006) God hath spoken in his holiness; I will rejoice, I will divide Shechem, and mete out the valley of Succoth.
en_uk_kjv~PSA~C059~009~(C060:007) Gilead [is] mine, and Manasseh [is] mine; Ephraim also [is] the strength of mine head; Judah [is] my lawgiver;
en_uk_kjv~PSA~C059~010~(C060:008) Moab [is] my washpot; over Edom will I cast out my shoe: Philistia, triumph thou because of me.
en_uk_kjv~PSA~C059~011~(C060:009) Who will bring me [into] the strong city? who will lead me into Edom?
en_uk_kjv~PSA~C059~012~(C060:010) [Wilt] not thou, O God, [which] hadst cast us off? and [thou], O God, [which] didst not go out with our armies?
en_uk_kjv~PSA~C059~013~(C060:011) Give us help from trouble: for vain [is] the help of man.
en_uk_kjv~PSA~C059~014~(C060:012) Through God we shall do valiantly: for he [it is that] shall tread down our enemies.
