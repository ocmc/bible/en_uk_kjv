en_uk_kjv~PRO~C12~01~Whoso loveth instruction loveth knowledge: but he that hateth reproof [is] brutish.
en_uk_kjv~PRO~C12~02~A good [man] obtaineth favour of the LORD: but a man of wicked devices will he condemn.
en_uk_kjv~PRO~C12~03~A man shall not be established by wickedness: but the root of the righteous shall not be moved.
en_uk_kjv~PRO~C12~04~A virtuous woman [is] a crown to her husband: but she that maketh ashamed [is] as rottenness in his bones.
en_uk_kjv~PRO~C12~05~The thoughts of the righteous [are] right: [but] the counsels of the wicked [are] deceit.
en_uk_kjv~PRO~C12~06~The words of the wicked [are] to lie in wait for blood: but the mouth of the upright shall deliver them.
en_uk_kjv~PRO~C12~07~The wicked are overthrown, and [are] not: but the house of the righteous shall stand.
en_uk_kjv~PRO~C12~08~A man shall be commended according to his wisdom: but he that is of a perverse heart shall be despised.
en_uk_kjv~PRO~C12~09~[He that is] despised, and hath a servant, [is] better than he that honoureth himself, and lacketh bread.
en_uk_kjv~PRO~C12~10~A righteous [man] regardeth the life of his beast: but the tender mercies of the wicked [are] cruel.
en_uk_kjv~PRO~C12~11~He that tilleth his land shall be satisfied with bread: but he that followeth vain [persons is] void of understanding.
en_uk_kjv~PRO~C12~12~The wicked desireth the net of evil [men:] but the root of the righteous yieldeth [fruit].
en_uk_kjv~PRO~C12~13~The wicked is snared by the transgression of [his] lips: but the just shall come out of trouble.
en_uk_kjv~PRO~C12~14~A man shall be satisfied with good by the fruit of [his] mouth: and the recompence of a man’s hands shall be rendered unto him.
en_uk_kjv~PRO~C12~15~The way of a fool [is] right in his own eyes: but he that hearkeneth unto counsel [is] wise.
en_uk_kjv~PRO~C12~16~A fool’s wrath is presently known: but a prudent [man] covereth shame.
en_uk_kjv~PRO~C12~17~[He that] speaketh truth sheweth forth righteousness: but a false witness deceit.
en_uk_kjv~PRO~C12~18~There is that speaketh like the piercings of a sword: but the tongue of the wise [is] health.
en_uk_kjv~PRO~C12~19~The lip of truth shall be established for ever: but a lying tongue [is] but for a moment.
en_uk_kjv~PRO~C12~20~Deceit [is] in the heart of them that imagine evil: but to the counsellors of peace [is] joy.
en_uk_kjv~PRO~C12~21~There shall no evil happen to the just: but the wicked shall be filled with mischief.
en_uk_kjv~PRO~C12~22~Lying lips [are] abomination to the LORD: but they that deal truly [are] his delight.
en_uk_kjv~PRO~C12~23~A prudent man concealeth knowledge: but the heart of fools proclaimeth foolishness.
en_uk_kjv~PRO~C12~24~The hand of the diligent shall bear rule: but the slothful shall be under tribute.
en_uk_kjv~PRO~C12~25~Heaviness in the heart of man maketh it stoop: but a good word maketh it glad.
en_uk_kjv~PRO~C12~26~The righteous [is] more excellent than his neighbour: but the way of the wicked seduceth them.
en_uk_kjv~PRO~C12~27~The slothful [man] roasteth not that which he took in hunting: but the substance of a diligent man [is] precious.
en_uk_kjv~PRO~C12~28~In the way of righteousness [is] life; and [in] the pathway [thereof there is] no death.
