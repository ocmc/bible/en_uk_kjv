en_uk_kjv~PSA~C072~001~(C073:001) (0) A Psalm of Asaph. (1) Truly God [is] good to Israel, [even] to such as are of a clean heart.
en_uk_kjv~PSA~C072~002~(C073:002) But as for me, my feet were almost gone; my steps had well nigh slipped.
en_uk_kjv~PSA~C072~003~(C073:003) For I was envious at the foolish, [when] I saw the prosperity of the wicked.
en_uk_kjv~PSA~C072~004~(C073:004) For [there are] no bands in their death: but their strength [is] firm.
en_uk_kjv~PSA~C072~005~(C073:005) They [are] not in trouble [as other] men; neither are they plagued like [other] men.
en_uk_kjv~PSA~C072~006~(C073:006) Therefore pride compasseth them about as a chain; violence covereth them [as] a garment.
en_uk_kjv~PSA~C072~007~(C073:007) Their eyes stand out with fatness: they have more than heart could wish.
en_uk_kjv~PSA~C072~008~(C073:008) They are corrupt, and speak wickedly [concerning] oppression: they speak loftily.
en_uk_kjv~PSA~C072~009~(C073:009) They set their mouth against the heavens, and their tongue walketh through the earth.
en_uk_kjv~PSA~C072~010~(C073:010) Therefore his people return hither: and waters of a full [cup] are wrung out to them.
en_uk_kjv~PSA~C072~011~(C073:011) And they say, How doth God know? and is there knowledge in the most High?
en_uk_kjv~PSA~C072~012~(C073:012) Behold, these [are] the ungodly, who prosper in the world; they increase [in] riches.
en_uk_kjv~PSA~C072~013~(C073:013) Verily I have cleansed my heart [in] vain, and washed my hands in innocency.
en_uk_kjv~PSA~C072~014~(C073:014) For all the day long have I been plagued, and chastened every morning.
en_uk_kjv~PSA~C072~015~(C073:015) If I say, I will speak thus; behold, I should offend [against] the generation of thy children.
en_uk_kjv~PSA~C072~016~(C073:016) When I thought to know this, it [was] too painful for me;
en_uk_kjv~PSA~C072~017~(C073:017) Until I went into the sanctuary of God; [then] understood I their end.
en_uk_kjv~PSA~C072~018~(C073:018) Surely thou didst set them in slippery places: thou castedst them down into destruction.
en_uk_kjv~PSA~C072~019~(C073:019) How are they [brought] into desolation, as in a moment! they are utterly consumed with terrors.
en_uk_kjv~PSA~C072~020~(C073:020) As a dream when [one] awaketh; [so], O Lord, when thou awakest, thou shalt despise their image.
en_uk_kjv~PSA~C072~021~(C073:021) Thus my heart was grieved, and I was pricked in my reins.
en_uk_kjv~PSA~C072~022~(C073:022) So foolish [was] I, and ignorant: I was [as] a beast before thee.
en_uk_kjv~PSA~C072~023~(C073:023) Nevertheless I [am] continually with thee: thou hast holden [me] by my right hand.
en_uk_kjv~PSA~C072~024~(C073:024) Thou shalt guide me with thy counsel, and afterward receive me [to] glory.
en_uk_kjv~PSA~C072~025~(C073:025) Whom have I in heaven [but thee]? and [there is] none upon earth [that] I desire beside thee.
en_uk_kjv~PSA~C072~026~(C073:026) My flesh and my heart faileth: [but] God [is] the strength of my heart, and my portion for ever.
en_uk_kjv~PSA~C072~027~(C073:027) For, lo, they that are far from thee shall perish: thou hast destroyed all them that go a whoring from thee.
en_uk_kjv~PSA~C072~028~(C073:028) But [it is] good for me to draw near to God: I have put my trust in the Lord GOD, that I may declare all thy works.
