en_uk_kjv~HEB~C04~01~Let us therefore fear, lest, a promise being left [us] of entering into his rest, any of you should seem to come short of it.
en_uk_kjv~HEB~C04~02~For unto us was the gospel preached, as well as unto them: but the word preached did not profit them, not being mixed with faith in them that heard [it].
en_uk_kjv~HEB~C04~03~For we which have believed do enter into rest, as he said, As I have sworn in my wrath, if they shall enter into my rest: although the works were finished from the foundation of the world.
en_uk_kjv~HEB~C04~04~For he spake in a certain place of the seventh [day] on this wise, And God did rest the seventh day from all his works.
en_uk_kjv~HEB~C04~05~And in this [place] again, If they shall enter into my rest.
en_uk_kjv~HEB~C04~06~Seeing therefore it remaineth that some must enter therein, and they to whom it was first preached entered not in because of unbelief:
en_uk_kjv~HEB~C04~07~Again, he limiteth a certain day, saying in David, To day, after so long a time; as it is said, To day if ye will hear his voice, harden not your hearts.
en_uk_kjv~HEB~C04~08~For if Jesus had given them rest, then would he not afterward have spoken of another day.
en_uk_kjv~HEB~C04~09~There remaineth therefore a rest to the people of God.
en_uk_kjv~HEB~C04~10~For he that is entered into his rest, he also hath ceased from his own works, as God [did] from his.
en_uk_kjv~HEB~C04~11~Let us labour therefore to enter into that rest, lest any man fall after the same example of unbelief.
en_uk_kjv~HEB~C04~12~For the word of God [is] quick, and powerful, and sharper than any twoedged sword, piercing even to the dividing asunder of soul and spirit, and of the joints and marrow, and [is] a discerner of the thoughts and intents of the heart.
en_uk_kjv~HEB~C04~13~Neither is there any creature that is not manifest in his sight: but all things [are] naked and opened unto the eyes of him with whom we have to do.
en_uk_kjv~HEB~C04~14~Seeing then that we have a great high priest, that is passed into the heavens, Jesus the Son of God, let us hold fast [our] profession.
en_uk_kjv~HEB~C04~15~For we have not an high priest which cannot be touched with the feeling of our infirmities; but was in all points tempted like as [we are, yet] without sin.
en_uk_kjv~HEB~C04~16~Let us therefore come boldly unto the throne of grace, that we may obtain mercy, and find grace to help in time of need.
