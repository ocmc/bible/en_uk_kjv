en_uk_kjv~HOS~C06~01~Come, and let us return unto the LORD: for he hath torn, and he will heal us; he hath smitten, and he will bind us up.
en_uk_kjv~HOS~C06~02~After two days will he revive us: in the third day he will raise us up, and we shall live in his sight.
en_uk_kjv~HOS~C06~03~Then shall we know, [if] we follow on to know the LORD: his going forth is prepared as the morning; and he shall come unto us as the rain, as the latter [and] former rain unto the earth.
en_uk_kjv~HOS~C06~04~O Ephraim, what shall I do unto thee? O Judah, what shall I do unto thee? for your goodness [is] as a morning cloud, and as the early dew it goeth away.
en_uk_kjv~HOS~C06~05~Therefore have I hewed [them] by the prophets; I have slain them by the words of my mouth: and thy judgments [are as] the light [that] goeth forth.
en_uk_kjv~HOS~C06~06~For I desired mercy, and not sacrifice; and the knowledge of God more than burnt offerings.
en_uk_kjv~HOS~C06~07~But they like men have transgressed the covenant: there have they dealt treacherously against me.
en_uk_kjv~HOS~C06~08~Gilead [is] a city of them that work iniquity, [and is] polluted with blood.
en_uk_kjv~HOS~C06~09~And as troops of robbers wait for a man, [so] the company of priests murder in the way by consent: for they commit lewdness.
en_uk_kjv~HOS~C06~10~I have seen an horrible thing in the house of Israel: there [is] the whoredom of Ephraim, Israel is defiled.
en_uk_kjv~HOS~C06~11~Also, O Judah, he hath set an harvest for thee, when I returned the captivity of my people.
