en_uk_kjv~CO2~C06~01~We then, [as] workers together [with him], beseech [you] also that ye receive not the grace of God in vain.
en_uk_kjv~CO2~C06~02~(For he saith, I have heard thee in a time accepted, and in the day of salvation have I succoured thee: behold, now [is] the accepted time; behold, now [is] the day of salvation.)
en_uk_kjv~CO2~C06~03~Giving no offence in any thing, that the ministry be not blamed:
en_uk_kjv~CO2~C06~04~But in all [things] approving ourselves as the ministers of God, in much patience, in afflictions, in necessities, in distresses,
en_uk_kjv~CO2~C06~05~In stripes, in imprisonments, in tumults, in labours, in watchings, in fastings;
en_uk_kjv~CO2~C06~06~By pureness, by knowledge, by longsuffering, by kindness, by the Holy Ghost, by love unfeigned,
en_uk_kjv~CO2~C06~07~By the word of truth, by the power of God, by the armour of righteousness on the right hand and on the left,
en_uk_kjv~CO2~C06~08~By honour and dishonour, by evil report and good report: as deceivers, and [yet] true;
en_uk_kjv~CO2~C06~09~As unknown, and [yet] well known; as dying, and, behold, we live; as chastened, and not killed;
en_uk_kjv~CO2~C06~10~As sorrowful, yet alway rejoicing; as poor, yet making many rich; as having nothing, and [yet] possessing all things.
en_uk_kjv~CO2~C06~11~O [ye] Corinthians, our mouth is open unto you, our heart is enlarged.
en_uk_kjv~CO2~C06~12~Ye are not straitened in us, but ye are straitened in your own bowels.
en_uk_kjv~CO2~C06~13~Now for a recompence in the same, (I speak as unto [my] children,) be ye also enlarged.
en_uk_kjv~CO2~C06~14~Be ye not unequally yoked together with unbelievers: for what fellowship hath righteousness with unrighteousness? and what communion hath light with darkness?
en_uk_kjv~CO2~C06~15~And what concord hath Christ with Belial? or what part hath he that believeth with an infidel?
en_uk_kjv~CO2~C06~16~And what agreement hath the temple of God with idols? for ye are the temple of the living God; as God hath said, I will dwell in them, and walk in [them]; and I will be their God, and they shall be my people.
en_uk_kjv~CO2~C06~17~Wherefore come out from among them, and be ye separate, saith the Lord, and touch not the unclean [thing]; and I will receive you,
en_uk_kjv~CO2~C06~18~And will be a Father unto you, and ye shall be my sons and daughters, saith the Lord Almighty.
