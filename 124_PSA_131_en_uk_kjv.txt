en_uk_kjv~PSA~C131~001~(C132:001) (0) A Song of degrees. (1) LORD, remember David, [and] all his afflictions:
en_uk_kjv~PSA~C131~002~(C132:002) How he sware unto the LORD, [and] vowed unto the mighty [God] of Jacob;
en_uk_kjv~PSA~C131~003~(C132:003) Surely I will not come into the tabernacle of my house, nor go up into my bed;
en_uk_kjv~PSA~C131~004~(C132:004) I will not give sleep to mine eyes, [or] slumber to mine eyelids,
en_uk_kjv~PSA~C131~005~(C132:005) Until I find out a place for the LORD, an habitation for the mighty [God] of Jacob.
en_uk_kjv~PSA~C131~006~(C132:006) Lo, we heard of it at Ephratah: we found it in the fields of the wood.
en_uk_kjv~PSA~C131~007~(C132:007) We will go into his tabernacles: we will worship at his footstool.
en_uk_kjv~PSA~C131~008~(C132:008) Arise, O LORD, into thy rest; thou, and the ark of thy strength.
en_uk_kjv~PSA~C131~009~(C132:009) Let thy priests be clothed with righteousness; and let thy saints shout for joy.
en_uk_kjv~PSA~C131~010~(C132:010) For thy servant David’s sake turn not away the face of thine anointed.
en_uk_kjv~PSA~C131~011~(C132:011) The LORD hath sworn [in] truth unto David; he will not turn from it; Of the fruit of thy body will I set upon thy throne.
en_uk_kjv~PSA~C131~012~(C132:012) If thy children will keep my covenant and my testimony that I shall teach them, their children shall also sit upon thy throne for evermore.
en_uk_kjv~PSA~C131~013~(C132:013) For the LORD hath chosen Zion; he hath desired [it] for his habitation.
en_uk_kjv~PSA~C131~014~(C132:014) This [is] my rest for ever: here will I dwell; for I have desired it.
en_uk_kjv~PSA~C131~015~(C132:015) I will abundantly bless her provision: I will satisfy her poor with bread.
en_uk_kjv~PSA~C131~016~(C132:016) I will also clothe her priests with salvation: and her saints shall shout aloud for joy.
en_uk_kjv~PSA~C131~017~(C132:017) There will I make the horn of David to bud: I have ordained a lamp for mine anointed.
en_uk_kjv~PSA~C131~018~(C132:018) His enemies will I clothe with shame: but upon himself shall his crown flourish.
