en_uk_kjv~ACT~C06~01~And in those days, when the number of the disciples was multiplied, there arose a murmuring of the Grecians against the Hebrews, because their widows were neglected in the daily ministration.
en_uk_kjv~ACT~C06~02~Then the twelve called the multitude of the disciples [unto them], and said, It is not reason that we should leave the word of God, and serve tables.
en_uk_kjv~ACT~C06~03~Wherefore, brethren, look ye out among you seven men of honest report, full of the Holy Ghost and wisdom, whom we may appoint over this business.
en_uk_kjv~ACT~C06~04~But we will give ourselves continually to prayer, and to the ministry of the word.
en_uk_kjv~ACT~C06~05~And the saying pleased the whole multitude: and they chose Stephen, a man full of faith and of the Holy Ghost, and Philip, and Prochorus, and Nicanor, and Timon, and Parmenas, and Nicolas a proselyte of Antioch:
en_uk_kjv~ACT~C06~06~Whom they set before the apostles: and when they had prayed, they laid [their] hands on them.
en_uk_kjv~ACT~C06~07~And the word of God increased; and the number of the disciples multiplied in Jerusalem greatly; and a great company of the priests were obedient to the faith.
en_uk_kjv~ACT~C06~08~And Stephen, full of faith and power, did great wonders and miracles among the people.
en_uk_kjv~ACT~C06~09~Then there arose certain of the synagogue, which is called [the synagogue] of the Libertines, and Cyrenians, and Alexandrians, and of them of Cilicia and of Asia, disputing with Stephen.
en_uk_kjv~ACT~C06~10~And they were not able to resist the wisdom and the spirit by which he spake.
en_uk_kjv~ACT~C06~11~Then they suborned men, which said, We have heard him speak blasphemous words against Moses, and [against] God.
en_uk_kjv~ACT~C06~12~And they stirred up the people, and the elders, and the scribes, and came upon [him], and caught him, and brought [him] to the council,
en_uk_kjv~ACT~C06~13~And set up false witnesses, which said, This man ceaseth not to speak blasphemous words against this holy place, and the law:
en_uk_kjv~ACT~C06~14~For we have heard him say, that this Jesus of Nazareth shall destroy this place, and shall change the customs which Moses delivered us.
en_uk_kjv~ACT~C06~15~And all that sat in the council, looking stedfastly on him, saw his face as it had been the face of an angel.
