en_uk_kjv~WIS~C10~01~She preserved the first formed father of the world, that was created alone, and brought him out of his fall,
en_uk_kjv~WIS~C10~02~And gave him power to rule all things.
en_uk_kjv~WIS~C10~03~But when the unrighteous went away from her in his anger, he perished also in the fury wherewith he murdered his brother.
en_uk_kjv~WIS~C10~04~For whose cause the earth being drowned with the flood, wisdom again preserved it, and directed the course of the righteous in a piece of wood of small value.
en_uk_kjv~WIS~C10~05~Moreover, the nations in their wicked conspiracy being confounded, she found out the righteous, and preserved him blameless unto God, and kept him strong against his tender compassion toward his son.
en_uk_kjv~WIS~C10~06~When the ungodly perished, she delivered the righteous man, who fled from the fire which fell down upon the five cities.
en_uk_kjv~WIS~C10~07~Of whose wickedness even to this day the waste land that smoketh is a testimony, and plants bearing fruit that never come to ripeness: and a standing pillar of salt is a monument of an unbelieving soul.
en_uk_kjv~WIS~C10~08~For regarding not wisdom, they gat not only this hurt, that they knew not the things which were good; but also left behind them to the world a memorial of their foolishness: so that in the things wherein they offended they could not so much as be hid.
en_uk_kjv~WIS~C10~09~But wisdom delivered from pain those that attended upon her.
en_uk_kjv~WIS~C10~10~When the righteous fled from his brother’s wrath she guided him in right paths, shewed him the kingdom of God, and gave him knowledge of holy things, made him rich in his travels, and multiplied the fruit of his labours.
en_uk_kjv~WIS~C10~11~In the covetousness of such as oppressed him she stood by him, and made him rich.
en_uk_kjv~WIS~C10~12~She defended him from his enemies, and kept him safe from those that lay in wait, and in a sore conflict she gave him the victory; that he might know that goodness is stronger than all.
en_uk_kjv~WIS~C10~13~When the righteous was sold, she forsook him not, but delivered him from sin: she went down with him into the pit,
en_uk_kjv~WIS~C10~14~And left him not in bonds, till she brought him the sceptre of the kingdom, and power against those that oppressed him: as for them that had accused him, she shewed them to be liars, and gave him perpetual glory.
en_uk_kjv~WIS~C10~15~She delivered the righteous people and blameless seed from the nation that oppressed them.
en_uk_kjv~WIS~C10~16~She entered into the soul of the servant of the Lord, and withstood dreadful kings in wonders and signs;
en_uk_kjv~WIS~C10~17~Rendered to the righteous a reward of their labours, guided them in a marvellous way, and was unto them for a cover by day, and a light of stars in the night season;
en_uk_kjv~WIS~C10~18~Brought them through the Red sea, and led them through much water:
en_uk_kjv~WIS~C10~19~But she drowned their enemies, and cast them up out of the bottom of the deep.
en_uk_kjv~WIS~C10~20~Therefore the righteous spoiled the ungodly, and praised thy holy name, O Lord, and magnified with one accord thine hand, that fought for them.
en_uk_kjv~WIS~C10~21~For wisdom opened the mouth of the dumb, and made the tongues of them that cannot speak eloquent.
