en_uk_kjv~PSA~C078~001~(C079:001) (0) A Psalm of Asaph. (1) O God, the heathen are come into thine inheritance; thy holy temple have they defiled; they have laid Jerusalem on heaps.
en_uk_kjv~PSA~C078~002~(C079:002) The dead bodies of thy servants have they given [to be] meat unto the fowls of the heaven, the flesh of thy saints unto the beasts of the earth.
en_uk_kjv~PSA~C078~003~(C079:003) Their blood have they shed like water round about Jerusalem; and [there was] none to bury [them].
en_uk_kjv~PSA~C078~004~(C079:004) We are become a reproach to our neighbours, a scorn and derision to them that are round about us.
en_uk_kjv~PSA~C078~005~(C079:005) How long, LORD? wilt thou be angry for ever? shall thy jealousy burn like fire?
en_uk_kjv~PSA~C078~006~(C079:006) Pour out thy wrath upon the heathen that have not known thee, and upon the kingdoms that have not called upon thy name.
en_uk_kjv~PSA~C078~007~(C079:007) For they have devoured Jacob, and laid waste his dwelling place.
en_uk_kjv~PSA~C078~008~(C079:008) O remember not against us former iniquities: let thy tender mercies speedily prevent us: for we are brought very low.
en_uk_kjv~PSA~C078~009~(C079:009) Help us, O God of our salvation, for the glory of thy name: and deliver us, and purge away our sins, for thy name’s sake.
en_uk_kjv~PSA~C078~010~(C079:010) Wherefore should the heathen say, Where [is] their God? let him be known among the heathen in our sight [by] the revenging of the blood of thy servants [which is] shed.
en_uk_kjv~PSA~C078~011~(C079:011) Let the sighing of the prisoner come before thee; according to the greatness of thy power preserve thou those that are appointed to die;
en_uk_kjv~PSA~C078~012~(C079:012) And render unto our neighbours sevenfold into their bosom their reproach, wherewith they have reproached thee, O Lord.
en_uk_kjv~PSA~C078~013~(C079:013) So we thy people and sheep of thy pasture will give thee thanks for ever: we will shew forth thy praise to all generations.
