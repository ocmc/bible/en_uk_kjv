en_uk_kjv~CH1~C12~01~Now these [are] they that came to David to Ziklag, while he yet kept himself close because of Saul the son of Kish: and they [were] among the mighty men, helpers of the war.
en_uk_kjv~CH1~C12~02~[They were] armed with bows, and could use both the right hand and the left in [hurling] stones and [shooting] arrows out of a bow, [even] of Saul’s brethren of Benjamin.
en_uk_kjv~CH1~C12~03~The chief [was] Ahiezer, then Joash, the sons of Shemaah the Gibeathite; and Jeziel, and Pelet, the sons of Azmaveth; and Berachah, and Jehu the Antothite,
en_uk_kjv~CH1~C12~04~And Ismaiah the Gibeonite, a mighty man among the thirty, and over the thirty; and Jeremiah, and Jahaziel, and Johanan, and Josabad the Gederathite,
en_uk_kjv~CH1~C12~05~Eluzai, and Jerimoth, and Bealiah, and Shemariah, and Shephatiah the Haruphite,
en_uk_kjv~CH1~C12~06~Elkanah, and Jesiah, and Azareel, and Joezer, and Jashobeam, the Korhites,
en_uk_kjv~CH1~C12~07~And Joelah, and Zebadiah, the sons of Jeroham of Gedor.
en_uk_kjv~CH1~C12~08~And of the Gadites there separated themselves unto David into the hold to the wilderness men of might, [and] men of war [fit] for the battle, that could handle shield and buckler, whose faces [were like] the faces of lions, and [were] as swift as the roes upon the mountains;
en_uk_kjv~CH1~C12~09~Ezer the first, Obadiah the second, Eliab the third,
en_uk_kjv~CH1~C12~10~Mishmannah the fourth, Jeremiah the fifth,
en_uk_kjv~CH1~C12~11~Attai the sixth, Eliel the seventh,
en_uk_kjv~CH1~C12~12~Johanan the eighth, Elzabad the ninth,
en_uk_kjv~CH1~C12~13~Jeremiah the tenth, Machbanai the eleventh.
en_uk_kjv~CH1~C12~14~These [were] of the sons of Gad, captains of the host: one of the least [was] over an hundred, and the greatest over a thousand.
en_uk_kjv~CH1~C12~15~These [are] they that went over Jordan in the first month, when it had overflown all his banks; and they put to flight all [them] of the valleys, [both] toward the east, and toward the west.
en_uk_kjv~CH1~C12~16~And there came of the children of Benjamin and Judah to the hold unto David.
en_uk_kjv~CH1~C12~17~And David went out to meet them, and answered and said unto them, If ye be come peaceably unto me to help me, mine heart shall be knit unto you: but if [ye be come] to betray me to mine enemies, seeing [there is] no wrong in mine hands, the God of our fathers look [thereon], and rebuke [it].
en_uk_kjv~CH1~C12~18~Then the spirit came upon Amasai, [who was] chief of the captains, [and he said], Thine [are we], David, and on thy side, thou son of Jesse: peace, peace [be] unto thee, and peace [be] to thine helpers; for thy God helpeth thee. Then David received them, and made them captains of the band.
en_uk_kjv~CH1~C12~19~And there fell [some] of Manasseh to David, when he came with the Philistines against Saul to battle: but they helped them not: for the lords of the Philistines upon advisement sent him away, saying, He will fall to his master Saul to [the jeopardy of] our heads.
en_uk_kjv~CH1~C12~20~As he went to Ziklag, there fell to him of Manasseh, Adnah, and Jozabad, and Jediael, and Michael, and Jozabad, and Elihu, and Zilthai, captains of the thousands that [were] of Manasseh.
en_uk_kjv~CH1~C12~21~And they helped David against the band [of the rovers:] for they [were] all mighty men of valour, and were captains in the host.
en_uk_kjv~CH1~C12~22~For at [that] time day by day there came to David to help him, until [it was] a great host, like the host of God.
en_uk_kjv~CH1~C12~23~And these [are] the numbers of the bands [that were] ready armed to the war, [and] came to David to Hebron, to turn the kingdom of Saul to him, according to the word of the LORD.
en_uk_kjv~CH1~C12~24~The children of Judah that bare shield and spear [were] six thousand and eight hundred, ready armed to the war.
en_uk_kjv~CH1~C12~25~Of the children of Simeon, mighty men of valour for the war, seven thousand and one hundred.
en_uk_kjv~CH1~C12~26~Of the children of Levi four thousand and six hundred.
en_uk_kjv~CH1~C12~27~And Jehoiada [was] the leader of the Aaronites, and with him [were] three thousand and seven hundred;
en_uk_kjv~CH1~C12~28~And Zadok, a young man mighty of valour, and of his father’s house twenty and two captains.
en_uk_kjv~CH1~C12~29~And of the children of Benjamin, the kindred of Saul, three thousand: for hitherto the greatest part of them had kept the ward of the house of Saul.
en_uk_kjv~CH1~C12~30~And of the children of Ephraim twenty thousand and eight hundred, mighty men of valour, famous throughout the house of their fathers.
en_uk_kjv~CH1~C12~31~And of the half tribe of Manasseh eighteen thousand, which were expressed by name, to come and make David king.
en_uk_kjv~CH1~C12~32~And of the children of Issachar, [which were men] that had understanding of the times, to know what Israel ought to do; the heads of them [were] two hundred; and all their brethren [were] at their commandment.
en_uk_kjv~CH1~C12~33~Of Zebulun, such as went forth to battle, expert in war, with all instruments of war, fifty thousand, which could keep rank: [they were] not of double heart.
en_uk_kjv~CH1~C12~34~And of Naphtali a thousand captains, and with them with shield and spear thirty and seven thousand.
en_uk_kjv~CH1~C12~35~And of the Danites expert in war twenty and eight thousand and six hundred.
en_uk_kjv~CH1~C12~36~And of Asher, such as went forth to battle, expert in war, forty thousand.
en_uk_kjv~CH1~C12~37~And on the other side of Jordan, of the Reubenites, and the Gadites, and of the half tribe of Manasseh, with all manner of instruments of war for the battle, an hundred and twenty thousand.
en_uk_kjv~CH1~C12~38~All these men of war, that could keep rank, came with a perfect heart to Hebron, to make David king over all Israel: and all the rest also of Israel [were] of one heart to make David king.
en_uk_kjv~CH1~C12~39~And there they were with David three days, eating and drinking: for their brethren had prepared for them.
en_uk_kjv~CH1~C12~40~Moreover they that were nigh them, [even] unto Issachar and Zebulun and Naphtali, brought bread on asses, and on camels, and on mules, and on oxen, [and] meat, meal, cakes of figs, and bunches of raisins, and wine, and oil, and oxen, and sheep abundantly: for [there was] joy in Israel.
