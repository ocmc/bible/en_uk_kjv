en_uk_kjv~MAT~C22~01~And Jesus answered and spake unto them again by parables, and said,
en_uk_kjv~MAT~C22~02~The kingdom of heaven is like unto a certain king, which made a marriage for his son,
en_uk_kjv~MAT~C22~03~And sent forth his servants to call them that were bidden to the wedding: and they would not come.
en_uk_kjv~MAT~C22~04~Again, he sent forth other servants, saying, Tell them which are bidden, Behold, I have prepared my dinner: my oxen and [my] fatlings [are] killed, and all things [are] ready: come unto the marriage.
en_uk_kjv~MAT~C22~05~But they made light of [it], and went their ways, one to his farm, another to his merchandise:
en_uk_kjv~MAT~C22~06~And the remnant took his servants, and entreated them spitefully, and slew [them].
en_uk_kjv~MAT~C22~07~But when the king heard [thereof], he was wroth: and he sent forth his armies, and destroyed those murderers, and burned up their city.
en_uk_kjv~MAT~C22~08~Then saith he to his servants, The wedding is ready, but they which were bidden were not worthy.
en_uk_kjv~MAT~C22~09~Go ye therefore into the highways, and as many as ye shall find, bid to the marriage.
en_uk_kjv~MAT~C22~10~So those servants went out into the highways, and gathered together all as many as they found, both bad and good: and the wedding was furnished with guests.
en_uk_kjv~MAT~C22~11~And when the king came in to see the guests, he saw there a man which had not on a wedding garment:
en_uk_kjv~MAT~C22~12~And he saith unto him, Friend, how camest thou in hither not having a wedding garment? And he was speechless.
en_uk_kjv~MAT~C22~13~Then said the king to the servants, Bind him hand and foot, and take him away, and cast [him] into outer darkness; there shall be weeping and gnashing of teeth.
en_uk_kjv~MAT~C22~14~For many are called, but few [are] chosen.
en_uk_kjv~MAT~C22~15~Then went the Pharisees, and took counsel how they might entangle him in [his] talk.
en_uk_kjv~MAT~C22~16~And they sent out unto him their disciples with the Herodians, saying, Master, we know that thou art true, and teachest the way of God in truth, neither carest thou for any [man]: for thou regardest not the person of men.
en_uk_kjv~MAT~C22~17~Tell us therefore, What thinkest thou? Is it lawful to give tribute unto Caesar, or not?
en_uk_kjv~MAT~C22~18~But Jesus perceived their wickedness, and said, Why tempt ye me, [ye] hypocrites?
en_uk_kjv~MAT~C22~19~Shew me the tribute money. And they brought unto him a penny.
en_uk_kjv~MAT~C22~20~And he saith unto them, Whose [is] this image and superscription?
en_uk_kjv~MAT~C22~21~They say unto him, Cæsar’s. Then saith he unto them, Render therefore unto Cæsar the things which are Cæsar’s; and unto God the things that are God’s.
en_uk_kjv~MAT~C22~22~When they had heard [these words], they marvelled, and left him, and went their way.
en_uk_kjv~MAT~C22~23~The same day came to him the Sadducees, which say that there is no resurrection, and asked him,
en_uk_kjv~MAT~C22~24~Saying, Master, Moses said, If a man die, having no children, his brother shall marry his wife, and raise up seed unto his brother.
en_uk_kjv~MAT~C22~25~Now there were with us seven brethren: and the first, when he had married a wife, deceased, and, having no issue, left his wife unto his brother:
en_uk_kjv~MAT~C22~26~Likewise the second also, and the third, unto the seventh.
en_uk_kjv~MAT~C22~27~And last of all the woman died also.
en_uk_kjv~MAT~C22~28~Therefore in the resurrection whose wife shall she be of the seven? for they all had her.
en_uk_kjv~MAT~C22~29~Jesus answered and said unto them, Ye do err, not knowing the scriptures, nor the power of God.
en_uk_kjv~MAT~C22~30~For in the resurrection they neither marry, nor are given in marriage, but are as the angels of God in heaven.
en_uk_kjv~MAT~C22~31~But as touching the resurrection of the dead, have ye not read that which was spoken unto you by God, saying,
en_uk_kjv~MAT~C22~32~I am the God of Abraham, and the God of Isaac, and the God of Jacob? God is not the God of the dead, but of the living.
en_uk_kjv~MAT~C22~33~And when the multitude heard [this], they were astonished at his doctrine.
en_uk_kjv~MAT~C22~34~But when the Pharisees had heard that he had put the Sadducees to silence, they were gathered together.
en_uk_kjv~MAT~C22~35~Then one of them, [which was] a lawyer, asked [him a question], tempting him, and saying,
en_uk_kjv~MAT~C22~36~Master, which [is] the great commandment in the law?
en_uk_kjv~MAT~C22~37~Jesus said unto him, Thou shalt love the Lord thy God with all thy heart, and with all thy soul, and with all thy mind.
en_uk_kjv~MAT~C22~38~This is the first and great commandment.
en_uk_kjv~MAT~C22~39~And the second [is] like unto it, Thou shalt love thy neighbour as thyself.
en_uk_kjv~MAT~C22~40~On these two commandments hang all the law and the prophets.
en_uk_kjv~MAT~C22~41~While the Pharisees were gathered together, Jesus asked them,
en_uk_kjv~MAT~C22~42~Saying, What think ye of Christ? whose son is he? They say unto him, [The Son] of David.
en_uk_kjv~MAT~C22~43~He saith unto them, How then doth David in spirit call him Lord, saying,
en_uk_kjv~MAT~C22~44~The LORD said unto my Lord, Sit thou on my right hand, till I make thine enemies thy footstool?
en_uk_kjv~MAT~C22~45~If David then call him Lord, how is he his son?
en_uk_kjv~MAT~C22~46~And no man was able to answer him a word, neither durst any [man] from that day forth ask him any more [questions.]
