en_uk_kjv~MA1~C12~01~Now when Jonathan saw that time served him, he chose certain men, and sent them to Rome, for to confirm and renew the friendship that they had with them.
en_uk_kjv~MA1~C12~02~He sent letters also to the Lacedemonians, and to other places, for the same purpose.
en_uk_kjv~MA1~C12~03~So they went unto Rome, and entered into the senate, and said, Jonathan the high priest, and the people of the Jews, sent us unto you, to the end ye should renew the friendship, which ye had with them, and league, as in former time.
en_uk_kjv~MA1~C12~04~Upon this the Romans gave them letters unto the governors of every place that they should bring them into the land of Judea peaceably.
en_uk_kjv~MA1~C12~05~And this is the copy of the letters which Jonathan wrote to the Lacedemonians:
en_uk_kjv~MA1~C12~06~Jonathan the high priest, and the elders of the nation, and the priests, and the other of the Jews, unto the Lacedemonians their brethren send greeting:
en_uk_kjv~MA1~C12~07~There were letters sent in times past unto Onias the high priest from Darius, who reigned then among you, to signify that ye are our brethren, as the copy here underwritten doth specify.
en_uk_kjv~MA1~C12~08~At which time Onias entreated the ambassador that was sent honourably, and received the letters, wherein declaration was made of the league and friendship.
en_uk_kjv~MA1~C12~09~Therefore we also, albeit we need none of these things, that we have the holy books of scripture in our hands to comfort us,
en_uk_kjv~MA1~C12~10~Have nevertheless attempted to send unto you for the renewing of brotherhood and friendship, lest we should become strangers unto you altogether: for there is a long time passed since ye sent unto us.
en_uk_kjv~MA1~C12~11~We therefore at all times without ceasing, both in our feasts, and other convenient days, do remember you in the sacrifices which we offer, and in our prayers, as reason is, and as it becometh us to think upon our brethren:
en_uk_kjv~MA1~C12~12~And we are right glad of your honour.
en_uk_kjv~MA1~C12~13~As for ourselves, we have had great troubles and wars on every side, forsomuch as the kings that are round about us have fought against us.
en_uk_kjv~MA1~C12~14~Howbeit we would not be troublesome unto you, nor to others of our confederates and friends, in these wars:
en_uk_kjv~MA1~C12~15~For we have help from heaven that succoureth us, so as we are delivered from our enemies, and our enemies are brought under foot.
en_uk_kjv~MA1~C12~16~For this cause we chose Numenius the son of Antiochus, and Antipater the son of Jason, and sent them unto the Romans, to renew the amity that we had with them, and the former league.
en_uk_kjv~MA1~C12~17~We commanded them also to go unto you, and to salute and to deliver you our letters concerning the renewing of our brotherhood.
en_uk_kjv~MA1~C12~18~Wherefore now ye shall do well to give us an answer thereto.
en_uk_kjv~MA1~C12~19~And this is the copy of the letters which Oniares sent.
en_uk_kjv~MA1~C12~20~Areus king of the Lacedemonians to Onias the high priest, greeting:
en_uk_kjv~MA1~C12~21~It is found in writing, that the Lacedemonians and Jews are brethren, and that they are of the stock of Abraham:
en_uk_kjv~MA1~C12~22~Now therefore, since this is come to our knowledge, ye shall do well to write unto us of your prosperity.
en_uk_kjv~MA1~C12~23~We do write back again to you, that your cattle and goods are our’s, and our’s are your’s We do command therefore our ambassadors to make report unto you on this wise.
en_uk_kjv~MA1~C12~24~Now when Jonathan heard that Demebius’ princes were come to fight against him with a greater host than afore,
en_uk_kjv~MA1~C12~25~He removed from Jerusalem, and met them in the land of Amathis: for he gave them no respite to enter his country.
en_uk_kjv~MA1~C12~26~He sent spies also unto their tents, who came again, and told him that they were appointed to come upon them in the night season.
en_uk_kjv~MA1~C12~27~Wherefore so soon as the sun was down, Jonathan commanded his men to watch, and to be in arms, that all the night long they might be ready to fight: also he sent forth centinels round about the host.
en_uk_kjv~MA1~C12~28~But when the adversaries heard that Jonathan and his men were ready for battle, they feared, and trembled in their hearts, and they kindled fires in their camp.
en_uk_kjv~MA1~C12~29~Howbeit Jonathan and his company knew it not till the morning: for they saw the lights burning.
en_uk_kjv~MA1~C12~30~Then Jonathan pursued after them, but overtook them not: for they were gone over the river Eleutherus.
en_uk_kjv~MA1~C12~31~Wherefore Jonathan turned to the Arabians, who were called Zabadeans, and smote them, and took their spoils.
en_uk_kjv~MA1~C12~32~And removing thence, he came to Damascus, and so passed through all the country,
en_uk_kjv~MA1~C12~33~Simon also went forth, and passed through the country unto Ascalon, and the holds there adjoining, from whence he turned aside to Joppa, and won it.
en_uk_kjv~MA1~C12~34~For he had heard that they would deliver the hold unto them that took Demetrius’ part; wherefore he set a garrison there to keep it.
en_uk_kjv~MA1~C12~35~After this came Jonathan home again, and calling the elders of the people together, he consulted with them about building strong holds in Judea,
en_uk_kjv~MA1~C12~36~And making the walls of Jerusalem higher, and raising a great mount between the tower and the city, for to separate it from the city, that so it might be alone, that men might neither sell nor buy in it.
en_uk_kjv~MA1~C12~37~Upon this they came together to build up the city, forasmuch as part of the wall toward the brook on the east side was fallen down, and they repaired that which was called Caphenatha.
en_uk_kjv~MA1~C12~38~Simon also set up Adida in Sephela, and made it strong with gates and bars.
en_uk_kjv~MA1~C12~39~Now Tryphon went about to get the kingdom of Asia, and to kill Antiochus the king, that he might set the crown upon his own head.
en_uk_kjv~MA1~C12~40~Howbeit he was afraid that Jonathan would not suffer him, and that he would fight against him; wherefore he sought a way how to take Jonathan, that he might kill him. So he removed, and came to Bethsan.
en_uk_kjv~MA1~C12~41~Then Jonathan went out to meet him with forty thousand men chosen for the battle, and came to Bethsan.
en_uk_kjv~MA1~C12~42~Now when Tryphon saw Jonathan came with so great a force, he durst not stretch his hand against him;
en_uk_kjv~MA1~C12~43~But received him honourably, and commended him unto all his friends, and gave him gifts, and commanded his men of war to be as obedient unto him, as to himself.
en_uk_kjv~MA1~C12~44~Unto Jonathan also he said, Why hast thou brought all this people to so great trouble, seeing there is no war betwixt us?
en_uk_kjv~MA1~C12~45~Therefore send them now home again, and choose a few men to wait on thee, and come thou with me to Ptolemais, for I will give it thee, and the rest of the strong holds and forces, and all that have any charge: as for me, I will return and depart: for this is the cause of my coming.
en_uk_kjv~MA1~C12~46~So Jonathan believing him did as he bade him, and sent away his host, who went into the land of Judea.
en_uk_kjv~MA1~C12~47~And with himself he retained but three thousand men, of whom he sent two thousand into Galilee, and one thousand went with him.
en_uk_kjv~MA1~C12~48~Now as soon as Jonathan entered into Ptolemais, they of Ptolemais shut the gates and took him, and all them that came with him they slew with the sword.
en_uk_kjv~MA1~C12~49~Then sent Tryphon an host of footmen and horsemen into Galilee, and into the great plain, to destroy all Jonathan’s company.
en_uk_kjv~MA1~C12~50~But when they knew that Jonathan and they that were with him were taken and slain, they encouraged one another; and went close together, prepared to fight.
en_uk_kjv~MA1~C12~51~They therefore that followed upon them, perceiving that they were ready to fight for their lives, turned back again.
en_uk_kjv~MA1~C12~52~Whereupon they all came into the land of Judea peaceably, and there they bewailed Jonathan, and them that were with him, and they were sore afraid; wherefore all Israel made great lamentation.
en_uk_kjv~MA1~C12~53~Then all the heathen that were round about then sought to destroy them: for said they, They have no captain, nor any to help them: now therefore let us make war upon them, and take away their memorial from among men.
