en_uk_kjv~JOB~C37~01~At this also my heart trembleth, and is moved out of his place.
en_uk_kjv~JOB~C37~02~Hear attentively the noise of his voice, and the sound [that] goeth out of his mouth.
en_uk_kjv~JOB~C37~03~He directeth it under the whole heaven, and his lightning unto the ends of the earth.
en_uk_kjv~JOB~C37~04~After it a voice roareth: he thundereth with the voice of his excellency; and he will not stay them when his voice is heard.
en_uk_kjv~JOB~C37~05~God thundereth marvellously with his voice; great things doeth he, which we cannot comprehend.
en_uk_kjv~JOB~C37~06~For he saith to the snow, Be thou [on] the earth; likewise to the small rain, and to the great rain of his strength.
en_uk_kjv~JOB~C37~07~He sealeth up the hand of every man; that all men may know his work.
en_uk_kjv~JOB~C37~08~Then the beasts go into dens, and remain in their places.
en_uk_kjv~JOB~C37~09~Out of the south cometh the whirlwind: and cold out of the north.
en_uk_kjv~JOB~C37~10~By the breath of God frost is given: and the breadth of the waters is straitened.
en_uk_kjv~JOB~C37~11~Also by watering he wearieth the thick cloud: he scattereth his bright cloud:
en_uk_kjv~JOB~C37~12~And it is turned round about by his counsels: that they may do whatsoever he commandeth them upon the face of the world in the earth.
en_uk_kjv~JOB~C37~13~He causeth it to come, whether for correction, or for his land, or for mercy.
en_uk_kjv~JOB~C37~14~Hearken unto this, O Job: stand still, and consider the wondrous works of God.
en_uk_kjv~JOB~C37~15~Dost thou know when God disposed them, and caused the light of his cloud to shine?
en_uk_kjv~JOB~C37~16~Dost thou know the balancings of the clouds, the wondrous works of him which is perfect in knowledge?
en_uk_kjv~JOB~C37~17~How thy garments [are] warm, when he quieteth the earth by the south [wind]?
en_uk_kjv~JOB~C37~18~Hast thou with him spread out the sky, [which is] strong, [and] as a molten looking glass?
en_uk_kjv~JOB~C37~19~Teach us what we shall say unto him; [for] we cannot order [our speech] by reason of darkness.
en_uk_kjv~JOB~C37~20~Shall it be told him that I speak? if a man speak, surely he shall be swallowed up.
en_uk_kjv~JOB~C37~21~And now [men] see not the bright light which [is] in the clouds: but the wind passeth, and cleanseth them.
en_uk_kjv~JOB~C37~22~Fair weather cometh out of the north: with God [is] terrible majesty.
en_uk_kjv~JOB~C37~23~[Touching] the Almighty, we cannot find him out: [he is] excellent in power, and in judgment, and in plenty of justice: he will not afflict.
en_uk_kjv~JOB~C37~24~Men do therefore fear him: he respecteth not any [that are] wise of heart.
