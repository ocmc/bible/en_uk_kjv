en_uk_kjv~CH1~C01~01~Adam, Sheth, Enosh,
en_uk_kjv~CH1~C01~02~Kenan, Mahalaleel, Jered,
en_uk_kjv~CH1~C01~03~Henoch, Methuselah, Lamech,
en_uk_kjv~CH1~C01~04~Noah, Shem, Ham, and Japheth.
en_uk_kjv~CH1~C01~05~The sons of Japheth; Gomer, and Magog, and Madai, and Javan, and Tubal, and Meshech, and Tiras.
en_uk_kjv~CH1~C01~06~And the sons of Gomer; Ashchenaz, and Riphath, and Togarmah.
en_uk_kjv~CH1~C01~07~And the sons of Javan; Elishah, and Tarshish, Kittim, and Dodanim.
en_uk_kjv~CH1~C01~08~The sons of Ham; Cush, and Mizraim, Put, and Canaan.
en_uk_kjv~CH1~C01~09~And the sons of Cush; Seba, and Havilah, and Sabta, and Raamah, and Sabtecha. And the sons of Raamah; Sheba, and Dedan.
en_uk_kjv~CH1~C01~10~And Cush begat Nimrod: he began to be mighty upon the earth.
en_uk_kjv~CH1~C01~11~And Mizraim begat Ludim, and Anamim, and Lehabim, and Naphtuhim,
en_uk_kjv~CH1~C01~12~And Pathrusim, and Casluhim, (of whom came the Philistines,) and Caphthorim.
en_uk_kjv~CH1~C01~13~And Canaan begat Zidon his firstborn, and Heth,
en_uk_kjv~CH1~C01~14~The Jebusite also, and the Amorite, and the Girgashite,
en_uk_kjv~CH1~C01~15~And the Hivite, and the Arkite, and the Sinite,
en_uk_kjv~CH1~C01~16~And the Arvadite, and the Zemarite, and the Hamathite.
en_uk_kjv~CH1~C01~17~The sons of Shem; Elam, and Asshur, and Arphaxad, and Lud, and Aram, and Uz, and Hul, and Gether, and Meshech.
en_uk_kjv~CH1~C01~18~And Arphaxad begat Shelah, and Shelah begat Eber.
en_uk_kjv~CH1~C01~19~And unto Eber were born two sons: the name of the one [was] Peleg; because in his days the earth was divided: and his brother’s name [was] Joktan.
en_uk_kjv~CH1~C01~20~And Joktan begat Almodad, and Sheleph, and Hazarmaveth, and Jerah,
en_uk_kjv~CH1~C01~21~Hadoram also, and Uzal, and Diklah,
en_uk_kjv~CH1~C01~22~And Ebal, and Abimael, and Sheba,
en_uk_kjv~CH1~C01~23~And Ophir, and Havilah, and Jobab. All these [were] the sons of Joktan.
en_uk_kjv~CH1~C01~24~Shem, Arphaxad, Shelah,
en_uk_kjv~CH1~C01~25~Eber, Peleg, Reu,
en_uk_kjv~CH1~C01~26~Serug, Nahor, Terah,
en_uk_kjv~CH1~C01~27~Abram; the same [is] Abraham.
en_uk_kjv~CH1~C01~28~The sons of Abraham; Isaac, and Ishmael.
en_uk_kjv~CH1~C01~29~These [are] their generations: The firstborn of Ishmael, Nebaioth; then Kedar, and Adbeel, and Mibsam,
en_uk_kjv~CH1~C01~30~Mishma, and Dumah, Massa, Hadad, and Tema,
en_uk_kjv~CH1~C01~31~Jetur, Naphish, and Kedemah. These are the sons of Ishmael.
en_uk_kjv~CH1~C01~32~Now the sons of Keturah, Abraham’s concubine: she bare Zimran, and Jokshan, and Medan, and Midian, and Ishbak, and Shuah. And the sons of Jokshan; Sheba, and Dedan.
en_uk_kjv~CH1~C01~33~And the sons of Midian; Ephah, and Epher, and Henoch, and Abida, and Eldaah. All these [are] the sons of Keturah.
en_uk_kjv~CH1~C01~34~And Abraham begat Isaac. The sons of Isaac; Esau and Israel.
en_uk_kjv~CH1~C01~35~The sons of Esau; Eliphaz, Reuel, and Jeush, and Jaalam, and Korah.
en_uk_kjv~CH1~C01~36~The sons of Eliphaz; Teman, and Omar, Zephi, and Gatam, Kenaz, and Timna, and Amalek.
en_uk_kjv~CH1~C01~37~The sons of Reuel; Nahath, Zerah, Shammah, and Mizzah.
en_uk_kjv~CH1~C01~38~And the sons of Seir; Lotan, and Shobal, and Zibeon, and Anah, and Dishon, and Ezer, and Dishan.
en_uk_kjv~CH1~C01~39~And the sons of Lotan; Hori, and Homam: and Timna [was] Lotan’s sister.
en_uk_kjv~CH1~C01~40~The sons of Shobal; Alian, and Manahath, and Ebal, Shephi, and Onam. And the sons of Zibeon; Aiah, and Anah.
en_uk_kjv~CH1~C01~41~The sons of Anah; Dishon. And the sons of Dishon; Amram, and Eshban, and Ithran, and Cheran.
en_uk_kjv~CH1~C01~42~The sons of Ezer; Bilhan, and Zavan, [and] Jakan. The sons of Dishan; Uz, and Aran.
en_uk_kjv~CH1~C01~43~Now these [are] the kings that reigned in the land of Edom before [any] king reigned over the children of Israel; Bela the son of Beor: and the name of his city [was] Dinhabah.
en_uk_kjv~CH1~C01~44~And when Bela was dead, Jobab the son of Zerah of Bozrah reigned in his stead.
en_uk_kjv~CH1~C01~45~And when Jobab was dead, Husham of the land of the Temanites reigned in his stead.
en_uk_kjv~CH1~C01~46~And when Husham was dead, Hadad the son of Bedad, which smote Midian in the field of Moab, reigned in his stead: and the name of his city [was] Avith.
en_uk_kjv~CH1~C01~47~And when Hadad was dead, Samlah of Masrekah reigned in his stead.
en_uk_kjv~CH1~C01~48~And when Samlah was dead, Shaul of Rehoboth by the river reigned in his stead.
en_uk_kjv~CH1~C01~49~And when Shaul was dead, Baal-hanan the son of Achbor reigned in his stead.
en_uk_kjv~CH1~C01~50~And when Baal-hanan was dead, Hadad reigned in his stead: and the name of his city [was] Pai; and his wife’s name [was] Mehetabel, the daughter of Matred, the daughter of Mezahab.
en_uk_kjv~CH1~C01~51~Hadad died also. And the dukes of Edom were; duke Timnah, duke Aliah, duke Jetheth,
en_uk_kjv~CH1~C01~52~Duke Aholibamah, duke Elah, duke Pinon,
en_uk_kjv~CH1~C01~53~Duke Kenaz, duke Teman, duke Mibzar,
en_uk_kjv~CH1~C01~54~Duke Magdiel, duke Iram. These [are] the dukes of Edom.
