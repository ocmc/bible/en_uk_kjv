en_uk_kjv~SA2~C24~01~And again the anger of the LORD was kindled against Israel, and he moved David against them to say, Go, number Israel and Judah.
en_uk_kjv~SA2~C24~02~For the king said to Joab the captain of the host, which [was] with him, Go now through all the tribes of Israel, from Dan even to Beer-sheba, and number ye the people, that I may know the number of the people.
en_uk_kjv~SA2~C24~03~And Joab said unto the king, Now the LORD thy God add unto the people, how many soever they be, an hundredfold, and that the eyes of my lord the king may see [it:] but why doth my lord the king delight in this thing?
en_uk_kjv~SA2~C24~04~Notwithstanding the king’s word prevailed against Joab, and against the captains of the host. And Joab and the captains of the host went out from the presence of the king, to number the people of Israel.
en_uk_kjv~SA2~C24~05~And they passed over Jordan, and pitched in Aroer, on the right side of the city that [lieth] in the midst of the river of Gad, and toward Jazer:
en_uk_kjv~SA2~C24~06~Then they came to Gilead, and to the land of Tahtim-hodshi; and they came to Dan-jaan, and about to Zidon,
en_uk_kjv~SA2~C24~07~And came to the strong hold of Tyre, and to all the cities of the Hivites, and of the Canaanites: and they went out to the south of Judah, [even] to Beer-sheba.
en_uk_kjv~SA2~C24~08~So when they had gone through all the land, they came to Jerusalem at the end of nine months and twenty days.
en_uk_kjv~SA2~C24~09~And Joab gave up the sum of the number of the people unto the king: and there were in Israel eight hundred thousand valiant men that drew the sword; and the men of Judah [were] five hundred thousand men.
en_uk_kjv~SA2~C24~10~And David’s heart smote him after that he had numbered the people. And David said unto the LORD, I have sinned greatly in that I have done: and now, I beseech thee, O LORD, take away the iniquity of thy servant; for I have done very foolishly.
en_uk_kjv~SA2~C24~11~For when David was up in the morning, the word of the LORD came unto the prophet Gad, David’s seer, saying,
en_uk_kjv~SA2~C24~12~Go and say unto David, Thus saith the LORD, I offer thee three [things;] choose thee one of them, that I may [do it] unto thee.
en_uk_kjv~SA2~C24~13~So Gad came to David, and told him, and said unto him, Shall seven years of famine come unto thee in thy land? or wilt thou flee three months before thine enemies, while they pursue thee? or that there be three days’ pestilence in thy land? now advise, and see what answer I shall return to him that sent me.
en_uk_kjv~SA2~C24~14~And David said unto Gad, I am in a great strait: let us fall now into the hand of the LORD; for his mercies [are] great: and let me not fall into the hand of man.
en_uk_kjv~SA2~C24~15~So the LORD sent a pestilence upon Israel from the morning even to the time appointed: and there died of the people from Dan even to Beer-sheba seventy thousand men.
en_uk_kjv~SA2~C24~16~And when the angel stretched out his hand upon Jerusalem to destroy it, the LORD repented him of the evil, and said to the angel that destroyed the people, It is enough: stay now thine hand. And the angel of the LORD was by the threshingplace of Araunah the Jebusite.
en_uk_kjv~SA2~C24~17~And David spake unto the LORD when he saw the angel that smote the people, and said, Lo, I have sinned, and I have done wickedly: but these sheep, what have they done? let thine hand, I pray thee, be against me, and against my father’s house.
en_uk_kjv~SA2~C24~18~And Gad came that day to David, and said unto him, Go up, rear an altar unto the LORD in the threshingfloor of Araunah the Jebusite.
en_uk_kjv~SA2~C24~19~And David, according to the saying of Gad, went up as the LORD commanded.
en_uk_kjv~SA2~C24~20~And Araunah looked, and saw the king and his servants coming on toward him: and Araunah went out, and bowed himself before the king on his face upon the ground.
en_uk_kjv~SA2~C24~21~And Araunah said, Wherefore is my lord the king come to his servant? And David said, To buy the threshingfloor of thee, to build an altar unto the LORD, that the plague may be stayed from the people.
en_uk_kjv~SA2~C24~22~And Araunah said unto David, Let my lord the king take and offer up what [seemeth] good unto him: behold, [here be] oxen for burnt sacrifice, and threshing instruments and [other] instruments of the oxen for wood.
en_uk_kjv~SA2~C24~23~All these [things] did Araunah, [as] a king, give unto the king. And Araunah said unto the king, The LORD thy God accept thee.
en_uk_kjv~SA2~C24~24~And the king said unto Araunah, Nay; but I will surely buy [it] of thee at a price: neither will I offer burnt offerings unto the LORD my God of that which doth cost me nothing. So David bought the threshingfloor and the oxen for fifty shekels of silver.
en_uk_kjv~SA2~C24~25~And David built there an altar unto the LORD, and offered burnt offerings and peace offerings. So the LORD was intreated for the land, and the plague was stayed from Israel.
