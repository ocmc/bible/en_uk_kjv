en_uk_kjv~PSA~C128~001~(C129:001) (0) A Song of degrees. (1) Many a time have they afflicted me from my youth, may Israel now say:
en_uk_kjv~PSA~C128~002~(C129:002) Many a time have they afflicted me from my youth: yet they have not prevailed against me.
en_uk_kjv~PSA~C128~003~(C129:003) The plowers plowed upon my back: they made long their furrows.
en_uk_kjv~PSA~C128~004~(C129:004) The LORD [is] righteous: he hath cut asunder the cords of the wicked.
en_uk_kjv~PSA~C128~005~(C129:005) Let them all be confounded and turned back that hate Zion.
en_uk_kjv~PSA~C128~006~(C129:006) Let them be as the grass [upon] the housetops, which withereth afore it groweth up:
en_uk_kjv~PSA~C128~007~(C129:007) Wherewith the mower filleth not his hand; nor he that bindeth sheaves his bosom.
en_uk_kjv~PSA~C128~008~(C129:008) Neither do they which go by say, The blessing of the LORD [be] upon you: we bless you in the name of the LORD.
