en_uk_kjv~PSA~C058~001~(C059:000) To the chief Musician, Al-taschith, Michtam of David; when Saul sent, and they watched the house to kill him.
en_uk_kjv~PSA~C058~002~(C059:001) Deliver me from mine enemies, O my God: defend me from them that rise up against me.
en_uk_kjv~PSA~C058~003~(C059:002) Deliver me from the workers of iniquity, and save me from bloody men.
en_uk_kjv~PSA~C058~004~(C059:003) For, lo, they lie in wait for my soul: the mighty are gathered against me; not [for] my transgression, nor [for] my sin, O LORD.
en_uk_kjv~PSA~C058~005~(C059:004) They run and prepare themselves without [my] fault: awake to help me, and behold
en_uk_kjv~PSA~C058~006~(C059:005) Thou therefore, O LORD God of hosts, the God of Israel, awake to visit all the heathen: be not merciful to any wicked transgressors. Selah.
en_uk_kjv~PSA~C058~007~(C059:006) They return at evening: they make a noise like a dog, and go round about the city.
en_uk_kjv~PSA~C058~008~(C059:007) Behold, they belch out with their mouth: swords [are] in their lips: for who, [say they], doth hear?
en_uk_kjv~PSA~C058~009~(C059:008) But thou, O LORD, shalt laugh at them; thou shalt have all the heathen in derision.
en_uk_kjv~PSA~C058~010~(C059:009) [Because of] his strength will I wait upon thee: for God [is] my defence.
en_uk_kjv~PSA~C058~011~(C059:010) The God of my mercy shall prevent me: God shall let me see [my desire] upon mine enemies.
en_uk_kjv~PSA~C058~012~(C059:011) Slay them not, lest my people forget: scatter them by thy power; and bring them down, O Lord our shield.
en_uk_kjv~PSA~C058~013~(C059:012) [For] the sin of their mouth [and] the words of their lips let them even be taken in their pride: and for cursing and lying [which] they speak.
en_uk_kjv~PSA~C058~014~(C059:013) Consume [them] in wrath, consume [them], that they [may] not [be:] and let them know that God ruleth in Jacob unto the ends of the earth. Selah.
en_uk_kjv~PSA~C058~015~(C059:014) And at evening let them return; [and] let them make a noise like a dog, and go round about the city.
en_uk_kjv~PSA~C058~016~(C059:015) Let them wander up and down for meat, and grudge if they be not satisfied.
en_uk_kjv~PSA~C058~017~(C059:016) But I will sing of thy power; yea, I will sing aloud of thy mercy in the morning: for thou hast been my defence and refuge in the day of my trouble.
en_uk_kjv~PSA~C058~018~(C059:017) Unto thee, O my strength, will I sing: for God [is] my defence, [and] the God of my mercy.
