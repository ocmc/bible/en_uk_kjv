en_uk_kjv~SIR~C02~01~My son, if thou come to serve the Lord, prepare thy soul for temptation.
en_uk_kjv~SIR~C02~02~Set thy heart aright, and constantly endure, and make not haste in time of trouble.
en_uk_kjv~SIR~C02~03~Cleave unto him, and depart not away, that thou mayest be increased at thy last end.
en_uk_kjv~SIR~C02~04~Whatsoever is brought upon thee take cheerfully, and be patient when thou art changed to a low estate.
en_uk_kjv~SIR~C02~05~For gold is tried in the fire, and acceptable men in the furnace of adversity.
en_uk_kjv~SIR~C02~06~Believe in him, and he will help thee; order thy way aright, and trust in him.
en_uk_kjv~SIR~C02~07~Ye that fear the Lord, wait for his mercy; and go not aside, lest ye fall.
en_uk_kjv~SIR~C02~08~Ye that fear the Lord, believe him; and your reward shall not fail.
en_uk_kjv~SIR~C02~09~Ye that fear the Lord, hope for good, and for everlasting joy and mercy.
en_uk_kjv~SIR~C02~10~Look at the generations of old, and see; did ever any trust in the Lord, and was confounded? or did any abide in his fear, and was forsaken? or whom did he ever despise, that called upon him?
en_uk_kjv~SIR~C02~11~For the Lord is full of compassion and mercy, longsuffering, and very pitiful, and forgiveth sins, and saveth in time of affliction.
en_uk_kjv~SIR~C02~12~Woe be to fearful hearts, and faint hands, and the sinner that goeth two ways!
en_uk_kjv~SIR~C02~13~Woe unto him that is fainthearted! for he believeth not; therefore shall he not be defended.
en_uk_kjv~SIR~C02~14~Woe unto you that have lost patience! and what will ye do when the Lord shall visit you?
en_uk_kjv~SIR~C02~15~They that fear the Lord will not disobey his Word; and they that love him will keep his ways.
en_uk_kjv~SIR~C02~16~They that fear the Lord will seek that which is well, pleasing unto him; and they that love him shall be filled with the law.
en_uk_kjv~SIR~C02~17~They that fear the Lord will prepare their hearts, and humble their souls in his sight,
en_uk_kjv~SIR~C02~18~Saying, We will fall into the hands of the Lord, and not into the hands of men: for as his majesty is, so is his mercy.
