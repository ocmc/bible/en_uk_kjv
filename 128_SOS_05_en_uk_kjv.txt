en_uk_kjv~SOS~C05~01~I am come into my garden, my sister, [my] spouse: I have gathered my myrrh with my spice; I have eaten my honeycomb with my honey; I have drunk my wine with my milk: eat, O friends; drink, yea, drink abundantly, O beloved.
en_uk_kjv~SOS~C05~02~I sleep, but my heart waketh: [it is] the voice of my beloved that knocketh, [saying], Open to me, my sister, my love, my dove, my undefiled: for my head is filled with dew, [and] my locks with the drops of the night.
en_uk_kjv~SOS~C05~03~I have put off my coat; how shall I put it on? I have washed my feet; how shall I defile them?
en_uk_kjv~SOS~C05~04~My beloved put in his hand by the hole [of the door], and my bowels were moved for him.
en_uk_kjv~SOS~C05~05~I rose up to open to my beloved; and my hands dropped [with] myrrh, and my fingers [with] sweet smelling myrrh, upon the handles of the lock.
en_uk_kjv~SOS~C05~06~I opened to my beloved; but my beloved had withdrawn himself, [and] was gone: my soul failed when he spake: I sought him, but I could not find him; I called him, but he gave me no answer.
en_uk_kjv~SOS~C05~07~The watchmen that went about the city found me, they smote me, they wounded me; the keepers of the walls took away my veil from me.
en_uk_kjv~SOS~C05~08~I charge you, O daughters of Jerusalem, if ye find my beloved, that ye tell him, that I [am] sick of love.
en_uk_kjv~SOS~C05~09~What [is] thy beloved more than [another] beloved, O thou fairest among women? what [is] thy beloved more than [another] beloved, that thou dost so charge us?
en_uk_kjv~SOS~C05~10~My beloved [is] white and ruddy, the chiefest among ten thousand.
en_uk_kjv~SOS~C05~11~His head [is as] the most fine gold, his locks [are] bushy, [and] black as a raven.
en_uk_kjv~SOS~C05~12~His eyes [are] as [the eyes] of doves by the rivers of waters, washed with milk, [and] fitly set.
en_uk_kjv~SOS~C05~13~His cheeks [are] as a bed of spices, [as] sweet flowers: his lips [like] lilies, dropping sweet smelling myrrh.
en_uk_kjv~SOS~C05~14~His hands [are as] gold rings set with the beryl: his belly [is as] bright ivory overlaid [with] sapphires.
en_uk_kjv~SOS~C05~15~His legs [are as] pillars of marble, set upon sockets of fine gold: his countenance [is] as Lebanon, excellent as the cedars.
en_uk_kjv~SOS~C05~16~His mouth [is] most sweet: yea, he [is] altogether lovely. This [is] my beloved, and this [is] my friend, O daughters of Jerusalem.
