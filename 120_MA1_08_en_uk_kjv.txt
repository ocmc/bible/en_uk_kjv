en_uk_kjv~MA1~C08~01~Now Judas had heard of the Romans, that they were mighty and valiant men, and such as would lovingly accept all that joined themselves unto them, and make a league of amity with all that came unto them;
en_uk_kjv~MA1~C08~02~And that they were men of great valour. It was told him also of their wars and noble acts which they had done among the Galatians, and how they had conquered them, and brought them under tribute;
en_uk_kjv~MA1~C08~03~And what they had done in the country of Spain, for the winning of the mines of the silver and gold which is there;
en_uk_kjv~MA1~C08~04~And that by their policy and patience they had conquered all the place, though it were very far from them; and the kings also that came against them from the uttermost part of the earth, till they had discomfited them, and given them a great overthrow, so that the rest did give them tribute every year:
en_uk_kjv~MA1~C08~05~Beside this, how they had discomfited in battle Philip, and Perseus, king of the Citims, with others that lifted up themselves against them, and had overcome them:
en_uk_kjv~MA1~C08~06~How also Antiochus the great king of Asia, that came against them in battle, having an hundred and twenty elephants, with horsemen, and chariots, and a very great army, was discomfited by them;
en_uk_kjv~MA1~C08~07~And how they took him alive, and covenanted that he and such as reigned after him should pay a great tribute, and give hostages, and that which was agreed upon,
en_uk_kjv~MA1~C08~08~And the country of India, and Media and Lydia and of the goodliest countries, which they took of him, and gave to king Eumenes:
en_uk_kjv~MA1~C08~09~Moreover how the Grecians had determined to come and destroy them;
en_uk_kjv~MA1~C08~10~And that they, having knowledge thereof sent against them a certain captain, and fighting with them slew many of them, and carried away captives their wives and their children, and spoiled them, and took possession of their lands, and pulled down their strong holds, and brought them to be their servants unto this day:
en_uk_kjv~MA1~C08~11~It was told him besides, how they destroyed and brought under their dominion all other kingdoms and isles that at any time resisted them;
en_uk_kjv~MA1~C08~12~But with their friends and such as relied upon them they kept amity: and that they had conquered kingdoms both far and nigh, insomuch as all that heard of their name were afraid of them:
en_uk_kjv~MA1~C08~13~Also that, whom they would help to a kingdom, those reign; and whom again they would, they displace: finally, that they were greatly exalted:
en_uk_kjv~MA1~C08~14~Yet for all this none of them wore a crown or was clothed in purple, to be magnified thereby:
en_uk_kjv~MA1~C08~15~Moreover how they had made for themselves a senate house, wherein three hundred and twenty men sat in council daily, consulting alway for the people, to the end they might be well ordered:
en_uk_kjv~MA1~C08~16~And that they committed their government to one man every year, who ruled over all their country, and that all were obedient to that one, and that there was neither envy nor emmulation among them.
en_uk_kjv~MA1~C08~17~In consideration of these things, Judas chose Eupolemus the son of John, the son of Accos, and Jason the son of Eleazar, and sent them to Rome, to make a league of amity and confederacy with them,
en_uk_kjv~MA1~C08~18~And to intreat them that they would take the yoke from them; for they saw that the kingdom of the Grecians did oppress Israel with servitude.
en_uk_kjv~MA1~C08~19~They went therefore to Rome, which was a very great journey, and came into the senate, where they spake and said.
en_uk_kjv~MA1~C08~20~Judas Maccabeus with his brethren, and the people of the Jews, have sent us unto you, to make a confederacy and peace with you, and that we might be registered your confederates and friends.
en_uk_kjv~MA1~C08~21~So that matter pleased the Romans well.
en_uk_kjv~MA1~C08~22~And this is the copy of the epistle which the senate wrote back again in tables of brass, and sent to Jerusalem, that there they might have by them a memorial of peace and confederacy:
en_uk_kjv~MA1~C08~23~Good success be to the Romans, and to the people of the Jews, by sea and by land for ever: the sword also and enemy be far from them,
en_uk_kjv~MA1~C08~24~If there come first any war upon the Romans or any of their confederates throughout all their dominion,
en_uk_kjv~MA1~C08~25~The people of the Jews shall help them, as the time shall be appointed, with all their heart:
en_uk_kjv~MA1~C08~26~Neither shall they give any thing unto them that make war upon them, or aid them with victuals, weapons, money, or ships, as it hath seemed good unto the Romans; but they shall keep their covenants without taking any thing therefore.
en_uk_kjv~MA1~C08~27~In the same manner also, if war come first upon the nation of the Jews, the Romans shall help them with all their heart, according as the time shall be appointed them:
en_uk_kjv~MA1~C08~28~Neither shall victuals be given to them that take part against them, or weapons, or money, or ships, as it hath seemed good to the Romans; but they shall keep their covenants, and that without deceit.
en_uk_kjv~MA1~C08~29~According to these articles did the Romans make a covenant with the people of the Jews.
en_uk_kjv~MA1~C08~30~Howbeit if hereafter the one party or the other shall think to meet to add or diminish any thing, they may do it at their pleasures, and whatsoever they shall add or take away shall be ratified.
en_uk_kjv~MA1~C08~31~And as touching the evils that Demetrius doeth to the Jews, we have written unto him, saying, Wherefore thou made thy yoke heavy upon our friends and confederates the Jews?
en_uk_kjv~MA1~C08~32~If therefore they complain any more against thee, we will do them justice, and fight with thee by sea and by land.
