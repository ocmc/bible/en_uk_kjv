en_uk_kjv~SA1~C18~01~And it came to pass, when he had made an end of speaking unto Saul, that the soul of Jonathan was knit with the soul of David, and Jonathan loved him as his own soul.
en_uk_kjv~SA1~C18~02~And Saul took him that day, and would let him go no more home to his father’s house.
en_uk_kjv~SA1~C18~03~Then Jonathan and David made a covenant, because he loved him as his own soul.
en_uk_kjv~SA1~C18~04~And Jonathan stripped himself of the robe that [was] upon him, and gave it to David, and his garments, even to his sword, and to his bow, and to his girdle.
en_uk_kjv~SA1~C18~05~And David went out whithersoever Saul sent him, [and] behaved himself wisely: and Saul set him over the men of war, and he was accepted in the sight of all the people, and also in the sight of Saul’s servants.
en_uk_kjv~SA1~C18~06~And it came to pass as they came, when David was returned from the slaughter of the Philistine, that the women came out of all cities of Israel, singing and dancing, to meet king Saul, with tabrets, with joy, and with instruments of musick.
en_uk_kjv~SA1~C18~07~And the women answered [one another] as they played, and said, Saul hath slain his thousands, and David his ten thousands.
en_uk_kjv~SA1~C18~08~And Saul was very wroth, and the saying displeased him; and he said, They have ascribed unto David ten thousands, and to me they have ascribed [but] thousands: and [what] can he have more but the kingdom?
en_uk_kjv~SA1~C18~09~And Saul eyed David from that day and forward.
en_uk_kjv~SA1~C18~10~And it came to pass on the morrow, that the evil spirit from God came upon Saul, and he prophesied in the midst of the house: and David played with his hand, as at other times: and [there was] a javelin in Saul’s hand.
en_uk_kjv~SA1~C18~11~And Saul cast the javelin; for he said, I will smite David even to the wall [with it]. And David avoided out of his presence twice.
en_uk_kjv~SA1~C18~12~And Saul was afraid of David, because the LORD was with him, and was departed from Saul.
en_uk_kjv~SA1~C18~13~Therefore Saul removed him from him, and made him his captain over a thousand; and he went out and came in before the people.
en_uk_kjv~SA1~C18~14~And David behaved himself wisely in all his ways; and the LORD [was] with him.
en_uk_kjv~SA1~C18~15~Wherefore when Saul saw that he behaved himself very wisely, he was afraid of him.
en_uk_kjv~SA1~C18~16~But all Israel and Judah loved David, because he went out and came in before them.
en_uk_kjv~SA1~C18~17~And Saul said to David, Behold my elder daughter Merab, her will I give thee to wife: only be thou valiant for me, and fight the LORD’s battles. For Saul said, Let not mine hand be upon him, but let the hand of the Philistines be upon him.
en_uk_kjv~SA1~C18~18~And David said unto Saul, Who [am] I? and what [is] my life, [or] my father’s family in Israel, that I should be son in law to the king?
en_uk_kjv~SA1~C18~19~But it came to pass at the time when Merab Saul’s daughter should have been given to David, that she was given unto Adriel the Meholathite to wife.
en_uk_kjv~SA1~C18~20~And Michal Saul’s daughter loved David: and they told Saul, and the thing pleased him.
en_uk_kjv~SA1~C18~21~And Saul said, I will give him her, that she may be a snare to him, and that the hand of the Philistines may be against him. Wherefore Saul said to David, Thou shalt this day be my son in law in [the one of] the twain.
en_uk_kjv~SA1~C18~22~And Saul commanded his servants, [saying], Commune with David secretly, and say, Behold, the king hath delight in thee, and all his servants love thee: now therefore be the king’s son in law.
en_uk_kjv~SA1~C18~23~And Saul’s servants spake those words in the ears of David. And David said, Seemeth it to you [a] light [thing] to be a king’s son in law, seeing that I [am] a poor man, and lightly esteemed?
en_uk_kjv~SA1~C18~24~And the servants of Saul told him, saying, On this manner spake David.
en_uk_kjv~SA1~C18~25~And Saul said, Thus shall ye say to David, The king desireth not any dowry, but an hundred foreskins of the Philistines, to be avenged of the king’s enemies. But Saul thought to make David fall by the hand of the Philistines.
en_uk_kjv~SA1~C18~26~And when his servants told David these words, it pleased David well to be the king’s son in law: and the days were not expired.
en_uk_kjv~SA1~C18~27~Wherefore David arose and went, he and his men, and slew of the Philistines two hundred men; and David brought their foreskins, and they gave them in full tale to the king, that he might be the king’s son in law. And Saul gave him Michal his daughter to wife.
en_uk_kjv~SA1~C18~28~And Saul saw and knew that the LORD [was] with David, and [that] Michal Saul’s daughter loved him.
en_uk_kjv~SA1~C18~29~And Saul was yet the more afraid of David; and Saul became David’s enemy continually.
en_uk_kjv~SA1~C18~30~Then the princes of the Philistines went forth: and it came to pass, after they went forth, [that] David behaved himself more wisely than all the servants of Saul; so that his name was much set by.
