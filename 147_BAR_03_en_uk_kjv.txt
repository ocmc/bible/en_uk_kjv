en_uk_kjv~BAR~C03~01~O Lord Almighty, God of Israel, the soul in anguish the troubled spirit, crieth unto thee.
en_uk_kjv~BAR~C03~02~Hear, O Lord, and have mercy; for thou art merciful: and have pity upon us, because we have sinned before thee.
en_uk_kjv~BAR~C03~03~For thou endurest for ever, and we perish utterly.
en_uk_kjv~BAR~C03~04~O Lord Almighty, thou God of Israel, hear now the prayers of the dead Israelites, and of their children, which have sinned before thee, and not hearkened unto the voice of thee their God: for the which cause these plagues cleave unto us.
en_uk_kjv~BAR~C03~05~Remember not the iniquities of our forefathers: but think upon thy power and thy name now at this time.
en_uk_kjv~BAR~C03~06~For thou art the Lord our God, and thee, O Lord, will we praise.
en_uk_kjv~BAR~C03~07~And for this cause thou hast put thy fear in our hearts, to the intent that we should call upon thy name, and praise thee in our captivity: for we have called to mind all the iniquity of our forefathers, that sinned before thee.
en_uk_kjv~BAR~C03~08~Behold, we are yet this day in our captivity, where thou hast scattered us, for a reproach and a curse, and to be subject to payments, according to all the iniquities of our fathers, which departed from the Lord our God.
en_uk_kjv~BAR~C03~09~Hear, Israel, the commandments of life: give ear to understand wisdom.
en_uk_kjv~BAR~C03~10~How happeneth it Israel, that thou art in thine enemies’ land, that thou art waxen old in a strange country, that thou art defiled with the dead,
en_uk_kjv~BAR~C03~11~That thou art counted with them that go down into the grave?
en_uk_kjv~BAR~C03~12~Thou hast forsaken the fountain of wisdom.
en_uk_kjv~BAR~C03~13~For if thou hadst walked in the way of God, thou shouldest have dwelled in peace for ever.
en_uk_kjv~BAR~C03~14~Learn where is wisdom, where is strength, where is understanding; that thou mayest know also where is length of days, and life, where is the light of the eyes, and peace.
en_uk_kjv~BAR~C03~15~Who hath found out her place? or who hath come into her treasures?
en_uk_kjv~BAR~C03~16~Where are the princes of the heathen become, and such as ruled the beasts upon the earth;
en_uk_kjv~BAR~C03~17~They that had their pastime with the fowls of the air, and they that hoarded up silver and gold, wherein men trust, and made no end of their getting?
en_uk_kjv~BAR~C03~18~For they that wrought in silver, and were so careful, and whose works are unsearchable,
en_uk_kjv~BAR~C03~19~They are vanished and gone down to the grave, and others are come up in their steads.
en_uk_kjv~BAR~C03~20~Young men have seen light, and dwelt upon the earth: but the way of knowledge have they not known,
en_uk_kjv~BAR~C03~21~Nor understood the paths thereof, nor laid hold of it: their children were far off from that way.
en_uk_kjv~BAR~C03~22~It hath not been heard of in Chanaan, neither hath it been seen in Theman.
en_uk_kjv~BAR~C03~23~The Agarenes that seek wisdom upon earth, the merchants of Meran and of Theman, the authors of fables, and searchers out of understanding; none of these have known the way of wisdom, or remember her paths.
en_uk_kjv~BAR~C03~24~O Israel, how great is the house of God! and how large is the place of his possession!
en_uk_kjv~BAR~C03~25~Great, and hath none end; high, and unmeasurable.
en_uk_kjv~BAR~C03~26~There were the giants famous from the beginning, that were of so great stature, and so expert in war.
en_uk_kjv~BAR~C03~27~Those did not the Lord choose, neither gave he the way of knowledge unto them:
en_uk_kjv~BAR~C03~28~But they were destroyed, because they had no wisdom, and perished through their own foolishness.
en_uk_kjv~BAR~C03~29~Who hath gone up into heaven, and taken her, and brought her down from the clouds?
en_uk_kjv~BAR~C03~30~Who hath gone over the sea, and found her, and will bring her for pure gold?
en_uk_kjv~BAR~C03~31~No man knoweth her way, nor thinketh of her path.
en_uk_kjv~BAR~C03~32~But he that knoweth all things knoweth her, and hath found her out with his understanding: he that prepared the earth for evermore hath filled it with fourfooted beasts:
en_uk_kjv~BAR~C03~33~He that sendeth forth light, and it goeth, calleth it again, and it obeyeth him with fear.
en_uk_kjv~BAR~C03~34~The stars shined in their watches, and rejoiced: when he calleth them, they say, Here we be; and so with cheerfulness they shewed light unto him that made them.
en_uk_kjv~BAR~C03~35~This is our God, and there shall none other be accounted of in comparison of him
en_uk_kjv~BAR~C03~36~He hath found out all the way of knowledge, and hath given it unto Jacob his servant, and to Israel his beloved.
en_uk_kjv~BAR~C03~37~Afterward did he shew himself upon earth, and conversed with men.
