en_uk_kjv~SIR~C41~01~O death, how bitter is the remembrance of thee to a man that liveth at rest in his possessions, unto the man that hath nothing to vex him, and that hath prosperity in all things: yea, unto him that is yet able to receive meat!
en_uk_kjv~SIR~C41~02~O death, acceptable is thy sentence unto the needy, and unto him whose strength faileth, that is now in the last age, and is vexed with all things, and to him that despaireth, and hath lost patience!
en_uk_kjv~SIR~C41~03~Fear not the sentence of death, remember them that have been before thee, and that come after; for this is the sentence of the Lord over all flesh.
en_uk_kjv~SIR~C41~04~And why art thou against the pleasure of the most High? there is no inquisition in the grave, whether thou have lived ten, or an hundred, or a thousand years.
en_uk_kjv~SIR~C41~05~The children of sinners are abominable children, and they that are conversant in the dwelling of the ungodly.
en_uk_kjv~SIR~C41~06~The inheritance of sinners’ children shall perish, and their posterity shall have a perpetual reproach.
en_uk_kjv~SIR~C41~07~The children will complain of an ungodly father, because they shall be reproached for his sake.
en_uk_kjv~SIR~C41~08~Woe be unto you, ungodly men, which have forsaken the law of the most high God! for if ye increase, it shall be to your destruction:
en_uk_kjv~SIR~C41~09~And if ye be born, ye shall be born to a curse: and if ye die, a curse shall be your portion.
en_uk_kjv~SIR~C41~10~All that are of the earth shall turn to earth again: so the ungodly shall go from a curse to destruction.
en_uk_kjv~SIR~C41~11~The mourning of men is about their bodies: but an ill name of sinners shall be blotted out.
en_uk_kjv~SIR~C41~12~Have regard to thy name; for that shall continue with thee above a thousand great treasures of gold.
en_uk_kjv~SIR~C41~13~A good life hath but few days: but a good name endureth for ever.
en_uk_kjv~SIR~C41~14~My children, keep discipline in peace: for wisdom that is hid, and a treasure that is not seen, what profit is in them both?
en_uk_kjv~SIR~C41~15~A man that hideth his foolishness is better than a man that hideth his wisdom.
en_uk_kjv~SIR~C41~16~Therefore be shamefaced according to my word: for it is not good to retain all shamefacedness; neither is it altogether approved in every thing.
en_uk_kjv~SIR~C41~17~Be ashamed of whoredom before father and mother: and of a lie before a prince and a mighty man;
en_uk_kjv~SIR~C41~18~Of an offence before a judge and ruler; of iniquity before a congregation and people; of unjust dealing before thy partner and friend;
en_uk_kjv~SIR~C41~19~And of theft in regard of the place where thou sojournest, and in regard of the truth of God and his covenant; and to lean with thine elbow upon the meat; and of scorning to give and take;
en_uk_kjv~SIR~C41~20~And of silence before them that salute thee; and to look upon an harlot;
en_uk_kjv~SIR~C41~21~And to turn away thy face from thy kinsman; or to take away a portion or a gift; or to gaze upon another man’s wife.
en_uk_kjv~SIR~C41~22~Or to be overbusy with his maid, and come not near her bed; or of upbraiding speeches before friends; and after thou hast given, upbraid not;
en_uk_kjv~SIR~C41~23~Or of iterating and speaking again that which thou hast heard; and of revealing of secrets.
en_uk_kjv~SIR~C41~24~So shalt thou be truly shamefaced and find favour before all men.
