en_uk_kjv~JOB~C23~01~Then Job answered and said,
en_uk_kjv~JOB~C23~02~Even to day [is] my complaint bitter: my stroke is heavier than my groaning.
en_uk_kjv~JOB~C23~03~Oh that I knew where I might find him! [that] I might come [even] to his seat!
en_uk_kjv~JOB~C23~04~I would order [my] cause before him, and fill my mouth with arguments.
en_uk_kjv~JOB~C23~05~I would know the words [which] he would answer me, and understand what he would say unto me.
en_uk_kjv~JOB~C23~06~Will he plead against me with [his] great power? No; but he would put [strength] in me.
en_uk_kjv~JOB~C23~07~There the righteous might dispute with him; so should I be delivered for ever from my judge.
en_uk_kjv~JOB~C23~08~Behold, I go forward, but he [is] not [there;] and backward, but I cannot perceive him:
en_uk_kjv~JOB~C23~09~On the left hand, where he doth work, but I cannot behold [him:] he hideth himself on the right hand, that I cannot see [him:]
en_uk_kjv~JOB~C23~10~But he knoweth the way that I take: [when] he hath tried me, I shall come forth as gold.
en_uk_kjv~JOB~C23~11~My foot hath held his steps, his way have I kept, and not declined.
en_uk_kjv~JOB~C23~12~Neither have I gone back from the commandment of his lips; I have esteemed the words of his mouth more than my necessary [food].
en_uk_kjv~JOB~C23~13~But he [is] in one [mind], and who can turn him? and [what] his soul desireth, even [that] he doeth.
en_uk_kjv~JOB~C23~14~For he performeth [the thing that is] appointed for me: and many such [things are] with him.
en_uk_kjv~JOB~C23~15~Therefore am I troubled at his presence: when I consider, I am afraid of him.
en_uk_kjv~JOB~C23~16~For God maketh my heart soft, and the Almighty troubleth me:
en_uk_kjv~JOB~C23~17~Because I was not cut off before the darkness, [neither] hath he covered the darkness from my face.
