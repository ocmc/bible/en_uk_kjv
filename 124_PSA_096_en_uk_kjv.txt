en_uk_kjv~PSA~C096~001~(C097:001) The LORD reigneth; let the earth rejoice; let the multitude of isles be glad [thereof].
en_uk_kjv~PSA~C096~002~(C097:002) Clouds and darkness [are] round about him: righteousness and judgment [are] the habitation of his throne.
en_uk_kjv~PSA~C096~003~(C097:003) A fire goeth before him, and burneth up his enemies round about.
en_uk_kjv~PSA~C096~004~(C097:004) His lightnings enlightened the world: the earth saw, and trembled.
en_uk_kjv~PSA~C096~005~(C097:005) The hills melted like wax at the presence of the LORD, at the presence of the Lord of the whole earth.
en_uk_kjv~PSA~C096~006~(C097:006) The heavens declare his righteousness, and all the people see his glory.
en_uk_kjv~PSA~C096~007~(C097:007) Confounded be all they that serve graven images, that boast themselves of idols: worship him, all [ye] gods.
en_uk_kjv~PSA~C096~008~(C097:008) Zion heard, and was glad; and the daughters of Judah rejoiced because of thy judgments, O LORD.
en_uk_kjv~PSA~C096~009~(C097:009) For thou, LORD, [art] high above all the earth: thou art exalted far above all gods.
en_uk_kjv~PSA~C096~010~(C097:010) Ye that love the LORD, hate evil: he preserveth the souls of his saints; he delivereth them out of the hand of the wicked.
en_uk_kjv~PSA~C096~011~(C097:011) Light is sown for the righteous, and gladness for the upright in heart.
en_uk_kjv~PSA~C096~012~(C097:012) Rejoice in the LORD, ye righteous; and give thanks at the remembrance of his holiness.
