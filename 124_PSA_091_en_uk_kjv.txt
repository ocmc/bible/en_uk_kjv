en_uk_kjv~PSA~C091~001~(C092:000) A Psalm [or] Song for the sabbath day.
en_uk_kjv~PSA~C091~002~(C092:001) [It is a] good [thing] to give thanks unto the LORD, and to sing praises unto thy name, O most High:
en_uk_kjv~PSA~C091~003~(C092:002) To shew forth thy lovingkindness in the morning, and thy faithfulness every night,
en_uk_kjv~PSA~C091~004~(C092:003) Upon an instrument of ten strings, and upon the psaltery; upon the harp with a solemn sound.
en_uk_kjv~PSA~C091~005~(C092:004) For thou, LORD, hast made me glad through thy work: I will triumph in the works of thy hands.
en_uk_kjv~PSA~C091~006~(C092:005) O LORD, how great are thy works! [and] thy thoughts are very deep.
en_uk_kjv~PSA~C091~007~(C092:006) A brutish man knoweth not; neither doth a fool understand this.
en_uk_kjv~PSA~C091~008~(C092:007) When the wicked spring as the grass, and when all the workers of iniquity do flourish; [it is] that they shall be destroyed for ever:
en_uk_kjv~PSA~C091~009~(C092:008) But thou, LORD, [art most] high for evermore.
en_uk_kjv~PSA~C091~010~(C092:009) For, lo, thine enemies, O LORD, for, lo, thine enemies shall perish; all the workers of iniquity shall be scattered.
en_uk_kjv~PSA~C091~011~(C092:010) But my horn shalt thou exalt like [the horn of] an unicorn: I shall be anointed with fresh oil.
en_uk_kjv~PSA~C091~012~(C092:011) Mine eye also shall see [my desire] on mine enemies, [and] mine ears shall hear [my desire] of the wicked that rise up against me.
en_uk_kjv~PSA~C091~013~(C092:012) The righteous shall flourish like the palm tree: he shall grow like a cedar in Lebanon.
en_uk_kjv~PSA~C091~014~(C092:013) Those that be planted in the house of the LORD shall flourish in the courts of our God.
en_uk_kjv~PSA~C091~015~(C092:014) They shall still bring forth fruit in old age; they shall be fat and flourishing;
en_uk_kjv~PSA~C091~016~(C092:015) To shew that the LORD [is] upright: [he is] my rock, and [there is] no unrighteousness in him.
