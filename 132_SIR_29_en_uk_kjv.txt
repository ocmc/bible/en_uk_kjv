en_uk_kjv~SIR~C29~01~He that is merciful will lend unto his neighbour; and he that strengtheneth his hand keepeth the commandments.
en_uk_kjv~SIR~C29~02~Lend to thy neighbour in time of his need, and pay thou thy neighbour again in due season.
en_uk_kjv~SIR~C29~03~Keep thy word, and deal faithfully with him, and thou shalt always find the thing that is necessary for thee.
en_uk_kjv~SIR~C29~04~Many, when a thing was lent them, reckoned it to be found, and put them to trouble that helped them.
en_uk_kjv~SIR~C29~05~Till he hath received, he will kiss a man’s hand; and for his neighbour’s money he will speak submissly: but when he should repay, he will prolong the time, and return words of grief, and complain of the time.
en_uk_kjv~SIR~C29~06~If he prevail, he shall hardly receive the half, and he will count as if he had found it: if not, he hath deprived him of his money, and he hath gotten him an enemy without cause: he payeth him with cursings and railings; and for honour he will pay him disgrace.
en_uk_kjv~SIR~C29~07~Many therefore have refused to lend for other men’s ill dealing, fearing to be defrauded.
en_uk_kjv~SIR~C29~08~Yet have thou patience with a man in poor estate, and delay not to shew him mercy.
en_uk_kjv~SIR~C29~09~Help the poor for the commandment’s sake, and turn him not away because of his poverty.
en_uk_kjv~SIR~C29~10~Lose thy money for thy brother and thy friend, and let it not rust under a stone to be lost.
en_uk_kjv~SIR~C29~11~Lay up thy treasure according to the commandments of the most High, and it shall bring thee more profit than gold.
en_uk_kjv~SIR~C29~12~Shut up alms in thy storehouses: and it shall deliver thee from all affliction.
en_uk_kjv~SIR~C29~13~It shall fight for thee against thine enemies better than a mighty shield and strong spear.
en_uk_kjv~SIR~C29~14~An honest man is surety for his neighbour: but he that is impudent will forsake him.
en_uk_kjv~SIR~C29~15~Forget not the friendship of thy surety, for he hath given his life for thee.
en_uk_kjv~SIR~C29~16~A sinner will overthrow the good estate of his surety:
en_uk_kjv~SIR~C29~17~And he that is of an unthankful mind will leave him [in danger] that delivered him.
en_uk_kjv~SIR~C29~18~Suretiship hath undone many of good estate, and shaken them as a wave of the sea: mighty men hath it driven from their houses, so that they wandered among strange nations.
en_uk_kjv~SIR~C29~19~A wicked man transgressing the commandments of the Lord shall fall into suretiship: and he that undertaketh and followeth other men’s business for gain shall fall into suits.
en_uk_kjv~SIR~C29~20~Help thy neighbour according to thy power, and beware that thou thyself fall not into the same.
en_uk_kjv~SIR~C29~21~The chief thing for life is water, and bread, and clothing, and an house to cover shame.
en_uk_kjv~SIR~C29~22~Better is the life of a poor man in a mean cottage, than delicate fare in another man’s house.
en_uk_kjv~SIR~C29~23~Be it little or much, hold thee contented, that thou hear not the reproach of thy house.
en_uk_kjv~SIR~C29~24~For it is a miserable life to go from house to house: for where thou art a stranger, thou darest not open thy mouth.
en_uk_kjv~SIR~C29~25~Thou shalt entertain, and feast, and have no thanks: moreover thou shalt hear bitter words:
en_uk_kjv~SIR~C29~26~Come, thou stranger, and furnish a table, and feed me of that thou hast ready.
en_uk_kjv~SIR~C29~27~Give place, thou stranger, to an honourable man; my brother cometh to be lodged, and I have need of mine house.
en_uk_kjv~SIR~C29~28~These things are grievous to a man of understanding; the upbraiding of houseroom, and reproaching of the lender.
