en_uk_kjv~WIS~C18~01~Nevertheless thy saints had a very great light, whose voice they hearing, and not seeing their shape, because they also had not suffered the same things, they counted them happy.
en_uk_kjv~WIS~C18~02~But for that they did not hurt them now, of whom they had been wronged before, they thanked them, and besought them pardon for that they had been enemies.
en_uk_kjv~WIS~C18~03~Instead whereof thou gavest them a burning pillar of fire, both to be a guide of the unknown journey, and an harmless sun to entertain them honourably.
en_uk_kjv~WIS~C18~04~For they were worthy to be deprived of light and imprisoned in darkness, who had kept thy sons shut up, by whom the uncorrupt light of the law was to be given unto the world.
en_uk_kjv~WIS~C18~05~And when they had determined to slay the babes of the saints, one child being cast forth, and saved, to reprove them, thou tookest away the multitude of their children, and destroyedst them altogether in a mighty water.
en_uk_kjv~WIS~C18~06~Of that night were our fathers certified afore, that assuredly knowing unto what oaths they had given credence, they might afterwards be of good cheer.
en_uk_kjv~WIS~C18~07~So of thy people was accepted both the salvation of the righteous, and destruction of the enemies.
en_uk_kjv~WIS~C18~08~For wherewith thou didst punish our adversaries, by the same thou didst glorify us, whom thou hadst called.
en_uk_kjv~WIS~C18~09~For the righteous children of good men did sacrifice secretly, and with one consent made a holy law, that the saints should be like partakers of the same good and evil, the fathers now singing out the songs of praise.
en_uk_kjv~WIS~C18~10~But on the other side there sounded an ill according cry of the enemies, and a lamentable noise was carried abroad for children that were bewailed.
en_uk_kjv~WIS~C18~11~The master and the servant were punished after one manner; and like as the king, so suffered the common person.
en_uk_kjv~WIS~C18~12~So they all together had innumerable dead with one kind of death; neither were the living sufficient to bury them: for in one moment the noblest offspring of them was destroyed.
en_uk_kjv~WIS~C18~13~For whereas they would not believe any thing by reason of the enchantments; upon the destruction of the firstborn, they acknowledged this people to be the sons of God.
en_uk_kjv~WIS~C18~14~For while all things were in quiet silence, and that night was in the midst of her swift course,
en_uk_kjv~WIS~C18~15~Thine Almighty word leaped down from heaven out of thy royal throne, as a fierce man of war into the midst of a land of destruction,
en_uk_kjv~WIS~C18~16~And brought thine unfeigned commandment as a sharp sword, and standing up filled all things with death; and it touched the heaven, but it stood upon the earth.
en_uk_kjv~WIS~C18~17~Then suddenly visions of horrible dreams troubled them sore, and terrors came upon them unlooked for.
en_uk_kjv~WIS~C18~18~And one thrown here, and another there, half dead, shewed the cause of his death.
en_uk_kjv~WIS~C18~19~For the dreams that troubled them did foreshew this, lest they should perish, and not know why they were afflicted.
en_uk_kjv~WIS~C18~20~Yea, the tasting of death touched the righteous also, and there was a destruction of the multitude in the wilderness: but the wrath endured not long.
en_uk_kjv~WIS~C18~21~For then the blameless man made haste, and stood forth to defend them; and bringing the shield of his proper ministry, even prayer, and the propitiation of incense, set himself against the wrath, and so brought the calamity to an end, declaring that he was thy servant.
en_uk_kjv~WIS~C18~22~So he overcame the destroyer, not with strength of body, nor force of arms, but with a word subdued him that punished, alleging the oaths and covenants made with the fathers.
en_uk_kjv~WIS~C18~23~For when the dead were now fallen down by heaps one upon another, standing between, he stayed the wrath, and parted the way to the living.
en_uk_kjv~WIS~C18~24~For in the long garment was the whole world, and in the four rows of the stones was the glory of the fathers graven, and thy Majesty upon the diadem of his head.
en_uk_kjv~WIS~C18~25~Unto these the destroyer gave place, and was afraid of them: for it was enough that they only tasted of the wrath.
