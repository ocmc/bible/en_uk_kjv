en_uk_kjv~PSA~C037~001~(C038:000) A Psalm of David, to bring to remembrance.
en_uk_kjv~PSA~C037~002~(C038:001) O LORD, rebuke me not in thy wrath: neither chasten me in thy hot displeasure.
en_uk_kjv~PSA~C037~003~(C038:002) For thine arrows stick fast in me, and thy hand presseth me sore.
en_uk_kjv~PSA~C037~004~(C038:003) [There is] no soundness in my flesh because of thine anger; neither [is there any] rest in my bones because of my sin.
en_uk_kjv~PSA~C037~005~(C038:004) For mine iniquities are gone over mine head: as an heavy burden they are too heavy for me.
en_uk_kjv~PSA~C037~006~(C038:005) My wounds stink [and] are corrupt because of my foolishness.
en_uk_kjv~PSA~C037~007~(C038:006) I am troubled; I am bowed down greatly; I go mourning all the day long.
en_uk_kjv~PSA~C037~008~(C038:007) For my loins are filled with a loathsome [disease:] and [there is] no soundness in my flesh.
en_uk_kjv~PSA~C037~009~(C038:008) I am feeble and sore broken: I have roared by reason of the disquietness of my heart.
en_uk_kjv~PSA~C037~010~(C038:009) Lord, all my desire [is] before thee; and my groaning is not hid from thee.
en_uk_kjv~PSA~C037~011~(C038:010) My heart panteth, my strength faileth me: as for the light of mine eyes, it also is gone from me.
en_uk_kjv~PSA~C037~012~(C038:011) My lovers and my friends stand aloof from my sore; and my kinsmen stand afar off.
en_uk_kjv~PSA~C037~013~(C038:012) They also that seek after my life lay snares [for me:] and they that seek my hurt speak mischievous things, and imagine deceits all the day long.
en_uk_kjv~PSA~C037~014~(C038:013) But I, as a deaf [man], heard not; and [I was] as a dumb man [that] openeth not his mouth.
en_uk_kjv~PSA~C037~015~(C038:014) Thus I was as a man that heareth not, and in whose mouth [are] no reproofs.
en_uk_kjv~PSA~C037~016~(C038:015) For in thee, O LORD, do I hope: thou wilt hear, O Lord my God.
en_uk_kjv~PSA~C037~017~(C038:016) For I said, [Hear me], lest [otherwise] they should rejoice over me: when my foot slippeth, they magnify [themselves] against me.
en_uk_kjv~PSA~C037~018~(C038:017) For I [am] ready to halt, and my sorrow [is] continually before me.
en_uk_kjv~PSA~C037~019~(C038:018) For I will declare mine iniquity; I will be sorry for my sin.
en_uk_kjv~PSA~C037~020~(C038:019) But mine enemies [are] lively, [and] they are strong: and they that hate me wrongfully are multiplied.
en_uk_kjv~PSA~C037~021~(C038:020) They also that render evil for good are mine adversaries; because I follow [the thing that] good [is].
en_uk_kjv~PSA~C037~022~(C038:021) Forsake me not, O LORD: O my God, be not far from me.
en_uk_kjv~PSA~C037~023~(C038:022) Make haste to help me, O Lord my salvation.
