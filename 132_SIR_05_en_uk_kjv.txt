en_uk_kjv~SIR~C05~01~Set thy heart upon thy goods; and say not, I have enough for my life.
en_uk_kjv~SIR~C05~02~Follow not thine own mind and thy strength, to walk in the ways of thy heart:
en_uk_kjv~SIR~C05~03~And say not, Who shall controul me for my works? for the Lord will surely revenge thy pride.
en_uk_kjv~SIR~C05~04~Say not, I have sinned, and what harm hath happened unto me? for the Lord is longsuffering, he will in no wise let thee go.
en_uk_kjv~SIR~C05~05~Concerning propitiation, be not without fear to add sin unto sin:
en_uk_kjv~SIR~C05~06~And say not His mercy is great; he will be pacified for the multitude of my sins: for mercy and wrath come from him, and his indignation resteth upon sinners.
en_uk_kjv~SIR~C05~07~Make no tarrying to turn to the Lord, and put not off from day to day: for suddenly shall the wrath of the Lord come forth, and in thy security thou shalt be destroyed, and perish in the day of vengeance.
en_uk_kjv~SIR~C05~08~Set not thine heart upon goods unjustly gotten, for they shall not profit thee in the day of calamity.
en_uk_kjv~SIR~C05~09~Winnow not with every wind, and go not into every way: for so doth the sinner that hath a double tongue.
en_uk_kjv~SIR~C05~10~Be stedfast in thy understanding; and let thy word be the same.
en_uk_kjv~SIR~C05~11~Be swift to hear; and let thy life be sincere; and with patience give answer.
en_uk_kjv~SIR~C05~12~If thou hast understanding, answer thy neighbour; if not, lay thy hand upon thy mouth.
en_uk_kjv~SIR~C05~13~Honour and shame is in talk: and the tongue of man is his fall.
en_uk_kjv~SIR~C05~14~Be not called a whisperer, and lie not in wait with thy tongue: for a foul shame is upon the thief, and an evil condemnation upon the double tongue.
en_uk_kjv~SIR~C05~15~Be not ignorant of any thing in a great matter or a small.
