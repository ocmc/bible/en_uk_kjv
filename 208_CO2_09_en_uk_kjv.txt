en_uk_kjv~CO2~C09~01~For as touching the ministering to the saints, it is superfluous for me to write to you:
en_uk_kjv~CO2~C09~02~For I know the forwardness of your mind, for which I boast of you to them of Macedonia, that Achaia was ready a year ago; and your zeal hath provoked very many.
en_uk_kjv~CO2~C09~03~Yet have I sent the brethren, lest our boasting of you should be in vain in this behalf; that, as I said, ye may be ready:
en_uk_kjv~CO2~C09~04~Lest haply if they of Macedonia come with me, and find you unprepared, we (that we say not, ye) should be ashamed in this same confident boasting.
en_uk_kjv~CO2~C09~05~Therefore I thought it necessary to exhort the brethren, that they would go before unto you, and make up beforehand your bounty, whereof ye had notice before, that the same might be ready, as [a matter of] bounty, and not as [of] covetousness.
en_uk_kjv~CO2~C09~06~But this [I say], He which soweth sparingly shall reap also sparingly; and he which soweth bountifully shall reap also bountifully.
en_uk_kjv~CO2~C09~07~Every man according as he purposeth in his heart, [so let him give]; not grudgingly, or of necessity: for God loveth a cheerful giver.
en_uk_kjv~CO2~C09~08~And God [is] able to make all grace abound toward you; that ye, always having all sufficiency in all [things], may abound to every good work:
en_uk_kjv~CO2~C09~09~(As it is written, He hath dispersed abroad; he hath given to the poor: his righteousness remaineth for ever.
en_uk_kjv~CO2~C09~10~Now he that ministereth seed to the sower both minister bread for [your] food, and multiply your seed sown, and increase the fruits of your righteousness;)
en_uk_kjv~CO2~C09~11~Being enriched in every thing to all bountifulness, which causeth through us thanksgiving to God.
en_uk_kjv~CO2~C09~12~For the administration of this service not only supplieth the want of the saints, but is abundant also by many thanksgivings unto God;
en_uk_kjv~CO2~C09~13~Whiles by the experiment of this ministration they glorify God for your professed subjection unto the gospel of Christ, and for [your] liberal distribution unto them, and unto all [men];
en_uk_kjv~CO2~C09~14~And by their prayer for you, which long after you for the exceeding grace of God in you.
en_uk_kjv~CO2~C09~15~Thanks [be] unto God for his unspeakable gift.
