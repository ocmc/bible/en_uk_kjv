en_uk_kjv~PSA~C076~001~(C077:000) To the chief Musician, to Jeduthun, A Psalm of Asaph.
en_uk_kjv~PSA~C076~002~(C077:001) I cried unto God with my voice, [even] unto God with my voice; and he gave ear unto me.
en_uk_kjv~PSA~C076~003~(C077:002) In the day of my trouble I sought the Lord: my sore ran in the night, and ceased not: my soul refused to be comforted.
en_uk_kjv~PSA~C076~004~(C077:003) I remembered God, and was troubled: I complained, and my spirit was overwhelmed. Selah.
en_uk_kjv~PSA~C076~005~(C077:004) Thou holdest mine eyes waking: I am so troubled that I cannot speak.
en_uk_kjv~PSA~C076~006~(C077:005) I have considered the days of old, the years of ancient times.
en_uk_kjv~PSA~C076~007~(C077:006) I call to remembrance my song in the night: I commune with mine own heart: and my spirit made diligent search.
en_uk_kjv~PSA~C076~008~(C077:007) Will the Lord cast off for ever? and will he be favourable no more?
en_uk_kjv~PSA~C076~009~(C077:008) Is his mercy clean gone for ever? doth [his] promise fail for evermore?
en_uk_kjv~PSA~C076~010~(C077:009) Hath God forgotten to be gracious? hath he in anger shut up his tender mercies? Selah.
en_uk_kjv~PSA~C076~011~(C077:010) And I said, This [is] my infirmity: [but I will remember] the years of the right hand of the most High.
en_uk_kjv~PSA~C076~012~(C077:011) I will remember the works of the LORD: surely I will remember thy wonders of old.
en_uk_kjv~PSA~C076~013~(C077:012) I will meditate also of all thy work, and talk of thy doings.
en_uk_kjv~PSA~C076~014~(C077:013) Thy way, O God, [is] in the sanctuary: who [is so] great a God as [our] God?
en_uk_kjv~PSA~C076~015~(C077:014) Thou [art] the God that doest wonders: thou hast declared thy strength among the people.
en_uk_kjv~PSA~C076~016~(C077:015) Thou hast with [thine] arm redeemed thy people, the sons of Jacob and Joseph. Selah.
en_uk_kjv~PSA~C076~017~(C077:016) The waters saw thee, O God, the waters saw thee; they were afraid: the depths also were troubled.
en_uk_kjv~PSA~C076~018~(C077:017) The clouds poured out water: the skies sent out a sound: thine arrows also went abroad.
en_uk_kjv~PSA~C076~019~(C077:018) The voice of thy thunder [was] in the heaven: the lightnings lightened the world: the earth trembled and shook.
en_uk_kjv~PSA~C076~020~(C077:019) Thy way [is] in the sea, and thy path in the great waters, and thy footsteps are not known.
en_uk_kjv~PSA~C076~021~(C077:020) Thou leddest thy people like a flock by the hand of Moses and Aaron.
