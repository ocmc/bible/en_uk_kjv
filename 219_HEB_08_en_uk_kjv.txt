en_uk_kjv~HEB~C08~01~Now of the things which we have spoken [this is] the sum: We have such an high priest, who is set on the right hand of the throne of the Majesty in the heavens;
en_uk_kjv~HEB~C08~02~A minister of the sanctuary, and of the true tabernacle, which the Lord pitched, and not man.
en_uk_kjv~HEB~C08~03~For every high priest is ordained to offer gifts and sacrifices: wherefore [it is] of necessity that this man have somewhat also to offer.
en_uk_kjv~HEB~C08~04~For if he were on earth, he should not be a priest, seeing that there are priests that offer gifts according to the law:
en_uk_kjv~HEB~C08~05~Who serve unto the example and shadow of heavenly things, as Moses was admonished of God when he was about to make the tabernacle: for, See, saith he, [that] thou make all things according to the pattern shewed to thee in the mount.
en_uk_kjv~HEB~C08~06~But now hath he obtained a more excellent ministry, by how much also he is the mediator of a better covenant, which was established upon better promises.
en_uk_kjv~HEB~C08~07~For if that first [covenant] had been faultless, then should no place have been sought for the second.
en_uk_kjv~HEB~C08~08~For finding fault with them, he saith, Behold, the days come, saith the Lord, when I will make a new covenant with the house of Israel and with the house of Judah:
en_uk_kjv~HEB~C08~09~Not according to the covenant that I made with their fathers in the day when I took them by the hand to lead them out of the land of Egypt; because they continued not in my covenant, and I regarded them not, saith the Lord.
en_uk_kjv~HEB~C08~10~For this [is] the covenant that I will make with the house of Israel after those days, saith the Lord; I will put my laws into their mind, and write them in their hearts: and I will be to them a God, and they shall be to me a people:
en_uk_kjv~HEB~C08~11~And they shall not teach every man his neighbour, and every man his brother, saying, Know the Lord: for all shall know me, from the least to the greatest.
en_uk_kjv~HEB~C08~12~For I will be merciful to their unrighteousness, and their sins and their iniquities will I remember no more.
en_uk_kjv~HEB~C08~13~In that he saith, A new [covenant], he hath made the first old. Now that which decayeth and waxeth old [is] ready to vanish away.
