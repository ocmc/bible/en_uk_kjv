en_uk_kjv~PSA~C140~001~(C141:001) (0) A Psalm of David. (1) LORD, I cry unto thee: make haste unto me; give ear unto my voice, when I cry unto thee.
en_uk_kjv~PSA~C140~002~(C141:002) Let my prayer be set forth before thee [as] incense; [and] the lifting up of my hands [as] the evening sacrifice.
en_uk_kjv~PSA~C140~003~(C141:003) Set a watch, O LORD, before my mouth; keep the door of my lips.
en_uk_kjv~PSA~C140~004~(C141:004) Incline not my heart to [any] evil thing, to practise wicked works with men that work iniquity: and let me not eat of their dainties.
en_uk_kjv~PSA~C140~005~(C141:005) Let the righteous smite me; [it shall be] a kindness: and let him reprove me; [it shall be] an excellent oil, [which] shall not break my head: for yet my prayer also [shall be] in their calamities.
en_uk_kjv~PSA~C140~006~(C141:006) When their judges are overthrown in stony places, they shall hear my words; for they are sweet.
en_uk_kjv~PSA~C140~007~(C141:007) Our bones are scattered at the grave’s mouth, as when one cutteth and cleaveth [wood] upon the earth.
en_uk_kjv~PSA~C140~008~(C141:008) But mine eyes [are] unto thee, O GOD the Lord: in thee is my trust; leave not my soul destitute.
en_uk_kjv~PSA~C140~009~(C141:009) Keep me from the snares [which] they have laid for me, and the gins of the workers of iniquity.
en_uk_kjv~PSA~C140~010~(C141:010) Let the wicked fall into their own nets, whilst that I withal escape.
