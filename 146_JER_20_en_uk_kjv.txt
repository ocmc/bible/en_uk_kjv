en_uk_kjv~JER~C20~01~Now Pashur the son of Immer the priest, who [was] also chief governor in the house of the LORD, heard that Jeremiah prophesied these things.
en_uk_kjv~JER~C20~02~Then Pashur smote Jeremiah the prophet, and put him in the stocks that [were] in the high gate of Benjamin, which [was] by the house of the LORD.
en_uk_kjv~JER~C20~03~And it came to pass on the morrow, that Pashur brought forth Jeremiah out of the stocks. Then said Jeremiah unto him, The LORD hath not called thy name Pashur, but Magor-missabib.
en_uk_kjv~JER~C20~04~For thus saith the LORD, Behold, I will make thee a terror to thyself, and to all thy friends: and they shall fall by the sword of their enemies, and thine eyes shall behold [it:] and I will give all Judah into the hand of the king of Babylon, and he shall carry them captive into Babylon, and shall slay them with the sword.
en_uk_kjv~JER~C20~05~Moreover I will deliver all the strength of this city, and all the labours thereof, and all the precious things thereof, and all the treasures of the kings of Judah will I give into the hand of their enemies, which shall spoil them, and take them, and carry them to Babylon.
en_uk_kjv~JER~C20~06~And thou, Pashur, and all that dwell in thine house shall go into captivity: and thou shalt come to Babylon, and there thou shalt die, and shalt be buried there, thou, and all thy friends, to whom thou hast prophesied lies.
en_uk_kjv~JER~C20~07~O LORD, thou hast deceived me, and I was deceived: thou art stronger than I, and hast prevailed: I am in derision daily, every one mocketh me.
en_uk_kjv~JER~C20~08~For since I spake, I cried out, I cried violence and spoil; because the word of the LORD was made a reproach unto me, and a derision, daily.
en_uk_kjv~JER~C20~09~Then I said, I will not make mention of him, nor speak any more in his name. But [his word] was in mine heart as a burning fire shut up in my bones, and I was weary with forbearing, and I could not [stay].
en_uk_kjv~JER~C20~10~For I heard the defaming of many, fear on every side. Report, [say they], and we will report it. All my familiars watched for my halting, [saying], Peradventure he will be enticed, and we shall prevail against him, and we shall take our revenge on him.
en_uk_kjv~JER~C20~11~But the LORD [is] with me as a mighty terrible one: therefore my persecutors shall stumble, and they shall not prevail: they shall be greatly ashamed; for they shall not prosper: [their] everlasting confusion shall never be forgotten.
en_uk_kjv~JER~C20~12~But, O LORD of hosts, that triest the righteous, [and] seest the reins and the heart, let me see thy vengeance on them: for unto thee have I opened my cause.
en_uk_kjv~JER~C20~13~Sing unto the LORD, praise ye the LORD: for he hath delivered the soul of the poor from the hand of evildoers.
en_uk_kjv~JER~C20~14~Cursed [be] the day wherein I was born: let not the day wherein my mother bare me be blessed.
en_uk_kjv~JER~C20~15~Cursed [be] the man who brought tidings to my father, saying, A man child is born unto thee; making him very glad.
en_uk_kjv~JER~C20~16~And let that man be as the cities which the LORD overthrew, and repented not: and let him hear the cry in the morning, and the shouting at noontide;
en_uk_kjv~JER~C20~17~Because he slew me not from the womb; or that my mother might have been my grave, and her womb [to be] always great [with me].
en_uk_kjv~JER~C20~18~Wherefore came I forth out of the womb to see labour and sorrow, that my days should be consumed with shame?
