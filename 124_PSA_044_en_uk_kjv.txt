en_uk_kjv~PSA~C044~001~(C045:000) To the chief Musician upon Shoshannim, for the sons of Korah, Maschil, A Song of loves.
en_uk_kjv~PSA~C044~002~(C045:001) My heart is inditing a good matter: I speak of the things which I have made touching the king: my tongue [is] the pen of a ready writer.
en_uk_kjv~PSA~C044~003~(C045:002) Thou art fairer than the children of men: grace is poured into thy lips: therefore God hath blessed thee for ever.
en_uk_kjv~PSA~C044~004~(C045:003) Gird thy sword upon [thy] thigh, O [most] mighty, with thy glory and thy majesty.
en_uk_kjv~PSA~C044~005~(C045:004) And in thy majesty ride prosperously because of truth and meekness [and] righteousness; and thy right hand shall teach thee terrible things.
en_uk_kjv~PSA~C044~006~(C045:005) Thine arrows [are] sharp in the heart of the king’s enemies; [whereby] the people fall under thee.
en_uk_kjv~PSA~C044~007~(C045:006) Thy throne, O God, [is] for ever and ever: the sceptre of thy kingdom [is] a right sceptre.
en_uk_kjv~PSA~C044~008~(C045:007) Thou lovest righteousness, and hatest wickedness: therefore God, thy God, hath anointed thee with the oil of gladness above thy fellows.
en_uk_kjv~PSA~C044~009~(C045:008) All thy garments [smell] of myrrh, and aloes, [and] cassia, out of the ivory palaces, whereby they have made thee glad.
en_uk_kjv~PSA~C044~010~(C045:009) Kings’ daughters [were] among thy honourable women: upon thy right hand did stand the queen in gold of Ophir.
en_uk_kjv~PSA~C044~011~(C045:010) Hearken, O daughter, and consider, and incline thine ear; forget also thine own people, and thy father’s house;
en_uk_kjv~PSA~C044~012~(C045:011) So shall the king greatly desire thy beauty: for he [is] thy Lord; and worship thou him.
en_uk_kjv~PSA~C044~013~(C045:012) And the daughter of Tyre shall be there with a gift; even the rich among the people shall intreat thy favour.
en_uk_kjv~PSA~C044~014~(C045:013) The king’s daughter [is] all glorious within: her clothing [is] of wrought gold.
en_uk_kjv~PSA~C044~015~(C045:014) She shall be brought unto the king in raiment of needlework: the virgins her companions that follow her shall be brought unto thee.
en_uk_kjv~PSA~C044~016~(C045:015) With gladness and rejoicing shall they be brought: they shall enter into the king’s palace.
en_uk_kjv~PSA~C044~017~(C045:016) Instead of thy fathers shall be thy children, whom thou mayest make princes in all the earth.
en_uk_kjv~PSA~C044~018~(C045:017) I will make thy name to be remembered in all generations: therefore shall the people praise thee for ever and ever.
