en_uk_kjv~PSA~C036~001~(C037:001) (0) [A Psalm] of David. (1) Fret not thyself because of evildoers, neither be thou envious against the workers of iniquity.
en_uk_kjv~PSA~C036~002~(C037:002) For they shall soon be cut down like the grass, and wither as the green herb.
en_uk_kjv~PSA~C036~003~(C037:003) Trust in the LORD, and do good; [so] shalt thou dwell in the land, and verily thou shalt be fed.
en_uk_kjv~PSA~C036~004~(C037:004) Delight thyself also in the LORD; and he shall give thee the desires of thine heart.
en_uk_kjv~PSA~C036~005~(C037:005) Commit thy way unto the LORD; trust also in him; and he shall bring [it] to pass.
en_uk_kjv~PSA~C036~006~(C037:006) And he shall bring forth thy righteousness as the light, and thy judgment as the noonday.
en_uk_kjv~PSA~C036~007~(C037:007) Rest in the LORD, and wait patiently for him: fret not thyself because of him who prospereth in his way, because of the man who bringeth wicked devices to pass.
en_uk_kjv~PSA~C036~008~(C037:008) Cease from anger, and forsake wrath: fret not thyself in any wise to do evil.
en_uk_kjv~PSA~C036~009~(C037:009) For evildoers shall be cut off: but those that wait upon the LORD, they shall inherit the earth.
en_uk_kjv~PSA~C036~010~(C037:010) For yet a little while, and the wicked [shall] not [be:] yea, thou shalt diligently consider his place, and it [shall] not [be].
en_uk_kjv~PSA~C036~011~(C037:011) But the meek shall inherit the earth; and shall delight themselves in the abundance of peace.
en_uk_kjv~PSA~C036~012~(C037:012) The wicked plotteth against the just, and gnasheth upon him with his teeth.
en_uk_kjv~PSA~C036~013~(C037:013) The Lord shall laugh at him: for he seeth that his day is coming.
en_uk_kjv~PSA~C036~014~(C037:014) The wicked have drawn out the sword, and have bent their bow, to cast down the poor and needy, [and] to slay such as be of upright conversation.
en_uk_kjv~PSA~C036~015~(C037:015) Their sword shall enter into their own heart, and their bows shall be broken.
en_uk_kjv~PSA~C036~016~(C037:016) A little that a righteous man hath [is] better than the riches of many wicked.
en_uk_kjv~PSA~C036~017~(C037:017) For the arms of the wicked shall be broken: but the LORD upholdeth the righteous.
en_uk_kjv~PSA~C036~018~(C037:018) The LORD knoweth the days of the upright: and their inheritance shall be for ever.
en_uk_kjv~PSA~C036~019~(C037:019) They shall not be ashamed in the evil time: and in the days of famine they shall be satisfied.
en_uk_kjv~PSA~C036~020~(C037:020) But the wicked shall perish, and the enemies of the LORD [shall be] as the fat of lambs: they shall consume; into smoke shall they consume away.
en_uk_kjv~PSA~C036~021~(C037:021) The wicked borroweth, and payeth not again: but the righteous sheweth mercy, and giveth.
en_uk_kjv~PSA~C036~022~(C037:022) For [such as be] blessed of him shall inherit the earth; and [they that be] cursed of him shall be cut off.
en_uk_kjv~PSA~C036~023~(C037:023) The steps of a [good] man are ordered by the LORD: and he delighteth in his way.
en_uk_kjv~PSA~C036~024~(C037:024) Though he fall, he shall not be utterly cast down: for the LORD upholdeth [him with] his hand.
en_uk_kjv~PSA~C036~025~(C037:025) I have been young, and [now] am old; yet have I not seen the righteous forsaken, nor his seed begging bread.
en_uk_kjv~PSA~C036~026~(C037:026) [He is] ever merciful, and lendeth; and his seed [is] blessed.
en_uk_kjv~PSA~C036~027~(C037:027) Depart from evil, and do good; and dwell for evermore.
en_uk_kjv~PSA~C036~028~(C037:028) For the LORD loveth judgment, and forsaketh not his saints; they are preserved for ever: but the seed of the wicked shall be cut off.
en_uk_kjv~PSA~C036~029~(C037:029) The righteous shall inherit the land, and dwell therein for ever.
en_uk_kjv~PSA~C036~030~(C037:030) The mouth of the righteous speaketh wisdom, and his tongue talketh of judgment.
en_uk_kjv~PSA~C036~031~(C037:031) The law of his God [is] in his heart; none of his steps shall slide.
en_uk_kjv~PSA~C036~032~(C037:032) The wicked watcheth the righteous, and seeketh to slay him.
en_uk_kjv~PSA~C036~033~(C037:033) The LORD will not leave him in his hand, nor condemn him when he is judged.
en_uk_kjv~PSA~C036~034~(C037:034) Wait on the LORD, and keep his way, and he shall exalt thee to inherit the land: when the wicked are cut off, thou shalt see [it].
en_uk_kjv~PSA~C036~035~(C037:035) I have seen the wicked in great power, and spreading himself like a green bay tree.
en_uk_kjv~PSA~C036~036~(C037:036) Yet he passed away, and, lo, he [was] not: yea, I sought him, but he could not be found.
en_uk_kjv~PSA~C036~037~(C037:037) Mark the perfect [man], and behold the upright: for the end of [that] man [is] peace.
en_uk_kjv~PSA~C036~038~(C037:038) But the transgressors shall be destroyed together: the end of the wicked shall be cut off.
en_uk_kjv~PSA~C036~039~(C037:039) But the salvation of the righteous [is] of the LORD: [he is] their strength in the time of trouble.
en_uk_kjv~PSA~C036~040~(C037:040) And the LORD shall help them, and deliver them: he shall deliver them from the wicked, and save them, because they trust in him.
