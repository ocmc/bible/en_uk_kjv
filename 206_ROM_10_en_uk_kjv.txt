en_uk_kjv~ROM~C10~01~Brethren, my heart’s desire and prayer to God for Israel is, that they might be saved.
en_uk_kjv~ROM~C10~02~For I bear them record that they have a zeal of God, but not according to knowledge.
en_uk_kjv~ROM~C10~03~For they being ignorant of God’s righteousness, and going about to establish their own righteousness, have not submitted themselves unto the righteousness of God.
en_uk_kjv~ROM~C10~04~For Christ [is] the end of the law for righteousness to every one that believeth.
en_uk_kjv~ROM~C10~05~For Moses describeth the righteousness which is of the law, That the man which doeth those things shall live by them.
en_uk_kjv~ROM~C10~06~But the righteousness which is of faith speaketh on this wise, Say not in thine heart, Who shall ascend into heaven? (that is, to bring Christ down [from above]:)
en_uk_kjv~ROM~C10~07~Or, Who shall descend into the deep? (that is, to bring up Christ again from the dead.)
en_uk_kjv~ROM~C10~08~But what saith it? The word is nigh thee, [even] in thy mouth, and in thy heart: that is, the word of faith, which we preach;
en_uk_kjv~ROM~C10~09~That if thou shalt confess with thy mouth the Lord Jesus, and shalt believe in thine heart that God hath raised him from the dead, thou shalt be saved.
en_uk_kjv~ROM~C10~10~For with the heart man believeth unto righteousness; and with the mouth confession is made unto salvation.
en_uk_kjv~ROM~C10~11~For the scripture saith, Whosoever believeth on him shall not be ashamed.
en_uk_kjv~ROM~C10~12~For there is no difference between the Jew and the Greek: for the same Lord over all is rich unto all that call upon him.
en_uk_kjv~ROM~C10~13~For whosoever shall call upon the name of the Lord shall be saved.
en_uk_kjv~ROM~C10~14~How then shall they call on him in whom they have not believed? and how shall they believe in him of whom they have not heard? and how shall they hear without a preacher?
en_uk_kjv~ROM~C10~15~And how shall they preach, except they be sent? as it is written, How beautiful are the feet of them that preach the gospel of peace, and bring glad tidings of good things!
en_uk_kjv~ROM~C10~16~But they have not all obeyed the gospel. For Esaias saith, Lord, who hath believed our report?
en_uk_kjv~ROM~C10~17~So then faith [cometh] by hearing, and hearing by the word of God.
en_uk_kjv~ROM~C10~18~But I say, Have they not heard? Yes verily, their sound went into all the earth, and their words unto the ends of the world.
en_uk_kjv~ROM~C10~19~But I say, Did not Israel know? First Moses saith, I will provoke you to jealousy by [them that are] no people, [and] by a foolish nation I will anger you.
en_uk_kjv~ROM~C10~20~But Esaias is very bold, and saith, I was found of them that sought me not; I was made manifest unto them that asked not after me.
en_uk_kjv~ROM~C10~21~But to Israel he saith, All day long I have stretched forth my hands unto a disobedient and gainsaying people.
