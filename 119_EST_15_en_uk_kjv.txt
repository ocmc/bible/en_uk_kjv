en_uk_kjv~EST~C15~01~And upon the third day, when she had ended her prayers, she laid away her mourning garments, and put on her glorious apparel.
en_uk_kjv~EST~C15~02~And being gloriously adorned, after she had called upon God, who is the beholder and saviour of all things, she took two maids with her:
en_uk_kjv~EST~C15~03~And upon the one she leaned, as carrying herself daintily;
en_uk_kjv~EST~C15~04~And the other followed, bearing up her train.
en_uk_kjv~EST~C15~05~And she was ruddy through the perfection of her beauty, and her countenance was cheerful and very amiable: but her heart was in anguish for fear.
en_uk_kjv~EST~C15~06~Then having passed through all the doors, she stood before the king, who sat upon his royal throne, and was clothed with all his robes of majesty, all glittering with gold and precious stones; and he was very dreadful.
en_uk_kjv~EST~C15~07~Then lifting up his countenance that shone with majesty, he looked very fiercely upon her: and the queen fell down, and was pale, and fainted, and bowed herself upon the head of the maid that went before her.
en_uk_kjv~EST~C15~08~Then God changed the spirit of the king into mildness, who in a fear leaped from his throne, and took her in his arms, till she came to herself again, and comforted her with loving words and said unto her,
en_uk_kjv~EST~C15~09~Esther, what is the matter? I am thy brother, be of good cheer:
en_uk_kjv~EST~C15~10~Thou shalt not die, though our commandment be general: come near.
en_uk_kjv~EST~C15~11~And so he held up his golden sceptre, and laid it upon her neck,
en_uk_kjv~EST~C15~12~And embraced her, and said, Speak unto me.
en_uk_kjv~EST~C15~13~Then said she unto him, I saw thee, my lord, as an angel of God, and my heart was troubled for fear of thy majesty.
en_uk_kjv~EST~C15~14~For wonderful art thou, lord, and thy countenance is full of grace.
en_uk_kjv~EST~C15~15~And as she was speaking, she fell down for faintness.
en_uk_kjv~EST~C15~16~Then the king was troubled, and all his servants comforted her.
