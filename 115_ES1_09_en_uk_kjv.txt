en_uk_kjv~ES1~C09~01~Then Esdras rising from the court of the temple went to the chamber of Joanan the son of Eliasib,
en_uk_kjv~ES1~C09~02~And remained there, and did eat no meat nor drink water, mourning for the great iniquities of the multitude.
en_uk_kjv~ES1~C09~03~And there was a proclamation in all Jewry and Jerusalem to all them that were of the captivity, that they should be gathered together at Jerusalem:
en_uk_kjv~ES1~C09~04~And that whosoever met not there within two or three days according as the elders that bare rule appointed, their cattle should be seized to the use of the temple, and himself cast out from them that were of the captivity.
en_uk_kjv~ES1~C09~05~And in three days were all they of the tribe of Judah and Benjamin gathered together at Jerusalem the twentieth day of the ninth month.
en_uk_kjv~ES1~C09~06~And all the multitude sat trembling in the broad court of the temple because of the present foul weather.
en_uk_kjv~ES1~C09~07~So Esdras arose up, and said unto them, Ye have transgressed the law in marrying strange wives, thereby to increase the sins of Israel.
en_uk_kjv~ES1~C09~08~And now by confessing give glory unto the Lord God of our fathers,
en_uk_kjv~ES1~C09~09~And do his will, and separate yourselves from the heathen of the land, and from the strange women.
en_uk_kjv~ES1~C09~10~Then cried the whole multitude, and said with a loud voice, Like as thou hast spoken, so will we do.
en_uk_kjv~ES1~C09~11~But forasmuch as the people are many, and it is foul weather, so that we cannot stand without, and this is not a work of a day or two, seeing our sin in these things is spread far:
en_uk_kjv~ES1~C09~12~Therefore let the rulers of the multitude stay, and let all them of our habitations that have strange wives come at the time appointed,
en_uk_kjv~ES1~C09~13~And with them the rulers and judges of every place, till we turn away the wrath of the Lord from us for this matter.
en_uk_kjv~ES1~C09~14~Then Jonathan the son of Azael and Ezechias the son of Theocanus accordingly took this matter upon them: and Mosollam and Levis and Sabbatheus helped them.
en_uk_kjv~ES1~C09~15~And they that were of the captivity did according to all these things.
en_uk_kjv~ES1~C09~16~And Esdras the priest chose unto him the principal men of their families, all by name: and in the first day of the tenth month they sat together to examine the matter.
en_uk_kjv~ES1~C09~17~So their cause that held strange wives was brought to an end in the first day of the first month.
en_uk_kjv~ES1~C09~18~And of the priests that were come together, and had strange wives, there were found:
en_uk_kjv~ES1~C09~19~Of the sons of Jesus the son of Josedec, and his brethren; Matthelas and Eleazar, and Joribus and Joadanus.
en_uk_kjv~ES1~C09~20~And they gave their hands to put away their wives and to offer rams to make reconcilement for their errors.
en_uk_kjv~ES1~C09~21~And of the sons of Emmer; Ananias, and Zabdeus, and Eanes, and Sameius, and Hiereel, and Azarias.
en_uk_kjv~ES1~C09~22~And of the sons of Phaisur; Elionas, Massias, Israel, and Nathanael, and Ocidelus, and Talsas.
en_uk_kjv~ES1~C09~23~And of the Levites; Jozabad, and Semis, and Colius, who was called Calitas, and Patheus, and Judas, and Jonas.
en_uk_kjv~ES1~C09~24~Of the holy singers; Eleazurus, Bacchurus.
en_uk_kjv~ES1~C09~25~Of the porters; Sallumus, and Tolbanes.
en_uk_kjv~ES1~C09~26~Of them of Israel, of the sons of Phoros; Hiermas, and Eddias, and Melchias, and Maelus, and Eleazar, and Asibias, and Baanias.
en_uk_kjv~ES1~C09~27~Of the sons of Ela; Matthanias, Zacharias, and Hierielus, and Hieremoth, and Aedias.
en_uk_kjv~ES1~C09~28~And of the sons of Zamoth; Eliadas, Elisimus, Othonias, Jarimoth, and Sabatus, and Sardeus.
en_uk_kjv~ES1~C09~29~Of the sons of Babai; Johannes, and Ananias and Josabad, and Amatheis.
en_uk_kjv~ES1~C09~30~Of the sons of Mani; Olamus, Mamuchus, Jedeus, Jasubus, Jasael, and Hieremoth.
en_uk_kjv~ES1~C09~31~And of the sons of Addi; Naathus, and Moosias, Lacunus, and Naidus, and Mathanias, and Sesthel, Balnuus, and Manasseas.
en_uk_kjv~ES1~C09~32~And of the sons of Annas; Elionas and Aseas, and Melchias, and Sabbeus, and Simon Chosameus.
en_uk_kjv~ES1~C09~33~And of the sons of Asom; Altaneus, and Matthias, and Baanaia, Eliphalet, and Manasses, and Semei.
en_uk_kjv~ES1~C09~34~And of the sons of Maani; Jeremias, Momdis, Omaerus, Juel, Mabdai, and Pelias, and Anos, Carabasion, and Enasibus, and Mamnitanaimus, Eliasis, Bannus, Eliali, Samis, Selemias, Nathanias: and of the sons of Ozora; Sesis, Esril, Azaelus, Samatus, Zambis, Josephus.
en_uk_kjv~ES1~C09~35~And of the sons of Ethma; Mazitias, Zabadaias, Edes, Juel, Banaias.
en_uk_kjv~ES1~C09~36~All these had taken strange wives, and they put them away with their children.
en_uk_kjv~ES1~C09~37~And the priests and Levites, and they that were of Israel, dwelt in Jerusalem, and in the country, in the first day of the seventh month: so the children of Israel were in their habitations.
en_uk_kjv~ES1~C09~38~And the whole multitude came together with one accord into the broad place of the holy porch toward the east:
en_uk_kjv~ES1~C09~39~And they spake unto Esdras the priest and reader, that he would bring the law of Moses, that was given of the Lord God of Israel.
en_uk_kjv~ES1~C09~40~So Esdras the chief priest brought the law unto the whole multitude from man to woman, and to all the priests, to hear law in the first day of the seventh month.
en_uk_kjv~ES1~C09~41~And he read in the broad court before the holy porch from morning unto midday, before both men and women; and the multitude gave heed unto the law.
en_uk_kjv~ES1~C09~42~And Esdras the priest and reader of the law stood up upon a pulpit of wood, which was made for that purpose.
en_uk_kjv~ES1~C09~43~And there stood up by him Mattathias, Sammus, Ananias, Azarias, Urias, Ezecias, Balasamus, upon the right hand:
en_uk_kjv~ES1~C09~44~And upon his left hand stood Phaldaius, Misael, Melchias, Lothasubus, and Nabarias.
en_uk_kjv~ES1~C09~45~Then took Esdras the book of the law before the multitude: for he sat honourably in the first place in the sight of them all.
en_uk_kjv~ES1~C09~46~And when he opened the law, they stood all straight up. So Esdras blessed the Lord God most High, the God of hosts, Almighty.
en_uk_kjv~ES1~C09~47~And all the people answered, Amen; and lifting up their hands they fell to the ground, and worshipped the Lord.
en_uk_kjv~ES1~C09~48~Also Jesus, Anus, Sarabias, Adinus, Jacubus, Sabateas, Auteas, Maianeas, and Calitas, Asrias, and Joazabdus, and Ananias, Biatas, the Levites, taught the law of the Lord, making them withal to understand it.
en_uk_kjv~ES1~C09~49~Then spake Attharates unto Esdras the chief priest. and reader, and to the Levites that taught the multitude, even to all, saying,
en_uk_kjv~ES1~C09~50~This day is holy unto the Lord; (for they all wept when they heard the law:)
en_uk_kjv~ES1~C09~51~Go then, and eat the fat, and drink the sweet, and send part to them that have nothing;
en_uk_kjv~ES1~C09~52~For this day is holy unto the Lord: and be not sorrowful; for the Lord will bring you to honour.
en_uk_kjv~ES1~C09~53~So the Levites published all things to the people, saying, This day is holy to the Lord; be not sorrowful.
en_uk_kjv~ES1~C09~54~Then went they their way, every one to eat and drink, and make merry, and to give part to them that had nothing, and to make great cheer;
en_uk_kjv~ES1~C09~55~Because they understood the words wherein they were instructed, and for the which they had been assembled.
