en_uk_kjv~JOB~C04~01~Then Eliphaz the Temanite answered and said,
en_uk_kjv~JOB~C04~02~[If] we assay to commune with thee, wilt thou be grieved? but who can withhold himself from speaking?
en_uk_kjv~JOB~C04~03~Behold, thou hast instructed many, and thou hast strengthened the weak hands.
en_uk_kjv~JOB~C04~04~Thy words have upholden him that was falling, and thou hast strengthened the feeble knees.
en_uk_kjv~JOB~C04~05~But now it is come upon thee, and thou faintest; it toucheth thee, and thou art troubled.
en_uk_kjv~JOB~C04~06~[Is] not [this] thy fear, thy confidence, thy hope, and the uprightness of thy ways?
en_uk_kjv~JOB~C04~07~Remember, I pray thee, who [ever] perished, being innocent? or where were the righteous cut off?
en_uk_kjv~JOB~C04~08~Even as I have seen, they that plow iniquity, and sow wickedness, reap the same.
en_uk_kjv~JOB~C04~09~By the blast of God they perish, and by the breath of his nostrils are they consumed.
en_uk_kjv~JOB~C04~10~The roaring of the lion, and the voice of the fierce lion, and the teeth of the young lions, are broken.
en_uk_kjv~JOB~C04~11~The old lion perisheth for lack of prey, and the stout lion’s whelps are scattered abroad.
en_uk_kjv~JOB~C04~12~Now a thing was secretly brought to me, and mine ear received a little thereof.
en_uk_kjv~JOB~C04~13~In thoughts from the visions of the night, when deep sleep falleth on men,
en_uk_kjv~JOB~C04~14~Fear came upon me, and trembling, which made all my bones to shake.
en_uk_kjv~JOB~C04~15~Then a spirit passed before my face; the hair of my flesh stood up:
en_uk_kjv~JOB~C04~16~It stood still, but I could not discern the form thereof: an image [was] before mine eyes, [there was] silence, and I heard a voice, [saying],
en_uk_kjv~JOB~C04~17~Shall mortal man be more just than God? shall a man be more pure than his maker?
en_uk_kjv~JOB~C04~18~Behold, he put no trust in his servants; and his angels he charged with folly:
en_uk_kjv~JOB~C04~19~How much less [in] them that dwell in houses of clay, whose foundation [is] in the dust, [which] are crushed before the moth?
en_uk_kjv~JOB~C04~20~They are destroyed from morning to evening: they perish for ever without any regarding [it].
en_uk_kjv~JOB~C04~21~Doth not their excellency [which is] in them go away? they die, even without wisdom.
