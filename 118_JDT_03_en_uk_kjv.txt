en_uk_kjv~JDT~C03~01~So they sent ambassadors unto him to treat of peace, saying,
en_uk_kjv~JDT~C03~02~Behold, we the servants of Nabuchodonosor the great king lie before thee; use us as shall be good in thy sight.
en_uk_kjv~JDT~C03~03~Behold, our houses, and all our places, and all our fields of wheat, and flocks, and herds, and all the lodges of our tents lie before thy face; use them as it pleaseth thee.
en_uk_kjv~JDT~C03~04~Behold, even our cities and the inhabitants thereof are thy servants; come and deal with them as seemeth good unto thee.
en_uk_kjv~JDT~C03~05~So the men came to Holofernes, and declared unto him after this manner.
en_uk_kjv~JDT~C03~06~Then came he down toward the sea coast, both he and his army, and set garrisons in the high cities, and took out of them chosen men for aid.
en_uk_kjv~JDT~C03~07~So they and all the country round about received them with garlands, with dances, and with timbrels.
en_uk_kjv~JDT~C03~08~Yet he did cast down their frontiers, and cut down their groves: for he had decreed to destroy all the gods of the land, that all nations should worship Nabuchodonosor only, and that all tongues and tribes should call upon him as god.
en_uk_kjv~JDT~C03~09~Also he came over against Esdraelon near unto Judea, over against the great strait of Judea.
en_uk_kjv~JDT~C03~10~And he pitched between Geba and Scythopolis, and there he tarried a whole month, that he might gather together all the carriages of his army.
