en_uk_kjv~JOH~C14~01~Let not your heart be troubled: ye believe in God, believe also in me.
en_uk_kjv~JOH~C14~02~In my Father’s house are many mansions: if [it were] not [so], I would have told you. I go to prepare a place for you.
en_uk_kjv~JOH~C14~03~And if I go and prepare a place for you, I will come again, and receive you unto myself; that where I am, [there] ye may be also.
en_uk_kjv~JOH~C14~04~And whither I go ye know, and the way ye know.
en_uk_kjv~JOH~C14~05~Thomas saith unto him, Lord, we know not whither thou goest; and how can we know the way?
en_uk_kjv~JOH~C14~06~Jesus saith unto him, I am the way, the truth, and the life: no man cometh unto the Father, but by me.
en_uk_kjv~JOH~C14~07~If ye had known me, ye should have known my Father also: and from henceforth ye know him, and have seen him.
en_uk_kjv~JOH~C14~08~Philip saith unto him, Lord, shew us the Father, and it sufficeth us.
en_uk_kjv~JOH~C14~09~Jesus saith unto him, Have I been so long time with you, and yet hast thou not known me, Philip? he that hath seen me hath seen the Father; and how sayest thou [then], Shew us the Father?
en_uk_kjv~JOH~C14~10~Believest thou not that I am in the Father, and the Father in me? the words that I speak unto you I speak not of myself: but the Father that dwelleth in me, he doeth the works.
en_uk_kjv~JOH~C14~11~Believe me that I [am] in the Father, and the Father in me: or else believe me for the very works’ sake.
en_uk_kjv~JOH~C14~12~Verily, verily, I say unto you, He that believeth on me, the works that I do shall he do also; and greater [works] than these shall he do; because I go unto my Father.
en_uk_kjv~JOH~C14~13~And whatsoever ye shall ask in my name, that will I do, that the Father may be glorified in the Son.
en_uk_kjv~JOH~C14~14~If ye shall ask any thing in my name, I will do [it.]
en_uk_kjv~JOH~C14~15~If ye love me, keep my commandments.
en_uk_kjv~JOH~C14~16~And I will pray the Father, and he shall give you another Comforter, that he may abide with you for ever;
en_uk_kjv~JOH~C14~17~[Even] the Spirit of truth; whom the world cannot receive, because it seeth him not, neither knoweth him: but ye know him; for he dwelleth with you, and shall be in you.
en_uk_kjv~JOH~C14~18~I will not leave you comfortless: I will come to you.
en_uk_kjv~JOH~C14~19~Yet a little while, and the world seeth me no more; but ye see me: because I live, ye shall live also.
en_uk_kjv~JOH~C14~20~At that day ye shall know that I [am] in my Father, and ye in me, and I in you.
en_uk_kjv~JOH~C14~21~He that hath my commandments, and keepeth them, he it is that loveth me: and he that loveth me shall be loved of my Father, and I will love him, and will manifest myself to him.
en_uk_kjv~JOH~C14~22~Judas saith unto him, not Iscariot, Lord, how is it that thou wilt manifest thyself unto us, and not unto the world?
en_uk_kjv~JOH~C14~23~Jesus answered and said unto him, If a man love me, he will keep my words: and my Father will love him, and we will come unto him, and make our abode with him.
en_uk_kjv~JOH~C14~24~He that loveth me not keepeth not my sayings: and the word which ye hear is not mine, but the Father’s which sent me.
en_uk_kjv~JOH~C14~25~These things have I spoken unto you, being [yet] present with you.
en_uk_kjv~JOH~C14~26~But the Comforter, [which is] the Holy Ghost, whom the Father will send in my name, he shall teach you all things, and bring all things to your remembrance, whatsoever I have said unto you.
en_uk_kjv~JOH~C14~27~Peace I leave with you, my peace I give unto you: not as the world giveth, give I unto you. Let not your heart be troubled, neither let it be afraid.
en_uk_kjv~JOH~C14~28~Ye have heard how I said unto you, I go away, and come [again] unto you. If ye loved me, ye would rejoice, because I said, I go unto the Father: for my Father is greater than I.
en_uk_kjv~JOH~C14~29~And now I have told you before it come to pass, that, when it is come to pass, ye might believe.
en_uk_kjv~JOH~C14~30~Hereafter I will not talk much with you: for the prince of this world cometh, and hath nothing in me.
en_uk_kjv~JOH~C14~31~But that the world may know that I love the Father; and as the Father gave me commandment, even so I do. Arise, let us go hence.
