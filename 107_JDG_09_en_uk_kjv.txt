en_uk_kjv~JDG~C09~01~And Abimelech the son of Jerubbaal went to Shechem unto his mother’s brethren, and communed with them, and with all the family of the house of his mother’s father, saying,
en_uk_kjv~JDG~C09~02~Speak, I pray you, in the ears of all the men of Shechem, Whether [is] better for you, either that all the sons of Jerubbaal, [which are] threescore and ten persons, reign over you, or that one reign over you? remember also that I [am] your bone and your flesh.
en_uk_kjv~JDG~C09~03~And his mother’s brethren spake of him in the ears of all the men of Shechem all these words: and their hearts inclined to follow Abimelech; for they said, He [is] our brother.
en_uk_kjv~JDG~C09~04~And they gave him threescore and ten [pieces] of silver out of the house of Baal-berith, wherewith Abimelech hired vain and light persons, which followed him.
en_uk_kjv~JDG~C09~05~And he went unto his father’s house at Ophrah, and slew his brethren the sons of Jerubbaal, [being] threescore and ten persons, upon one stone: notwithstanding yet Jotham the youngest son of Jerubbaal was left; for he hid himself.
en_uk_kjv~JDG~C09~06~And all the men of Shechem gathered together, and all the house of Millo, and went, and made Abimelech king, by the plain of the pillar that [was] in Shechem.
en_uk_kjv~JDG~C09~07~And when they told [it] to Jotham, he went and stood in the top of mount Gerizim, and lifted up his voice, and cried, and said unto them, Hearken unto me, ye men of Shechem, that God may hearken unto you.
en_uk_kjv~JDG~C09~08~The trees went forth [on a time] to anoint a king over them; and they said unto the olive tree, Reign thou over us.
en_uk_kjv~JDG~C09~09~But the olive tree said unto them, Should I leave my fatness, wherewith by me they honour God and man, and go to be promoted over the trees?
en_uk_kjv~JDG~C09~10~And the trees said to the fig tree, Come thou, [and] reign over us.
en_uk_kjv~JDG~C09~11~But the fig tree said unto them, Should I forsake my sweetness, and my good fruit, and go to be promoted over the trees?
en_uk_kjv~JDG~C09~12~Then said the trees unto the vine, Come thou, [and] reign over us.
en_uk_kjv~JDG~C09~13~And the vine said unto them, Should I leave my wine, which cheereth God and man, and go to be promoted over the trees?
en_uk_kjv~JDG~C09~14~Then said all the trees unto the bramble, Come thou, [and] reign over us.
en_uk_kjv~JDG~C09~15~And the bramble said unto the trees, If in truth ye anoint me king over you, [then] come [and] put your trust in my shadow: and if not, let fire come out of the bramble, and devour the cedars of Lebanon.
en_uk_kjv~JDG~C09~16~Now therefore, if ye have done truly and sincerely, in that ye have made Abimelech king, and if ye have dealt well with Jerubbaal and his house, and have done unto him according to the deserving of his hands;
en_uk_kjv~JDG~C09~17~(For my father fought for you, and adventured his life far, and delivered you out of the hand of Midian:
en_uk_kjv~JDG~C09~18~And ye are risen up against my father’s house this day, and have slain his sons, threescore and ten persons, upon one stone, and have made Abimelech, the son of his maidservant, king over the men of Shechem, because he [is] your brother;)
en_uk_kjv~JDG~C09~19~If ye then have dealt truly and sincerely with Jerubbaal and with his house this day, [then] rejoice ye in Abimelech, and let him also rejoice in you:
en_uk_kjv~JDG~C09~20~But if not, let fire come out from Abimelech, and devour the men of Shechem, and the house of Millo; and let fire come out from the men of Shechem, and from the house of Millo, and devour Abimelech.
en_uk_kjv~JDG~C09~21~And Jotham ran away, and fled, and went to Beer, and dwelt there, for fear of Abimelech his brother.
en_uk_kjv~JDG~C09~22~When Abimelech had reigned three years over Israel,
en_uk_kjv~JDG~C09~23~Then God sent an evil spirit between Abimelech and the men of Shechem; and the men of Shechem dealt treacherously with Abimelech:
en_uk_kjv~JDG~C09~24~That the cruelty [done] to the threescore and ten sons of Jerubbaal might come, and their blood be laid upon Abimelech their brother, which slew them; and upon the men of Shechem, which aided him in the killing of his brethren.
en_uk_kjv~JDG~C09~25~And the men of Shechem set liers in wait for him in the top of the mountains, and they robbed all that came along that way by them: and it was told Abimelech.
en_uk_kjv~JDG~C09~26~And Gaal the son of Ebed came with his brethren, and went over to Shechem: and the men of Shechem put their confidence in him.
en_uk_kjv~JDG~C09~27~And they went out into the fields, and gathered their vineyards, and trode [the grapes], and made merry, and went into the house of their god, and did eat and drink, and cursed Abimelech.
en_uk_kjv~JDG~C09~28~And Gaal the son of Ebed said, Who [is] Abimelech, and who [is] Shechem, that we should serve him? [is] not [he] the son of Jerubbaal? and Zebul his officer? serve the men of Hamor the father of Shechem: for why should we serve him?
en_uk_kjv~JDG~C09~29~And would to God this people were under my hand! then would I remove Abimelech. And he said to Abimelech, Increase thine army, and come out.
en_uk_kjv~JDG~C09~30~And when Zebul the ruler of the city heard the words of Gaal the son of Ebed, his anger was kindled.
en_uk_kjv~JDG~C09~31~And he sent messengers unto Abimelech privily, saying, Behold, Gaal the son of Ebed and his brethren be come to Shechem; and, behold, they fortify the city against thee.
en_uk_kjv~JDG~C09~32~Now therefore up by night, thou and the people that [is] with thee, and lie in wait in the field:
en_uk_kjv~JDG~C09~33~And it shall be, [that] in the morning, as soon as the sun is up, thou shalt rise early, and set upon the city: and, behold, [when] he and the people that [is] with him come out against thee, then mayest thou do to them as thou shalt find occasion.
en_uk_kjv~JDG~C09~34~And Abimelech rose up, and all the people that [were] with him, by night, and they laid wait against Shechem in four companies.
en_uk_kjv~JDG~C09~35~And Gaal the son of Ebed went out, and stood in the entering of the gate of the city: and Abimelech rose up, and the people that [were] with him, from lying in wait.
en_uk_kjv~JDG~C09~36~And when Gaal saw the people, he said to Zebul, Behold, there come people down from the top of the mountains. And Zebul said unto him, Thou seest the shadow of the mountains as [if they were] men.
en_uk_kjv~JDG~C09~37~And Gaal spake again and said, See there come people down by the middle of the land, and another company come along by the plain of Meonenim.
en_uk_kjv~JDG~C09~38~Then said Zebul unto him, Where [is] now thy mouth, wherewith thou saidst, Who [is] Abimelech, that we should serve him? [is] not this the people that thou hast despised? go out, I pray now, and fight with them.
en_uk_kjv~JDG~C09~39~And Gaal went out before the men of Shechem, and fought with Abimelech.
en_uk_kjv~JDG~C09~40~And Abimelech chased him, and he fled before him, and many were overthrown [and] wounded, [even] unto the entering of the gate.
en_uk_kjv~JDG~C09~41~And Abimelech dwelt at Arumah: and Zebul thrust out Gaal and his brethren, that they should not dwell in Shechem.
en_uk_kjv~JDG~C09~42~And it came to pass on the morrow, that the people went out into the field; and they told Abimelech.
en_uk_kjv~JDG~C09~43~And he took the people, and divided them into three companies, and laid wait in the field, and looked, and, behold, the people [were] come forth out of the city; and he rose up against them, and smote them.
en_uk_kjv~JDG~C09~44~And Abimelech, and the company that [was] with him, rushed forward, and stood in the entering of the gate of the city: and the two [other] companies ran upon all [the people] that [were] in the fields, and slew them.
en_uk_kjv~JDG~C09~45~And Abimelech fought against the city all that day; and he took the city, and slew the people that [was] therein, and beat down the city, and sowed it with salt.
en_uk_kjv~JDG~C09~46~And when all the men of the tower of Shechem heard [that], they entered into an hold of the house of the god Berith.
en_uk_kjv~JDG~C09~47~And it was told Abimelech, that all the men of the tower of Shechem were gathered together.
en_uk_kjv~JDG~C09~48~And Abimelech gat him up to mount Zalmon, he and all the people that [were] with him; and Abimelech took an axe in his hand, and cut down a bough from the trees, and took it, and laid [it] on his shoulder, and said unto the people that [were] with him, What ye have seen me do, make haste, [and] do as I [have done].
en_uk_kjv~JDG~C09~49~And all the people likewise cut down every man his bough, and followed Abimelech, and put [them] to the hold, and set the hold on fire upon them; so that all the men of the tower of Shechem died also, about a thousand men and women.
en_uk_kjv~JDG~C09~50~Then went Abimelech to Thebez, and encamped against Thebez, and took it.
en_uk_kjv~JDG~C09~51~But there was a strong tower within the city, and thither fled all the men and women, and all they of the city, and shut [it] to them, and gat them up to the top of the tower.
en_uk_kjv~JDG~C09~52~And Abimelech came unto the tower, and fought against it, and went hard unto the door of the tower to burn it with fire.
en_uk_kjv~JDG~C09~53~And a certain woman cast a piece of a millstone upon Abimelech’s head, and all to brake his skull.
en_uk_kjv~JDG~C09~54~Then he called hastily unto the young man his armourbearer, and said unto him, Draw thy sword, and slay me, that men say not of me, A woman slew him. And his young man thrust him through, and he died.
en_uk_kjv~JDG~C09~55~And when the men of Israel saw that Abimelech was dead, they departed every man unto his place.
en_uk_kjv~JDG~C09~56~Thus God rendered the wickedness of Abimelech, which he did unto his father, in slaying his seventy brethren:
en_uk_kjv~JDG~C09~57~And all the evil of the men of Shechem did God render upon their heads: and upon them came the curse of Jotham the son of Jerubbaal.
