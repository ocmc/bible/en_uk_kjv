en_uk_kjv~JON~C02~01~Then Jonah prayed unto the LORD his God out of the fish’s belly,
en_uk_kjv~JON~C02~02~And said, I cried by reason of mine affliction unto the LORD, and he heard me; out of the belly of hell cried I, [and] thou heardest my voice.
en_uk_kjv~JON~C02~03~For thou hadst cast me into the deep, in the midst of the seas; and the floods compassed me about: all thy billows and thy waves passed over me.
en_uk_kjv~JON~C02~04~Then I said, I am cast out of thy sight; yet I will look again toward thy holy temple.
en_uk_kjv~JON~C02~05~The waters compassed me about, [even] to the soul: the depth closed me round about, the weeds were wrapped about my head.
en_uk_kjv~JON~C02~06~I went down to the bottoms of the mountains; the earth with her bars [was] about me for ever: yet hast thou brought up my life from corruption, O LORD my God.
en_uk_kjv~JON~C02~07~When my soul fainted within me I remembered the LORD: and my prayer came in unto thee, into thine holy temple.
en_uk_kjv~JON~C02~08~They that observe lying vanities forsake their own mercy.
en_uk_kjv~JON~C02~09~But I will sacrifice unto thee with the voice of thanksgiving; I will pay [that] that I have vowed. Salvation [is] of the LORD.
en_uk_kjv~JON~C02~10~And the LORD spake unto the fish, and it vomited out Jonah upon the dry [land].
