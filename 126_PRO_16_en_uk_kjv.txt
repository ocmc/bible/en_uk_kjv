en_uk_kjv~PRO~C16~01~The preparations of the heart in man, and the answer of the tongue, [is] from the LORD.
en_uk_kjv~PRO~C16~02~All the ways of a man [are] clean in his own eyes; but the LORD weigheth the spirits.
en_uk_kjv~PRO~C16~03~Commit thy works unto the LORD, and thy thoughts shall be established.
en_uk_kjv~PRO~C16~04~The LORD hath made all [things] for himself: yea, even the wicked for the day of evil.
en_uk_kjv~PRO~C16~05~Every one [that is] proud in heart [is] an abomination to the LORD: [though] hand [join] in hand, he shall not be unpunished.
en_uk_kjv~PRO~C16~06~By mercy and truth iniquity is purged: and by the fear of the LORD [men] depart from evil.
en_uk_kjv~PRO~C16~07~When a man’s ways please the LORD, he maketh even his enemies to be at peace with him.
en_uk_kjv~PRO~C16~08~Better [is] a little with righteousness than great revenues without right.
en_uk_kjv~PRO~C16~09~A man’s heart deviseth his way: but the LORD directeth his steps.
en_uk_kjv~PRO~C16~10~A divine sentence [is] in the lips of the king: his mouth transgresseth not in judgment.
en_uk_kjv~PRO~C16~11~A just weight and balance [are] the LORD’s: all the weights of the bag [are] his work.
en_uk_kjv~PRO~C16~12~[It is] an abomination to kings to commit wickedness: for the throne is established by righteousness.
en_uk_kjv~PRO~C16~13~Righteous lips [are] the delight of kings; and they love him that speaketh right.
en_uk_kjv~PRO~C16~14~The wrath of a king [is as] messengers of death: but a wise man will pacify it.
en_uk_kjv~PRO~C16~15~In the light of the king’s countenance [is] life; and his favour [is] as a cloud of the latter rain.
en_uk_kjv~PRO~C16~16~How much better [is it] to get wisdom than gold! and to get understanding rather to be chosen than silver!
en_uk_kjv~PRO~C16~17~The highway of the upright [is] to depart from evil: he that keepeth his way preserveth his soul.
en_uk_kjv~PRO~C16~18~Pride [goeth] before destruction, and an haughty spirit before a fall.
en_uk_kjv~PRO~C16~19~Better [it is to be] of an humble spirit with the lowly, than to divide the spoil with the proud.
en_uk_kjv~PRO~C16~20~He that handleth a matter wisely shall find good: and whoso trusteth in the LORD, happy [is] he.
en_uk_kjv~PRO~C16~21~The wise in heart shall be called prudent: and the sweetness of the lips increaseth learning.
en_uk_kjv~PRO~C16~22~Understanding [is] a wellspring of life unto him that hath it: but the instruction of fools [is] folly.
en_uk_kjv~PRO~C16~23~The heart of the wise teacheth his mouth, and addeth learning to his lips.
en_uk_kjv~PRO~C16~24~Pleasant words [are as] an honeycomb, sweet to the soul, and health to the bones.
en_uk_kjv~PRO~C16~25~There is a way that seemeth right unto a man, but the end thereof [are] the ways of death.
en_uk_kjv~PRO~C16~26~He that laboureth laboureth for himself; for his mouth craveth it of him.
en_uk_kjv~PRO~C16~27~An ungodly man diggeth up evil: and in his lips [there is] as a burning fire.
en_uk_kjv~PRO~C16~28~A froward man soweth strife: and a whisperer separateth chief friends.
en_uk_kjv~PRO~C16~29~A violent man enticeth his neighbour, and leadeth him into the way [that is] not good.
en_uk_kjv~PRO~C16~30~He shutteth his eyes to devise froward things: moving his lips he bringeth evil to pass.
en_uk_kjv~PRO~C16~31~The hoary head [is] a crown of glory, [if] it be found in the way of righteousness.
en_uk_kjv~PRO~C16~32~[He that is] slow to anger [is] better than the mighty; and he that ruleth his spirit than he that taketh a city.
en_uk_kjv~PRO~C16~33~The lot is cast into the lap; but the whole disposing thereof [is] of the LORD.
