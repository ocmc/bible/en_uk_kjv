en_uk_kjv~KI1~C15~01~Now in the eighteenth year of king Jeroboam the son of Nebat reigned Abijam over Judah.
en_uk_kjv~KI1~C15~02~Three years reigned he in Jerusalem. And his mother’s name [was] Maachah, the daughter of Abishalom.
en_uk_kjv~KI1~C15~03~And he walked in all the sins of his father, which he had done before him: and his heart was not perfect with the LORD his God, as the heart of David his father.
en_uk_kjv~KI1~C15~04~Nevertheless for David’s sake did the LORD his God give him a lamp in Jerusalem, to set up his son after him, and to establish Jerusalem:
en_uk_kjv~KI1~C15~05~Because David did [that which was] right in the eyes of the LORD, and turned not aside from any [thing] that he commanded him all the days of his life, save only in the matter of Uriah the Hittite.
en_uk_kjv~KI1~C15~06~And there was war between Rehoboam and Jeroboam all the days of his life.
en_uk_kjv~KI1~C15~07~Now the rest of the acts of Abijam, and all that he did, [are] they not written in the book of the chronicles of the kings of Judah? And there was war between Abijam and Jeroboam.
en_uk_kjv~KI1~C15~08~And Abijam slept with his fathers; and they buried him in the city of David: and Asa his son reigned in his stead.
en_uk_kjv~KI1~C15~09~And in the twentieth year of Jeroboam king of Israel reigned Asa over Judah.
en_uk_kjv~KI1~C15~10~And forty and one years reigned he in Jerusalem. And his mother’s name [was] Maachah, the daughter of Abishalom.
en_uk_kjv~KI1~C15~11~And Asa did [that which was] right in the eyes of the LORD, as [did] David his father.
en_uk_kjv~KI1~C15~12~And he took away the sodomites out of the land, and removed all the idols that his fathers had made.
en_uk_kjv~KI1~C15~13~And also Maachah his mother, even her he removed from [being] queen, because she had made an idol in a grove; and Asa destroyed her idol, and burnt [it] by the brook Kidron.
en_uk_kjv~KI1~C15~14~But the high places were not removed: nevertheless Asa’s heart was perfect with the LORD all his days.
en_uk_kjv~KI1~C15~15~And he brought in the things which his father had dedicated, and the things which himself had dedicated, into the house of the LORD, silver, and gold, and vessels.
en_uk_kjv~KI1~C15~16~And there was war between Asa and Baasha king of Israel all their days.
en_uk_kjv~KI1~C15~17~And Baasha king of Israel went up against Judah, and built Ramah, that he might not suffer any to go out or come in to Asa king of Judah.
en_uk_kjv~KI1~C15~18~Then Asa took all the silver and the gold [that were] left in the treasures of the house of the LORD, and the treasures of the king’s house, and delivered them into the hand of his servants: and king Asa sent them to Ben-hadad, the son of Tabrimon, the son of Hezion, king of Syria, that dwelt at Damascus, saying,
en_uk_kjv~KI1~C15~19~[There is] a league between me and thee, [and] between my father and thy father: behold, I have sent unto thee a present of silver and gold; come and break thy league with Baasha king of Israel, that he may depart from me.
en_uk_kjv~KI1~C15~20~So Ben-hadad hearkened unto king Asa, and sent the captains of the hosts which he had against the cities of Israel, and smote Ijon, and Dan, and Abel-beth-maachah, and all Cinneroth, with all the land of Naphtali.
en_uk_kjv~KI1~C15~21~And it came to pass, when Baasha heard [thereof], that he left off building of Ramah, and dwelt in Tirzah.
en_uk_kjv~KI1~C15~22~Then king Asa made a proclamation throughout all Judah; none [was] exempted: and they took away the stones of Ramah, and the timber thereof, wherewith Baasha had builded; and king Asa built with them Geba of Benjamin, and Mizpah.
en_uk_kjv~KI1~C15~23~The rest of all the acts of Asa, and all his might, and all that he did, and the cities which he built, [are] they not written in the book of the chronicles of the kings of Judah? Nevertheless in the time of his old age he was diseased in his feet.
en_uk_kjv~KI1~C15~24~And Asa slept with his fathers, and was buried with his fathers in the city of David his father: and Jehoshaphat his son reigned in his stead.
en_uk_kjv~KI1~C15~25~And Nadab the son of Jeroboam began to reign over Israel in the second year of Asa king of Judah, and reigned over Israel two years.
en_uk_kjv~KI1~C15~26~And he did evil in the sight of the LORD, and walked in the way of his father, and in his sin wherewith he made Israel to sin.
en_uk_kjv~KI1~C15~27~And Baasha the son of Ahijah, of the house of Issachar, conspired against him; and Baasha smote him at Gibbethon, which [belonged] to the Philistines; for Nadab and all Israel laid siege to Gibbethon.
en_uk_kjv~KI1~C15~28~Even in the third year of Asa king of Judah did Baasha slay him, and reigned in his stead.
en_uk_kjv~KI1~C15~29~And it came to pass, when he reigned, [that] he smote all the house of Jeroboam; he left not to Jeroboam any that breathed, until he had destroyed him, according unto the saying of the LORD, which he spake by his servant Ahijah the Shilonite:
en_uk_kjv~KI1~C15~30~Because of the sins of Jeroboam which he sinned, and which he made Israel sin, by his provocation wherewith he provoked the LORD God of Israel to anger.
en_uk_kjv~KI1~C15~31~Now the rest of the acts of Nadab, and all that he did, [are] they not written in the book of the chronicles of the kings of Israel?
en_uk_kjv~KI1~C15~32~And there was war between Asa and Baasha king of Israel all their days.
en_uk_kjv~KI1~C15~33~In the third year of Asa king of Judah began Baasha the son of Ahijah to reign over all Israel in Tirzah, twenty and four years.
en_uk_kjv~KI1~C15~34~And he did evil in the sight of the LORD, and walked in the way of Jeroboam, and in his sin wherewith he made Israel to sin.
