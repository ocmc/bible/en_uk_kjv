en_uk_kjv~TH2~C01~01~Paul, and Silvanus, and Timotheus, unto the church of the Thessalonians in God our Father and the Lord Jesus Christ:
en_uk_kjv~TH2~C01~02~Grace unto you, and peace, from God our Father and the Lord Jesus Christ.
en_uk_kjv~TH2~C01~03~We are bound to thank God always for you, brethren, as it is meet, because that your faith groweth exceedingly, and the charity of every one of you all toward each other aboundeth;
en_uk_kjv~TH2~C01~04~So that we ourselves glory in you in the churches of God for your patience and faith in all your persecutions and tribulations that ye endure:
en_uk_kjv~TH2~C01~05~[Which is] a manifest token of the righteous judgment of God, that ye may be counted worthy of the kingdom of God, for which ye also suffer:
en_uk_kjv~TH2~C01~06~Seeing [it is] a righteous thing with God to recompense tribulation to them that trouble you;
en_uk_kjv~TH2~C01~07~And to you who are troubled rest with us, when the Lord Jesus shall be revealed from heaven with his mighty angels,
en_uk_kjv~TH2~C01~08~In flaming fire taking vengeance on them that know not God, and that obey not the gospel of our Lord Jesus Christ:
en_uk_kjv~TH2~C01~09~Who shall be punished with everlasting destruction from the presence of the Lord, and from the glory of his power;
en_uk_kjv~TH2~C01~10~When he shall come to be glorified in his saints, and to be admired in all them that believe (because our testimony among you was believed) in that day.
en_uk_kjv~TH2~C01~11~Wherefore also we pray always for you, that our God would count you worthy of [this] calling, and fulfil all the good pleasure of [his] goodness, and the work of faith with power:
en_uk_kjv~TH2~C01~12~That the name of our Lord Jesus Christ may be glorified in you, and ye in him, according to the grace of our God and the Lord Jesus Christ.
