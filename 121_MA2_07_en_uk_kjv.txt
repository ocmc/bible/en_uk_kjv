en_uk_kjv~MA2~C07~01~It came to pass also, that seven brethren with their mother were taken, and compelled by the king against the law to taste swine’s flesh, and were tormented with scourges and whips.
en_uk_kjv~MA2~C07~02~But one of them that spake first said thus, What wouldest thou ask or learn of us? we are ready to die, rather than to transgress the laws of our fathers.
en_uk_kjv~MA2~C07~03~Then the king, being in a rage, commanded pans and caldrons to be made hot:
en_uk_kjv~MA2~C07~04~Which forthwith being heated, he commanded to cut out the tongue of him that spake first, and to cut off the utmost parts of his body, the rest of his brethren and his mother looking on.
en_uk_kjv~MA2~C07~05~Now when he was thus maimed in all his members, he commanded him being yet alive to be brought to the fire, and to be fried in the pan: and as the vapour of the pan was for a good space dispersed, they exhorted one another with the mother to die manfully, saying thus,
en_uk_kjv~MA2~C07~06~The Lord God looketh upon us, and in truth hath comfort in us, as Moses in his song, which witnessed to their faces, declared, saying, And he shall be comforted in his servants.
en_uk_kjv~MA2~C07~07~So when the first was dead after this manner, they brought the second to make him a mocking stock: and when they had pulled off the skin of his head with the hair, they asked him, Wilt thou eat, before thou be punished throughout every member of thy body?
en_uk_kjv~MA2~C07~08~But he answered in his own language, and said, No. Wherefore he also received the next torment in order, as the former did.
en_uk_kjv~MA2~C07~09~And when he was at the last gasp, he said, Thou like a fury takest us out of this present life, but the King of the world shall raise us up, who have died for his laws, unto everlasting life.
en_uk_kjv~MA2~C07~10~After him was the third made a mocking stock: and when he was required, he put out his tongue, and that right soon, holding forth his hands manfully.
en_uk_kjv~MA2~C07~11~And said courageously, These I had from heaven; and for his laws I despise them; and from him I hope to receive them again.
en_uk_kjv~MA2~C07~12~Insomuch that the king, and they that were with him, marvelled at the young man’s courage, for that he nothing regarded the pains.
en_uk_kjv~MA2~C07~13~Now when this man was dead also, they tormented and mangled the fourth in like manner.
en_uk_kjv~MA2~C07~14~So when he was ready to die he said thus, It is good, being put to death by men, to look for hope from God to be raised up again by him: as for thee, thou shalt have no resurrection to life.
en_uk_kjv~MA2~C07~15~Afterward they brought the fifth also, and mangled him.
en_uk_kjv~MA2~C07~16~Then looked he unto the king, and said, Thou hast power over men, thou art corruptible, thou doest what thou wilt; yet think not that our nation is forsaken of God;
en_uk_kjv~MA2~C07~17~But abide a while, and behold his great power, how he will torment thee and thy seed.
en_uk_kjv~MA2~C07~18~After him also they brought the sixth, who being ready to die said, Be not deceived without cause: for we suffer these things for ourselves, having sinned against our God: therefore marvellous things are done unto us.
en_uk_kjv~MA2~C07~19~But think not thou, that takest in hand to strive against God, that thou shalt escape unpunished.
en_uk_kjv~MA2~C07~20~But the mother was marvellous above all, and worthy of honourable memory: for when she saw her seven sons slain within the space of one day, she bare it with a good courage, because of the hope that she had in the Lord.
en_uk_kjv~MA2~C07~21~Yea, she exhorted every one of them in her own language, filled with courageous spirits; and stirring up her womanish thoughts with a manly stomach, she said unto them,
en_uk_kjv~MA2~C07~22~I cannot tell how ye came into my womb: for I neither gave you breath nor life, neither was it I that formed the members of every one of you;
en_uk_kjv~MA2~C07~23~But doubtless the Creator of the world, who formed the generation of man, and found out the beginning of all things, will also of his own mercy give you breath and life again, as ye now regard not your own selves for his laws’ sake.
en_uk_kjv~MA2~C07~24~Now Antiochus, thinking himself despised, and suspecting it to be a reproachful speech, whilst the youngest was yet alive, did not only exhort him by words, but also assured him with oaths, that he would make him both a rich and a happy man, if he would turn from the laws of his fathers; and that also he would take him for his friend, and trust him with affairs.
en_uk_kjv~MA2~C07~25~But when the young man would in no case hearken unto him, the king called his mother, and exhorted her that she would counsel the young man to save his life.
en_uk_kjv~MA2~C07~26~And when he had exhorted her with many words, she promised him that she would counsel her son.
en_uk_kjv~MA2~C07~27~But she bowing herself toward him, laughing the cruel tyrant to scorn, spake in her country language on this manner; O my son, have pity upon me that bare thee nine months in my womb, and gave thee suck three years, and nourished thee, and brought thee up unto this age, and endured the troubles of education.
en_uk_kjv~MA2~C07~28~I beseech thee, my son, look upon the heaven and the earth, and all that is therein, and consider that God made them of things that were not; and so was mankind made likewise.
en_uk_kjv~MA2~C07~29~Fear not this tormentor, but, being worthy of thy brethren, take thy death that I may receive thee again in mercy with thy brethren.
en_uk_kjv~MA2~C07~30~Whiles she was yet speaking these words, the young man said, Whom wait ye for? I will not obey the king’s commandment: but I will obey the commandment of the law that was given unto our fathers by Moses.
en_uk_kjv~MA2~C07~31~And thou, that hast been the author of all mischief against the Hebrews, shalt not escape the hands of God.
en_uk_kjv~MA2~C07~32~For we suffer because of our sins.
en_uk_kjv~MA2~C07~33~And though the living Lord be angry with us a little while for our chastening and correction, yet shall he be at one again with his servants.
en_uk_kjv~MA2~C07~34~But thou, O godless man, and of all other most wicked, be not lifted up without a cause, nor puffed up with uncertain hopes, lifting up thy hand against the servants of God:
en_uk_kjv~MA2~C07~35~For thou hast not yet escaped the judgment of Almighty God, who seeth all things.
en_uk_kjv~MA2~C07~36~For our brethren, who now have suffered a short pain, are dead under God’s covenant of everlasting life: but thou, through the judgment of God, shalt receive just punishment for thy pride.
en_uk_kjv~MA2~C07~37~But I, as my brethren, offer up my body and life for the laws of our fathers, beseeching God that he would speedily be merciful unto our nation; and that thou by torments and plagues mayest confess, that he alone is God;
en_uk_kjv~MA2~C07~38~And that in me and my brethren the wrath of the Almighty, which is justly brought upon our nation, may cease.
en_uk_kjv~MA2~C07~39~Then the king being in a rage, handed him worse than all the rest, and took it grievously that he was mocked.
en_uk_kjv~MA2~C07~40~So this man died undefiled, and put his whole trust in the Lord.
en_uk_kjv~MA2~C07~41~Last of all after the sons the mother died.
en_uk_kjv~MA2~C07~42~Let this be enough now to have spoken concerning the idolatrous feasts, and the extreme tortures.
