en_uk_kjv~PSA~C137~001~(C138:001) (0) [A Psalm] of David. (1) I will praise thee with my whole heart: before the gods will I sing praise unto thee.
en_uk_kjv~PSA~C137~002~(C138:002) I will worship toward thy holy temple, and praise thy name for thy lovingkindness and for thy truth: for thou hast magnified thy word above all thy name.
en_uk_kjv~PSA~C137~003~(C138:003) In the day when I cried thou answeredst me, [and] strengthenedst me [with] strength in my soul.
en_uk_kjv~PSA~C137~004~(C138:004) All the kings of the earth shall praise thee, O LORD, when they hear the words of thy mouth.
en_uk_kjv~PSA~C137~005~(C138:005) Yea, they shall sing in the ways of the LORD: for great [is] the glory of the LORD.
en_uk_kjv~PSA~C137~006~(C138:006) Though the LORD [be] high, yet hath he respect unto the lowly: but the proud he knoweth afar off.
en_uk_kjv~PSA~C137~007~(C138:007) Though I walk in the midst of trouble, thou wilt revive me: thou shalt stretch forth thine hand against the wrath of mine enemies, and thy right hand shall save me.
en_uk_kjv~PSA~C137~008~(C138:008) The LORD will perfect [that which] concerneth me: thy mercy, O LORD, [endureth] for ever: forsake not the works of thine own hands.
