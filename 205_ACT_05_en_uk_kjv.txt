en_uk_kjv~ACT~C05~01~But a certain man named Ananias, with Sapphira his wife, sold a possession,
en_uk_kjv~ACT~C05~02~And kept back [part] of the price, his wife also being privy [to it], and brought a certain part, and laid [it] at the apostles’ feet.
en_uk_kjv~ACT~C05~03~But Peter said, Ananias, why hath Satan filled thine heart to lie to the Holy Ghost, and to keep back [part] of the price of the land?
en_uk_kjv~ACT~C05~04~Whiles it remained, was it not thine own? and after it was sold, was it not in thine own power? why hast thou conceived this thing in thine heart? thou hast not lied unto men, but unto God.
en_uk_kjv~ACT~C05~05~And Ananias hearing these words fell down, and gave up the ghost: and great fear came on all them that heard these things.
en_uk_kjv~ACT~C05~06~And the young men arose, wound him up, and carried [him] out, and buried [him].
en_uk_kjv~ACT~C05~07~And it was about the space of three hours after, when his wife, not knowing what was done, came in.
en_uk_kjv~ACT~C05~08~And Peter answered unto her, Tell me whether ye sold the land for so much? And she said, Yea, for so much.
en_uk_kjv~ACT~C05~09~Then Peter said unto her, How is it that ye have agreed together to tempt the Spirit of the Lord? behold, the feet of them which have buried thy husband [are] at the door, and shall carry thee out.
en_uk_kjv~ACT~C05~10~Then fell she down straightway at his feet, and yielded up the ghost: and the young men came in, and found her dead, and, carrying [her] forth, buried [her] by her husband.
en_uk_kjv~ACT~C05~11~And great fear came upon all the church, and upon as many as heard these things.
en_uk_kjv~ACT~C05~12~And by the hands of the apostles were many signs and wonders wrought among the people; (and they were all with one accord in Solomon’s porch.
en_uk_kjv~ACT~C05~13~And of the rest durst no man join himself to them: but the people magnified them.
en_uk_kjv~ACT~C05~14~And believers were the more added to the Lord, multitudes both of men and women.)
en_uk_kjv~ACT~C05~15~Insomuch that they brought forth the sick into the streets, and laid [them] on beds and couches, that at the least the shadow of Peter passing by might overshadow some of them.
en_uk_kjv~ACT~C05~16~There came also a multitude [out] of the cities round about unto Jerusalem, bringing sick folks, and them which were vexed with unclean spirits: and they were healed every one.
en_uk_kjv~ACT~C05~17~Then the high priest rose up, and all they that were with him, (which is the sect of the Sadducees,) and were filled with indignation,
en_uk_kjv~ACT~C05~18~And laid their hands on the apostles, and put them in the common prison.
en_uk_kjv~ACT~C05~19~But the angel of the Lord by night opened the prison doors, and brought them forth, and said,
en_uk_kjv~ACT~C05~20~Go, stand and speak in the temple to the people all the words of this life.
en_uk_kjv~ACT~C05~21~And when they heard [that], they entered into the temple early in the morning, and taught. But the high priest came, and they that were with him, and called the council together, and all the senate of the children of Israel, and sent to the prison to have them brought.
en_uk_kjv~ACT~C05~22~But when the officers came, and found them not in the prison, they returned, and told,
en_uk_kjv~ACT~C05~23~Saying, The prison truly found we shut with all safety, and the keepers standing without before the doors: but when we had opened, we found no man within.
en_uk_kjv~ACT~C05~24~Now when the high priest and the captain of the temple and the chief priests heard these things, they doubted of them whereunto this would grow.
en_uk_kjv~ACT~C05~25~Then came one and told them, saying, Behold, the men whom ye put in prison are standing in the temple, and teaching the people.
en_uk_kjv~ACT~C05~26~Then went the captain with the officers, and brought them without violence: for they feared the people, lest they should have been stoned.
en_uk_kjv~ACT~C05~27~And when they had brought them, they set [them] before the council: and the high priest asked them,
en_uk_kjv~ACT~C05~28~Saying, Did not we straitly command you that ye should not teach in this name? and, behold, ye have filled Jerusalem with your doctrine, and intend to bring this man’s blood upon us.
en_uk_kjv~ACT~C05~29~Then Peter and the [other] apostles answered and said, We ought to obey God rather than men.
en_uk_kjv~ACT~C05~30~The God of our fathers raised up Jesus, whom ye slew and hanged on a tree.
en_uk_kjv~ACT~C05~31~Him hath God exalted with his right hand [to be] a Prince and a Saviour, for to give repentance to Israel, and forgiveness of sins.
en_uk_kjv~ACT~C05~32~And we are his witnesses of these things; and [so is] also the Holy Ghost, whom God hath given to them that obey him.
en_uk_kjv~ACT~C05~33~When they heard [that], they were cut [to the heart], and took counsel to slay them.
en_uk_kjv~ACT~C05~34~Then stood there up one in the council, a Pharisee, named Gamaliel, a doctor of the law, had in reputation among all the people, and commanded to put the apostles forth a little space;
en_uk_kjv~ACT~C05~35~And said unto them, Ye men of Israel, take heed to yourselves what ye intend to do as touching these men.
en_uk_kjv~ACT~C05~36~For before these days rose up Theudas, boasting himself to be somebody; to whom a number of men, about four hundred, joined themselves: who was slain; and all, as many as obeyed him, were scattered, and brought to nought.
en_uk_kjv~ACT~C05~37~After this man rose up Judas of Galilee in the days of the taxing, and drew away much people after him: he also perished; and all, [even] as many as obeyed him, were dispersed.
en_uk_kjv~ACT~C05~38~And now I say unto you, Refrain from these men, and let them alone: for if this counsel or this work be of men, it will come to nought:
en_uk_kjv~ACT~C05~39~But if it be of God, ye cannot overthrow it; lest haply ye be found even to fight against God.
en_uk_kjv~ACT~C05~40~And to him they agreed: and when they had called the apostles, and beaten [them], they commanded that they should not speak in the name of Jesus, and let them go.
en_uk_kjv~ACT~C05~41~And they departed from the presence of the council, rejoicing that they were counted worthy to suffer shame for his name.
en_uk_kjv~ACT~C05~42~And daily in the temple, and in every house, they ceased not to teach and preach Jesus Christ.
